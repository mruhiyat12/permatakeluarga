/*
 Navicat Premium Data Transfer

 Source Server         : Lokal
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : rspklc_db

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 25/11/2021 21:09:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rumah_sakit
-- ----------------------------
DROP TABLE IF EXISTS `rumah_sakit`;
CREATE TABLE `rumah_sakit`  (
  `id_rumahsakit` int(11) NOT NULL AUTO_INCREMENT,
  `nama_rumahsakit` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `telp` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jl` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_rumahsakit`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rumah_sakit
-- ----------------------------
INSERT INTO `rumah_sakit` VALUES (1, 'rspk lippo', '<p>lippo</p>', 1, '+62 822-9119-1199', ' Jl. M.H. Thamrin Kav.129 Lippo - Cikarang Kab. Bekasi - 17550 Jawab Barat - Indonesia');
INSERT INTO `rumah_sakit` VALUES (2, 'RSPK Jababeka', '<p>Jababeka</p>', 1, '+62 856-0408-1560', '  Jl. Dr. Cipto Mangunkusumo Blok A No. 1A, Medical City - Kota Jababeka Kab. Bekasi - 17550 Jawa Barat - Indonesia\r\n');
INSERT INTO `rumah_sakit` VALUES (4, 'RSPK galuhmas', '<p>RSPK galuhmas</p>', 1, '+62 856-0408-1560', ' jl.Galuhmas');

SET FOREIGN_KEY_CHECKS = 1;
