<?php

class Konsuladmin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // if ($this->session->userdata('_USER_ID') == '') {
        //     redirect(base_url());
        //     exit;
        // }
    }

    public function index($params = null)
    {

        if ($params == null) {
            $action = base_url('konsuladmin/store/');
        } else {
            $data  = $this->db->get_where('tmkonsultasi', [
                'id' => $params
            ])->row_array();
            $action = base_url('konsuladmin/edit/' . $data['id']);
        }
        $this->load->view('cms/konsul/index', [
            'title' => 'Manage Halaman kosnultasi',
            'id' => isset($data['id']) ? $data['id'] : '',
            'action' => $action,
            'tmrs_id' => isset($data['tmrs_id']) ? $data['tmrs_id'] : '',
            'penjaminan' =>  isset($data['penjaminan']) ? $data['penjaminan'] : '',
            'tgl_konsul' =>  isset($data['tgl_konsul']) ? $data['tgl_konsul'] : '',
            'penjaminan' =>  isset($data['penjaminan']) ? $data['penjaminan'] : '',
            'jadwal_dokter_id' =>  isset($data['jadwal_dokter_id']) ? $data['jadwal_dokter_id'] : '',
            'no_rek_medis' =>  isset($data['no_rek_medis']) ? $data['no_rek_medis'] : '',
            'nama_lengkap' =>  isset($data['nama_lengkap']) ? $data['nama_lengkap'] : '',
            'tgl_lahir' =>  isset($data['tgl_lahir']) ? $data['tgl_lahir'] : '',
            'jk' =>  isset($data['jk']) ? $data['jk'] : '',
            'hp' =>  isset($data['hp']) ? $data['hp'] : '',
            'email' =>  isset($data['email']) ? $data['email'] : '',
            'ktp_passpor' =>  isset($data['ktp_passpor']) ? $data['ktp_passpor'] : '',
            'updated_at' =>  isset($data['updated_at']) ? $data['updated_at'] : '',
            'cretated_at' =>  isset($data['cretated_at']) ? $data['cretated_at'] : '',
            'param' => isset($data['param']) ? $data['param'] : ''
        ]);
    }
    public function add()
    {

        $id = '';
        $penjaminan = '';
        $tmrs_id = '';
        $penjaminan = '';
        $tgl_konsul = '';
        $jadwal_dokter_id = '';
        $no_rek_medis = '';
        $nama_lengkap = '';
        $tgl_lahir = '';
        $jk = '';
        $hp = '';
        $email = '';
        $ktp_passpor = '';
        $updated_at = '';
        $cretated_at = '';
        $this->load->view('cms/kosul/form', [
            'id' => '',
            'action' => base_url('konsuladmin/store/'),
            'tmrs_id' => '',
            'penjaminan' => '',
            'tgl_konsul' => '',
            'penjaminan' => '',
            'jadwal_dokter_id' => '',
            'no_rek_medis' => '',
            'nama_lengkap' => '',
            'tgl_lahir' => '',
            'jk' => '',
            'hp' => '',
            'email' => '',
            'ktp_passpor' => '',
            'updated_at' => '',
            'cretated_at' => '',
        ]);
    }

    public function store()
    {

        $id  = $this->input->post('id');
        try {
            $this->db->insert(
                'tmkonsultasi',
                [
                    'id' => $this->input->post('id'),
                    'tmrs_id' => $this->input->post('tmrs_id'),
                    'penjaminan' => $this->input->post('penjaminan'),
                    'tgl_konsul' => $this->input->post('tgl_konsul'),
                    'jadwal_dokter_id' => $this->input->post('jadwal_dokter_id'),
                    'no_rek_medis' => $this->input->post('no_rek_medis'),
                    'nama_lengkap' => $this->input->post('nama_lengkap'),
                    'tgl_lahir' => $this->input->post('tgl_lahir'),
                    'jk' => $this->input->post('jk'),
                    'hp' => $this->input->post('hp'),
                    'email' => $this->input->post('email'),
                    'ktp_passpor' => $this->input->post('ktp_passpor'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'cretated_at' => date('Y-m-d H:i:s'),
                ],
            );
            $this->status_json(1);
        } catch (\Throwable $th) {
            $this->status_json(2);
        }
    }
    function doprocess($id)
    {

        echo 'ss';
        // $data = $this->db->get_where('');
    }
    public function edit($id)
    {
        $row = $this->db->get_where('tmkonsultasi', ['id' => $id]);
        if ($row->num_rows() < 0) {
            show_404();
            exit();
        }
        $data = $row->row_array();
        $id = $data['id'];
        $tmrs_id = $data['tmrs_id'];
        $penjaminan = $data['penjaminan'];
        $tgl_konsul = $data['tgl_konsul'];
        $jadwal_dokter_id = $data['jadwal_dokter_id'];
        $no_rek_medis = $data['no_rek_medis'];
        $nama_lengkap = $data['nama_lengkap'];
        $tgl_lahir = $data['tgl_lahir'];
        $jk = $data['jk'];
        $hp = $data['hp'];
        $email = $data['email'];
        $ktp_passpor = $data['ktp_passpor'];
        $updated_at = $data['updated_at'];
        $cretated_at = $data['cretated_at'];

        $this->load->view('cms/konsul/form', [
            'id' => $id,
            'action' => base_url('konsuladmin/update/' . $id),
            'tmrs_id' => $tmrs_id,
            'penjaminan' => $penjaminan,
            'tgl_konsul' => $tgl_konsul,
            'jadwal_dokter_id' => $jadwal_dokter_id,
            'no_rek_medis' => $no_rek_medis,
            'nama_lengkap' => $nama_lengkap,
            'tgl_lahir' => $tgl_lahir,
            'jk' => $jk,
            'hp' => $hp,
            'email' => $email,
            'ktp_passpor' => $ktp_passpor,
            'updated_at' => $updated_at,
            'cretated_at' => $cretated_at,
        ]);
    }

    public function update($id)
    {
        $id  = $this->input->post('id');
        try {
            $this->db->update(
                'tmkonsultasi',
                [
                    [
                        'id' => $this->input->post('id'),
                        'tmrs_id' => $this->input->post('tmrs_id'),
                        'penjaminan' => $this->input->post('penjaminan'),
                        'tgl_konsul' => $this->input->post('tgl_konsul'),
                        'jadwal_dokter_id' => $this->input->post('jadwal_dokter_id'),
                        'no_rek_medis' => $this->input->post('no_rek_medis'),
                        'nama_lengkap' => $this->input->post('nama_lengkap'),
                        'tgl_lahir' => $this->input->post('tgl_lahir'),
                        'jk' => $this->input->post('jk'),
                        'hp' => $this->input->post('hp'),
                        'email' => $this->input->post('email'),
                        'ktp_passpor' => $this->input->post('ktp_passpor'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'cretated_at' => date('Y-m-d H:i:s'),

                    ],
                ],
                [
                    'id' => $id
                ]
            );
            $this->status_json(1);
        } catch (\Throwable $th) {
            $this->status_json(2);
        }
    }

    private function status_json($kode)
    {
        if ($kode == 1) {
            echo json_encode(array('data' => array(
                'code' => 1,
                'message' => 'data berhasil disimpan'
            )));
        } else {
            echo json_encode(array('data' => array(
                'code' => 2,
                'message' => 'Data gagal di simpan',
            )));
        }
    }

    public function destroy($id)
    {
        $id  = $this->input->post('id');
        try {
            $this->db->delete('tmkonsultasi', [
                'id' => $id
            ]);
            echo json_encode(array('data' => array(
                'code' => 1,
                'message' => 'data berhasil di hapus'
            )));
        } catch (\Throwable $th) {
            $this->status_json(2);
        }
    }
}
