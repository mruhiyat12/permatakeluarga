<?php
ini_set('date.timezone', 'Asia/Jakarta');
class KonsulController extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('frontmodel');
        // $this->load->model('frontmodel');
        $this->load->library('pagination');
    }

    public function telekonsul($id = '')
    {
        $par = isset($_GET['par']) ? $_GET['par'] : '';
        $rs = ($this->uri->segment(2)) ?  $this->uri->segment(2) : 'lippo';
        $id_rs = $this->frontmodel->switch_uri($rs);
        $rsdatanya = $this->db->get_where('rumah_sakit', [
            'id_rumahsakit' => $id_rs ? $id_rs : 1,
        ])->row_array();
        $data = array();
        $data['dokter'] = $this->frontmodel->get_limit_dokter($id_rs, 3);
        $data['rmahsakit']  = $this->db->get('rumah_sakit');
        $data['id_rs'] = $id_rs ? $id_rs : 1;
        $data['rs'] = $rs;
        if ($par == 'janji_dokter') {
            $rjudul = 'Janji Dokter';
        } else if ($par == 'telekonsultasi') {
            $rjudul = 'Telekonsultasi online';
        } else {
            $rjudul = 'Polikilinik Rawat Jalan';
        }

        $data['judul'] = $rjudul;
        $data['clinic'] =  $this->db->query('SELECT DISTINCT
        bagian.nama_bagian, 
        bagian.id_bagian
    FROM
        bagian
        INNER JOIN
        dokter
        ON 
            bagian.id_bagian = dokter.id_bagian');
        $data['rsname'] = $rsdatanya['nama_rumahsakit'];

        $this->template->load('layouts/template', 'telekonsul/index', $data);
    }

    // action get detail jadawl dokter

    public function getdetailDokter()
    {

        $par = isset($_GET['par']) ? $_GET['par'] : '';

        $rumahsakitid = $this->input->post('rmahsakitid');
        $klinik = $this->input->post('klinik');
        $penjaminanid = $this->input->post('penjaminanid');
        $tgl_konsult = $this->input->post('tgl_konsultasi');

        // search of date from where instead 
        $cday =  date('l', strtotime($tgl_konsult));
        $hari =  $this->converdate($cday);
        $params = [
            'hari' => $hari,
            'bidang' => $klinik,
            'rumahsakit_id' => $rumahsakitid
        ];
        switch ($rumahsakitid) {
            case 1:
                $rs = 'lippo';
                break;
            case 2:
                $rs = 'jababeka';
                break;
            case 4:
                $rs = 'galuhmas';
                break;
            default:
                $rs = 'lippo';
                break;
        }
        $id_rs = $this->frontmodel->switch_uri($rs);
        $rsdata = $this->db->get_where('rumah_sakit', [
            'id_rumahsakit' => $id_rs ? $id_rs : 1
        ])->row_array();

        $data['rsname'] = $rsdata['nama_rumahsakit'];

        $data['jadwal_dokter'] = $this->frontmodel->detailjadwal($params);
        $data['jadwal_dokter_oncall'] = $this->frontmodel->get_jadwal_dokter_oncall($id_rs);
        $data['par'] = $par;
        $data['dokter'] = $this->frontmodel->get_limit_dokter($id_rs, 3);
        $data['rmahsakit']  = $this->db->get('rumah_sakit');
        $data['id_rs'] = $id_rs;
        $data['rs'] = $rs;
        $data['clinic'] =  $this->db->query('SELECT DISTINCT
        bagian.nama_bagian, 
        bagian.id_bagian as id
    FROM
        bagian
        INNER JOIN
        dokter
        ON 
            bagian.id_bagian = dokter.id_bagian');

        $data['hari'] = $hari;
        $data['rs'] = $rs;
        if ($par == 'janji_dokter') {
            $rjudul = 'Janji Dokter';
            $status = 1;
        } else if ($par == 'telekonsultasi') {
            $rjudul = 'Telekonsultasi online';
            $status = 2;
        } else {
            $rjudul = 'Polikilinik Rawat';
            $status = 3;
        }
        $data['jenis_konsultasi'] = $status;
        $data['judulk'] = $rjudul;
        $data['rumah_sakit_id'] = ($rumahsakitid) ? $rumahsakitid : 1;
        $this->template->load('layouts/template', 'telekonsul/pilihdokter', $data);
        // '1' => 'Telekonsultasi Dokter',
        // '2' => 'Konsultasi Online',
        // '3' => 'Telekonsultassi' 
    }
    //get api data 
    public function apidokter()
    {

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        };
        $rumahsakitid = $this->input->get('rmahsakitid');
        $klinik = $this->input->get('klinik');
        $penjaminanid = $this->input->get('penjaminanid');
        $tgl_konsult = $this->input->get('tgl_konsultasi');

        // search of date from where instead 
        $cday =  date('l', strtotime($tgl_konsult));
        $hari =  $this->converdate($cday);
        $params = [
            'hari' => $hari,
            'bidang' => $klinik,
            'rumahsakit_id' => $rumahsakitid
        ];
        switch ($rumahsakitid) {
            case 1:
                $rs = 'lippo';
                break;
            case 2:
                $rs = 'jababeka';
                break;
            case 4:
                $rs = 'galuhmas';
                break;
        }

        $id_rs = $this->frontmodel->switch_uri($rs);
        $data['jadwal_dokter'] = $this->frontmodel->detailjadwal($params);
        $data['jadwal_dokter_oncall'] = $this->frontmodel->get_jadwal_dokter_oncall($id_rs);
        $data['dokter'] = $this->frontmodel->get_limit_dokter($id_rs, 3);
        $data['rmahsakit']  = $this->db->get('rumah_sakit');
        $data['id_rs'] = $id_rs;
        $data['rs'] = $rs;
        $data['clinic'] =  $this->db->get('tmspesialis');
        $data['hari'] = $hari;
        $data['rs'] = $rs;

        $this->load->view('telekonsul/pilihdokter_api', $data);
    }



    // action buat janji dokter
    public function insertjanji()
    {
        try {
            // var_dump(count($_FILES));
            // exit; 
            $fld = 'assets/image/konsultasi/';
            $config['upload_path'] = $fld;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG';
            $config['max_size'] = '10000';
            $this->load->library('upload', $config);

            $error  = [];
            $files  = [];
            if (count($_FILES) == '') {
                $parstat = $this->input->post('par_stat');
                $this->db->insert('tmkonsultasi', [
                    'tmrs_id' => $this->input->post('rs_id'),
                    'penjaminan' => ($this->input->post('penjaminan')) ? $this->input->post('penjaminan') : 'kosong',
                    'tgl_konsul' => $this->input->post('tgl_konsul'),
                    'jadwal_dokter_id' => $this->input->post('jadwalid'),
                    'no_rek_medis' => $this->input->post('medrec'),
                    'nama_lengkap' => $this->input->post('nama_pasien'),
                    'tgl_lahir' => $this->input->post('tgl_lahir'),
                    'jk' => $this->input->post('jenis_kelamin'),
                    'hp' => $this->input->post('whatsapp'),
                    'email' => $this->input->post('email'),
                    'alamat' => $this->input->post('alamat'),
                    'jenis_konsultasi' => $this->input->post('jenis_konsultasi'),
                    'rumah_sakit_id' => $this->input->post('rumah_sakit_id')
                ]);
                $id  = $this->db->select('max(id) as idnya')->from('tmkonsultasi')->get();
                echo json_encode([
                    'id' => $id->row()->idnya,
                    'status' => 1,
                    'par' => $parstat,
                    'msg' => 'Data berhasil di simpan'
                ]);
                die;
            } else {
                for ($j = 1; $j <= count($_FILES); $j++) {
                    if (!$this->upload->do_upload('file' . $j)) {
                        $error[] = $this->upload->display_errors();
                    } else {
                        $files[] = $_FILES['file' . $j]['name'];
                    }
                }
            }

            $parstat = $this->input->post('par_stat');
            $this->db->insert('tmkonsultasi', [
                'tmrs_id' => $this->input->post('rs_id'),
                'penjaminan' => ($this->input->post('penjaminan')) ? $this->input->post('penjaminan') : 'kosong',
                'tgl_konsul' => $this->input->post('tgl_konsul'),
                'jadwal_dokter_id' => $this->input->post('jadwalid'),
                'no_rek_medis' => $this->input->post('medrec'),
                'nama_lengkap' => $this->input->post('nama_pasien'),
                'tgl_lahir' => $this->input->post('tgl_lahir'),
                'jk' => $this->input->post('jenis_kelamin'),
                'hp' => $this->input->post('whatsapp'),
                'email' => $this->input->post('email'),
                'alamat' => $this->input->post('alamat'),
                'ktp_passpor ' => isset($files[0]) ? $files[0] : '',
                'bukti_bayar ' => isset($files[1]) ? $files[1] : '',
                'jenis_konsultasi' => $this->input->post('jenis_konsultasi'),
                // 'dokter_id'
                'rumah_sakit_id' => $this->input->post('rumah_sakit_id')
            ]);
            $id  = $this->db->select('max(id) as idnya')->from('tmkonsultasi')->get();
            echo json_encode([
                'id' => $id->row()->idnya,
                'status' => 1,
                'par' => $parstat,
                'msg' => 'Data berhasil di simpan'
            ]);
            die;
        } catch (\Throwable $th) {
            //throw $th; 
            echo json_encode([
                'status' => 2,
                'msg' => 'data gagal di simpna karena' . $th
            ]);
        }
    }

    public function berhasildaftar($id)
    {
        if ($id != '') {
            $data = $this->db->get_where('tmkonsultasi', [
                'id' => $id
            ]);


            if ($data->num_rows() > 0) {
                $rs = ($this->uri->segment(1)) ?  $this->uri->segment(1) : 'lippo';
                $id_rs = $this->frontmodel->switch_uri($rs);
                $fid =  $this->db->escape($id);
                $konsultdata = $this->db->select('
                tmkonsultasi.nama_lengkap, 
                tmkonsultasi.tgl_lahir, 
                tmkonsultasi.jk, 
                tmkonsultasi.hp, 
                tmkonsultasi.email, 
                tmkonsultasi.alamat as alamatpas, 
                tmkonsultasi.bukti_bayar,    
                dokter.nama_dokter, 
                dokter.img, 
                dokter.alamat, 
                tmkonsultasi.no_rek_medis, 
                tmkonsultasi.ktp_passpor,
                dokter.id_rumahsakit,

                bagian.nama_bagian,
                rumah_sakit.nama_rumahsakit

                 ')
                    ->from('tmkonsultasi')
                    ->join('jadwal_dokter', 'tmkonsultasi.jadwal_dokter_id = jadwal_dokter.id_jadwal', 'left')
                    ->join('dokter', 'jadwal_dokter.id_dokter = dokter.id_dokter', 'left')
                    ->join('bagian', 'bagian.id_bagian = dokter.id_bagian', 'left')
                    ->join('rumah_sakit', 'rumah_sakit.id_rumahsakit = dokter.id_rumahsakit', 'left')

                    ->where('tmkonsultasi.id', $id)
                    ->get()->row();
                $x['konsultdata'] = $konsultdata;
                $x['rs'] = $id_rs;
                $x['data'] = $data->row();
                $x['params'] = $id;


                // cek parameter jika login
                // if($this->session->usedata(''))
                // var_dump($_SESSION);
                if ($this->session->userdata('_USER_ID') != '') {
                }
                $print = isset($_GET['print']) ? $_GET['print'] : '';
                $x['print'] = $print;
                if ($print == 'yes') {
                    $this->load->view('telekonsul/berhasil_print', $x);
                } else {
                    $this->template->load('layouts/template', 'telekonsul/berhasil', $x);
                }
            } else {
                echo json_encode([
                    'status' => 2,
                    'msg' => 'data tidak di temukan'
                ]);
            }
        }
    }

    public function doprocess()
    {
        $parameter_id = $this->input->post('parameter_id');
        date_default_timezone_set('Asia/Jakarta');
        $jam = date("G");
        if ($jam >= 0 && $jam <= 11)
            $sapa = "Selamat Pagi.";
        else if ($jam >= 12 && $jam <= 15)
            $sapa = "Selamat Siang.";
        else if ($jam >= 16 && $jam <= 18)
            $sapa = "Selamat Sore.";
        else if ($jam >= 19 && $jam <= 23)
            $sapa = "Selamat Malam.";

        $konsultdata = $this->db->select('
            tmkonsultasi.nama_lengkap, 
            tmkonsultasi.tgl_lahir, 
            tmkonsultasi.jk, 
            tmkonsultasi.hp, 
            tmkonsultasi.email, 
            dokter.nama_dokter, 
            dokter.img, 
            dokter.alamat, 
            tmkonsultasi.no_rek_medis, 
            tmkonsultasi.ktp_passpor,
            dokter.id_rumahsakit
            
          
            ')
            ->from('tmkonsultasi')
            ->join('jadwal_dokter', 'tmkonsultasi.jadwal_dokter_id = jadwal_dokter.id_jadwal', 'left')
            ->join('dokter', 'jadwal_dokter.id_dokter = dokter.id_dokter', 'left')
            ->where('tmkonsultasi.id', $parameter_id)
            ->get()->row();
        $this->db->update('tmkonsultasi', [
            'status_con' => 2
        ], ['id' => $parameter_id]);
    }
    private function converdate($dayname)
    {
        $par = [

            'Senin' => 'Monday',
            'Selasa' => 'Tuesday',
            'Rabu' => 'Wednesday',
            'Kamis' => 'Thursday',
            'Jumat' => 'Friday',
            'Sabtu' => 'Saturday',
            'Minggu' => 'Sunday'
        ];
        $f = array_search($dayname, $par);
        return $f;
    }
    //after clicked data 
    public function _doprocess()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        };
    }
    public function layanan($id_rumahsakit = null, $id_article = null)
    {
        $data['judulk'] = 'Layanann gawat darurat';
        $rs = ($this->uri->segment(2)) ?  $this->uri->segment(2) : 'lippo';
        $id_rs = $this->frontmodel->switch_uri($rs);
        $dataquery = $this->db->query('
        SELECT
 
	rumah_sakit.nama_rumahsakit, 
	setting.name_set, 
	setting.value_set, 
	setting.description, 
	setting.is_active, 
	setting.is_system, 
	setting.is_removeable
FROM
	setting
	INNER JOIN
	rumah_sakit
	ON 
    setting.id_rumahsakit = rumah_sakit.id_rumahsakit 
    GROUP BY setting.id_rumahsakit
        ');
        $rs = ($this->uri->segment(2)) ?  $this->uri->segment(2) : 'lippo';
        $data['rs'] = $rs;
        $data['data'] = $this->db->get('rumah_sakit');

        $data['lisrs'] = $dataquery;

        $this->template->load('layouts/template', 'telekonsul/layananagawatdarurat', $data);
    }
}
