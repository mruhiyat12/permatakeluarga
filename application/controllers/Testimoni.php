<?php


class Testimoni extends CI_controller
{


    public function __construc()
    {
        parent::__construct();
    }

    public function index($param = null)
    {

        $data = $this->db->get_where('tmtestimoni', [
            'id' => $param,
        ])->row_array();

        $action = ($param == null) ? base_url('testimoni/store') : base_url('testimoni/update/' . $param);
        $this->load->view('/cms/testimoni/index', [
            'title' => "Tambah Data testimoni",
            'action' => $action,
            'id' => '',
            'isi' => isset($data['isi']) ? $data['isi'] : '',
            'gambar' => isset($data['gambar']) ?  $data['gambar'] : '',
            'user_id' => isset($data['user_id']) ? $data['user_id']  : '',
            'param' => $param,
            'created_at' => '',
            'updated_at' => '',
        ]);
    }
    public function create()
    {
        $this->load->view('/cms/testimoni/form', [
            'title' => "Tambah Data testimoni",
            'action' => base_url('testimoni/store'),
            'id' => '',
            'isi' => '',
            'gambar' => '',
            'user_id' => '',
            'created_at' => '',
            'updated_at' => '',
        ]);
    }

    public function store()
    {
        try {

            $fld = 'assets/image/testimoni/';
            $config['upload_path'] = $fld;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG';
            $config['max_size'] = '10000';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('gambar')) {
                // http_response_code(500);
                $message = $this->upload->display_errors();
                echo json_encode(['data' => [
                    'code' => 2,
                    'message' => $message
                ]]);
            } else {
                $this->db->insert('tmtestimoni', [
                    'isi' => $this->input->post('isi'),
                    'gambar' => $this->upload->file_name,
                    'user_id' => $this->session->userdata('_USER_ID'),
                    'id_rs' => $this->session->userdata('RS_ID'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                echo json_encode(['data' => [
                    'code' => 1,
                    'message' => 'data berhasil di simpan'
                ]]);
            }
        } catch (\Throwable $th) {
            echo json_encode(['data' => [
                'code' => 2,
                'message' => 'data gagal di simpan' . $th
            ]]);
            // echo json_encode($response);
        }
    }
    public function edit($id)
    {
        $r = $this->db->get_where('tmtestimoni', [
            'id' => $id
        ]);


        $data = $r->row_array();


        $this->load->view('cms/testimoni/form', [
            'action' => base_url('testimoni/update/' . $id),
            'title' => "Tambah Data testimoni",
            'action' => base_url('update/' . $data['id']),
            'id' =>  $data['id'],
            'isi' =>  $data['isi'],
            'gambar' =>  $data['gambar'],
            'user_id' =>  $data['user_id'],
            'created_at' =>  $data['created_at'],
            'updated_at' =>  $data['updated_at'],
        ]);
    }
    public function update($id)
    {
        if ($id != '') {
            try {
                $fld = 'assets/image/testimoni/';
                $dataas = $this->db->get_where('tmtestimoni', [
                    'id' => $id
                ])->row_array();

                if (file_exists($fld . $dataas['gambar'])) {
                    @unlink($fld . $dataas['gambar']);
                }
                $config['upload_path'] = $fld;
                $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG';
                $config['max_size'] = '10000';


                if ($_FILES['gambar']['name'] == '') {
                    $this->db->update('tmtestimoni', [
                        'isi' => strip_tags($this->input->post('isi')),
                        // 'gambar' => $this->upload->file_name,
                        'id_rs' => $this->session->userdata('RS_ID'),
                        'user_id' => $this->session->userdata('_USER_ID'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ], [
                        'id' => $id
                    ]);
                    echo json_encode(['data' => [
                        'code' => 1,
                        'message' => 'data berhasil di simpan'
                    ]]);
                } else {
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('gambar')) {
                        // http_response_code(500);
                        $message = $this->upload->display_errors();
                        echo json_encode([
                            'code' => 2,
                            'message' => $message
                        ]);
                    } else {
                        $this->db->update('tmtestimoni', [
                            'isi' => strip_tags($this->input->post('isi')),
                            'gambar' => $this->upload->file_name,
                            'user_id' => $this->session->userdata('_USER_ID'),
                            'id_rs' => $this->session->userdata('RS_ID'),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ], [
                            'id' => $id
                        ]);
                        echo json_encode(['data' => [
                            'code' => 1,
                            'message' => 'data berhasil di simpan'
                        ]]);
                    }
                }
            } catch (\Throwable $th) {
                http_response_code(400);
                echo json_encode(['data' => [
                    'code' => 2,
                    'message' => 'data gagal di simpan' . $th
                ]]);
            }
        }
    }
    public function destroy($id)
    {
        $fld = 'assets/image/testimoni/';
        $dataas = $this->db->get_where('tmtestimoni', [
            'id' => $id
        ])->row_array();
        if (file_exists($fld . $dataas['gambar'])) {
            @unlink($fld . $dataas['gambar']);
        }
        $this->db->delete('tmtestimoni', [
            'id' => $id
        ]);
        echo json_encode(['data' => [
            'code' => 1,
            'message' => 'data berhasil di hapus'
        ]]);
    }
}
