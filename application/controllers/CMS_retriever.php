<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CMS_retriever extends CI_Controller
{
	private $activeSession; // store session
	private $rs_id;

	public function __construct()
	{
		parent::__construct();
		$this->activeSession = $this->session->userdata('_USER_ID');
		$this->rs_id = $this->session->userdata('RS_ID');
	}

	public function index()
	{
		redirect(site_url('view/home'));
	}


	public function list_user()
	{
		return $this->db->query('
		SELECT
	m_user.rumahsakit_id, 
	m_user.username, 
	m_user.`password`, 
	m_user.`name`, 
	m_user.email, 
	m_user.phone, 
	m_user.`level`, 
	m_user.is_active, 
	m_user.img, 
	m_user.position, 
	m_user.user_id, 
	rumah_sakit.id_rumahsakit, 
	rumah_sakit.nama_rumahsakit, 
	rumah_sakit.alamat, 
	rumah_sakit.is_active
FROM
	m_user
	LEFT OUTER JOIN
	rumah_sakit
	ON 
		m_user.rumahsakit_id = rumah_sakit.id_rumahsakit
		');
	}
	/*
	* read object
	*/
	public function record($specific = null)
	{
		/*
		* code info:
		*	- 0 = akses tidak sah & data tidak perlu di tampilkan
		*	- 1 = akses sah & data di tampilkan
		*/
		$code = 0;
		$object = null;

		if ($this->activeSession != null) {
			switch ($this->input->post('model-input')) {
				case 'user':
					$query['table'] = $this->list_user();
					break;
				case 'bagian':
					$query['table'] = 'v_bagian';
					break;
				case 'articlecategory':
					$query['table'] = 'article_category';
					break;
				case 'article':
					$query['table'] = 'v_article';
					break;
				case 'gallery':
					$query['table'] = 'v_gallery';
					break;
				case 'guestbook':
					$query['table'] = 'v_guestbook';
					break;
				case 'dokter':
					$query['table'] = 'v_dokter';
					break;
				case 'coment':
					$query['table'] = 'v_comment';
					break;
				case 'gallery_detail':
					$query['table'] = 'v_gallery';
					break;
				case 'slide':
					$query['table'] = 'v_slide';
					break;

				case 'jadwaldokter':
					$query['table'] = 'v_jadwaldokter';
					break;

				default:
					$query['table'] = $this->input->post('model-input');
					break;
			}

			$query['where'] = array($this->input->post('key-input') => $this->input->post('value-input'));

			$object = $this->model->getRecord($query);


			$code = 1;
		}

		echo json_encode(array('data' => array(
			'code' => $code,
			'object' => $object
		)));
	}

	/* |||||||||||||||||||||||||||||||||||| DATATABLES |||||||||||||||||||||||||||||||||||| */
	/*
	* read objects - DataTables
	*/
	public function records($table, $key = 'null', $value = 'null', $picker = 'no')
	{
		$data = array();

		if ($this->activeSession != null) {
			if (isset($table)) {
				if ($key != 'null' && $value != 'null') {
					$query['where'] = array($key => $value);
				}

				switch ($table) {
					case 'articlecategory':
						$query['table'] = 'article_category';
						break;
					case 'user':
						$query['table'] =  'v_user_list'; //		break;
					case 'article':
						$query['table'] = 'v_article';
						break;
					case 'ebook_category':
						$query['table'] = 'ebook_category';
						break;
					case 'ebook':
						$query['table'] = 'v_ebook';
						break;
					case 'download':
						$query['table'] = 'v_download';
						break;
					case 'rumah_sakit':
						$query['table'] = 'rumah_sakit';
						break;
					case 'dokter':
						$query['table'] = 'v_dokter';
						break;
					case 'guestbook':
						$query['table'] = 'v_guestbook';
						break;
					case 'jadwaldokter':
						$query['table'] = 'v_jadwaldokter';
						break;
					case 'setting':
						$query['table'] = 'v_setting';
						break;
					case 'gallery':
						$query['table'] = 'v_gallery_header';
						break;
					case 'slide':
						$query['table'] = 'v_slide';
						break;
					case 'bagian':
						$query['table'] = 'v_bagian';
						break;
						// addded by ismarianto 
					case 'telekonsultasi':
						$query['table'] = 'v_telekonsultasi';
						break;
					case 'jadwal_dokter':
						$query['table'] = 'v_jadwal_dokter';
						break;
					case 'testimoni':
						$query['table'] = 'v_testimoni';
						break;
					default:
						$query['table'] = $table;
						break;
				}
				// die;   
				// if ($table == 'jadwaldokter') {
				// 	$rs_idnya = $this->session->userdata('RS_ID');
				// 	$where = array('where' => array('id_rumahsakit' => $rs_idnya));
				// 	$qrs = array_merge($where, $query); 

				// 	$records = $this->model->getList($qrs);
				// 	$inner = '_' . $table;
				// 	$data = $this->$inner($records, $picker);
				// }else

				if ($table == 'article') {
					$rs_idnya = $this->session->userdata('RS_ID');
					$where = array('where' => array('id_rumahsakit' => $rs_idnya));
					$qrs = array_merge($where, $query);

					$records = $this->model->getList($qrs);
					$inner = '_' . $table;
					$data = $this->$inner($records, $picker);
				} else if ($table == 'telekonsultasi') {
					// konsultasi id nya berapa;
					$konsul_id = $this->input->post('konsul_id');
					$rs_idnya = $this->session->userdata('RS_ID');
					$where = array('where' => array('tmrs_id' => $rs_idnya));
					if ($konsul_id != '') {
						$where = array('where' => array('status_id' => $konsul_id));
					}
					$qrs = array_merge($where, $query);
					$records = $this->model->getList($qrs);
					$inner = '_' . $table;
					$data = $this->$inner($records, $picker);
				} else if ($table == 'testimoni') {
					$rs_idnya = $this->session->userdata('RS_ID');
					$where = array('where' => array('rumahsakit_id' => $rs_idnya));
					$qrs = array_merge($where, $query);

					$records = $this->model->getList($qrs);
					$inner = '_' . $table;
					$data = $this->$inner($records, $picker);
				} else {
					$records = $this->model->getList($query);
					$inner = '_' . $table;
					$data = $this->$inner($records, $picker);
				}
			}
		}
		echo json_encode(array('data' => $data));
	}

	/*
	* inner data generator
	* ===================================== write your custom function here =====================================
	*/


	public function _testimoni($records, $picker = 'no')
	{
		$data = array();
		$no = 1;
		$id_rs = $this->session->userdata('RS_ID');

		$records = $this->db->select('*')->from('v_testimoni')->where('rumahsakit_id', $id_rs)->get()->result();
		foreach ($records as $record) {

			$linkBtn = ' <a href="#' . $record->id . '" class="btn btn-xs btn-primary editBtn" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-edit"></i> Edit</a>';

			$linkBtn .= ' <a href="#' . $record->id . '" class="btn btn-xs btn-danger removeBtn" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash"></i> Delete</a>';

			// $gambar = (file_exists('./assets/image/testimoni/' . $record->gambar)) ?  'assets/image/testimoni/' . $records->gambar : '';
			$namars = ($record->rumahsakit_id) ? $record->nama_rumahsakit : 'Kosong';
			$data[] = array(
				'no' => $no,
				'gbr' => '<img src="' . base_url('/assets/image/testimoni/' . $record->gambar) . '" onerror="this.onerror=null;this.src=\'https://e7.pngegg.com/pngimages/319/145/png-clipart-http-404-user-interface-design-design-purple-text.png\';" class="img-responsive" style="width:100px;height:100px" >',
				'isi' => substr(strip_tags($record->isi), 0, 10),
				'rumahsakit' => strip_tags($namars),
				'user' => $record->name,
				'aksi' => $linkBtn
			);
			$no++;
		}
		return $data;
	}

	public function _telekonsultasi($records, $picker = 'no')
	{

		$row = array();
		$no = 1;

		$id_rs = $this->session->userdata('RS_ID');
		$records = $this->db->get_where('v_telekonsultasi', ['id_rumahsakit' => $id_rs])->result();

		foreach ($records as $data) {

			$linkBtn = ' <a href="#' . $data->id . '" class="btn btn-xs btn-primary editBtn" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-edit"></i> Edit</a>';
			$linkBtn .= ' <a href="#' . $data->id . '" class="btn btn-xs btn-danger removeBtn" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash"></i> Delete</a>';

			if ($data->status_id == 1) {
				$linkBtn .= ' <a href="#' . $data->id . '#' . sha1('RIAN' . date($data->id) . 'RIAN') . '?par=janji_dokter" class="btn btn-xs btn-info" onclick="javascript:confirmApprove(\'' . $data->id . '\',\'#' . sha1('RIAN' . date($data->id) . 'RIAN') .  '?par=janji_dokter\')" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash"></i> Accept</a>';
			} else if ($data->status_id == 2) {
				$linkBtn .= ' <a href="#' . $data->id . '#' . sha1('RIAN' . date($data->id) . 'RIAN') . '" class="btn btn-xs btn-info" onclick="javascript:confirmApprove(\'' . $data->id . '\',\'#' . sha1('RIAN' . date($data->id) . 'RIAN') .  '\')" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash"></i> Accept</a>';
			} else {
				$linkBtn .= ' <a href="#' . $data->id . '#' . sha1('RIAN' . date($data->id) . 'RIAN') . '" class="btn btn-xs btn-info" onclick="javascript:confirmApprove(\'' . $data->id . '\',\'#' . sha1('RIAN' . date($data->id) . 'RIAN') .  '\')" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash"></i> Accept</a>';
			}

			$row[] = array(
				'no' => $no,
				'penjaminan' => $data->penjaminan,
				'tgl_konsul' => $data->tgl_konsul,
				'no_rek_medis' => $data->no_rek_medis,
				'nama_lengkap' => $data->nama_lengkap,
				'tgl_lahir' => $data->tgl_lahir,
				'jenis_konsultasi' => $data->jenis_konsultasi,
				'gbr' => '<img src="' . base_url('assets/image/konsultasi/' . $data->ktp_passpor) . '" class="img-responsive" style="width:100px;height:100px" onerror="this.onerror=null;this.src=\'https://e7.pngegg.com/pngimages/319/145/png-clipart-http-404-user-interface-design-design-purple-text.png\';">',
				'bukti_bayar' => '<a href="' . base_url('assets/image/konsultasi/' . $data->bukti_bayar) . '" target="_blank"><img src="' . base_url('assets/image/konsultasi/' . $data->bukti_bayar) . '" class="img-responsive" style="width:10 0px;height:100px" onerror="this.onerror=null;this.src=\'https://e7.pngegg.com/pngimages/319/145/png-clipart-http-404-user-interface-design-design-purple-text.png\';"></a>',
				'jk' => $data->jk,
				'hp' => $data->hp,
				'email' => $data->email,
				'nama_rumahsakit' => $data->nama_rumahsakit,
				'alamat' => $data->alamat,
				'aksi' => $linkBtn,
				'statusnya' => $data->statusnya
			);
			$no++;
		}

		return $row;
	}

	// added by imarianto 

	function _articlecategory($records, $picker = 'no')
	{
		$data = array();

		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->article_category_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->article_category_id . '" class="btn btn-xs btn-primary editBtn" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-edit"></i> Edit</a>';
				//is removeable
				if ($record->is_removeable == 1) {
					$linkBtn .= ' <a href="#' . $record->article_category_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash-o"></i> Hapus</a>';
				}
			}
			$data[] = array(
				'category_name' => $record->category_name,
				'description' => $record->description,
				'is_active' => statusData($record->is_active),
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _article($records, $picker = 'no')
	{
		$data = array();
		$records = $this->db->get_where('v_article', [
			'id_rumahsakit' => $this->rs_id
		])->result();
		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->article_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->article_id . '" class="btn btn-xs btn-info viewBtn" title="View"><i class="fa fa-eye"></i> View</a>';
				$linkBtn .= ' <a href="#' . $record->article_id . '" class="btn btn-xs btn-primary editBtn" title="Edit"><i class="fa fa-edit"></i> Edit</a>';
				$linkBtn .= ' <a href="#' . $record->article_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"><i class="fa fa-trash-o"></i> Hapus</a>';
			}
			$data[] = array(
				'title' => $record->title,
				'date' => $record->created_datetime,
				'name' => $record->name,
				'category' => $record->category_name,
				'nama_rumahsakit' => $record->nama_rumahsakit,
				'is_publish' => statusYN($record->is_publish),

				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _page($records, $picker = 'no')
	{
		$data = array();

		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->page_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->page_id . '" class="btn btn-xs btn-primary editBtn" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-edit"></i> Edit</a>';
				if ($record->is_removeable == 1) {
					$linkBtn .= ' <a href="#' . $record->page_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash-o"></i> Hapus</a>';
				}
			}
			$data[] = array(
				'title' => $record->title,
				'is_publish' => statusYN($record->is_publish),
				'date' => $record->created_datetime,
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _user($records, $picker = 'no')
	{

		// $data = array();
		$fn = $this->list_user();

		foreach ($fn->result() as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->user_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->user_id . '" class="btn btn-xs btn-primary editBtn" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-edit"></i> Edit</a>';
				$linkBtn .= ' <a href="#' . $record->user_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash-o"></i> Hapus</a>';
			}
			// print_r($record->is_active);
			$data[] = array(
				'username' => $record->username,
				'name' => $record->name,
				'position' => $record->position,
				'email' => $record->email,
				'phone' => $record->phone,
				'level' => $record->level,
				'is_active' => statusData($record->is_active),
				'nama_rs' => ($record->nama_rumahsakit) ? $record->nama_rumahsakit : 'Belum di setting',
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _setting($records, $picker = 'no')
	{
		$data = array();
		$id_rs = $this->session->userdata('RS_ID');
		$records = $this->db->select('*')->from('setting')->join('rumah_sakit', 'rumah_sakit.id_rumahsakit = setting.id_rumahsakit')->where([
			'setting.id_rumahsakit' => $id_rs
		])->get()->result();

		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->setting_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				//manageable
				if ($record->is_system == 1) {
					$linkBtn = null;
				} else {
					$linkBtn = ' <a href="#' . $record->setting_id . '" class="btn btn-xs btn-primary editBtn" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-edit"></i> Edit</a>';
					if ($record->is_removeable == 1) {
						$linkBtn .= ' <a href="#' . $record->setting_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash-o"></i> Hapus</a>';
					}
				}
			}
			$data[] = array(
				'description' => $record->description,
				'name' => $record->name_set,
				'value' => $record->value_set,
				'nama_rumahsakit' => $record->nama_rumahsakit,
				'is_active' => statusYN($record->is_active),
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _guestbook($records, $picker = 'no')
	{
		$data = array();

		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->guestbook_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->guestbook_id . '" class="btn btn-xs btn-info viewBtn" title="View"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-eye"></i> View</a>';
				$linkBtn .= ' <a href="#' . $record->guestbook_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash-o"></i> Hapus</a>';
			}
			$data[] = array(
				'title' => $record->title,
				'name' => $record->name,
				'email' => $record->email,
				'type' => $record->type,
				'company' => $record->company,
				'nama_rumahsakit' => $record->nama_rumahsakit,
				'phone' => $record->phone,
				'date' => $record->created_datetime,
				'is_processed' => statusYN($record->is_processed),
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _gallery($records, $picker = 'no')
	{
		$data = array();

		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->gallery_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->gallery_id . '" class="btn btn-xs btn-info detailBtn" title="Manage Image"  style="width:90px;margin:5px 5px 5px;><i class="fa fa-image"></i> Manage Image</a>';
				$linkBtn .= ' <a href="#' . $record->gallery_id . '" class="btn btn-xs btn-primary editBtn" title="Edit"  style="width:80px;margin:5px 5px 5px;><i class="fa fa-edit"></i> Edit</a>';
				$linkBtn .= ' <a href="#' . $record->gallery_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus" style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash-o"></i> Hapus</a>';
			}
			$data[] = array(
				'gallery_name' => $record->gallery_name,
				'description' => $record->description,
				'nama_rumahsakit' => $record->nama_rumahsakit,
				'is_publish' => statusYN($record->is_publish),
				'date' => $record->created_datetime,
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _gallery_video($records, $picker = 'no')
	{
		$data = array();

		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->gallery_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->gallery_id . '" class="btn btn-xs btn-info detailBtn" title="Manage Image"><i class="fa fa-image"></i> Manage Image</a>';
				$linkBtn .= ' <a href="#' . $record->gallery_id . '" class="btn btn-xs btn-primary editBtn" title="Edit"><i class="fa fa-edit"></i> Edit</a>';
				$linkBtn .= ' <a href="#' . $record->gallery_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"><i class="fa fa-trash-o"></i> Hapus</a>';
			}
			$data[] = array(
				'gallery_name' => $record->gallery_name,
				'description' => $record->description,
				'nama_rumahsakit' => $record->nama_rumahsakit,
				'is_publish' => statusYN($record->is_publish),
				'date' => $record->created_datetime,
				'aksi' => $linkBtn
			);
		}

		return $data;
	}



	function _slide_form($records, $picker = 'no')
	{
		$data = array();

		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->slide_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->slide_id . '" class="btn btn-xs btn-info detailBtn" title="Manage Image"><i class="fa fa-image"></i> Manage Image</a>';
				$linkBtn .= ' <a href="#' . $record->coment_id . '" class="btn btn-xs btn-primary editBtn" title="Edit"><i class="fa fa-edit"></i> Edit</a>';
				$linkBtn .= ' <a href="#' . $record->slide_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"><i class="fa fa-trash-o"></i> Hapus</a>';
			}
			$data[] = array(
				'title' => $record->title,
				'description' => $record->description,
				'alt' => $record->alt,
				'nama_rumahsakit' => $record->nama_rumahsakit,
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _coment($records, $picker = 'no')
	{
		$data = array();

		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->coment_id . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				if ($record->is_approve == 1) {
					$class = "btn-warning warningBtn";
					$tittle = "unApprove";
					$icon = "fa fa-times";
				} else {
					$class = "btn-success checkBtn";
					$tittle = "Approve";
					$icon = "fa fa-check";
				}
				$linkBtn = ' <a href="#' . $record->coment_id . '" class="btn btn-xs btn-info viewBtn" title="View" style="width:80px;margin:5px 5px 5px;"><i class="fa fa-eye"></i> View</a>';
				$linkBtn .= ' <a href="#' . $record->coment_id . '" class="btn btn-xs ' . $class . ' title="' . $tittle . ' style="width:80px;margin:5px 5px 5px;"><i class="' . $icon . '"></i> ' . $tittle . '</a>';
				$linkBtn .= ' <a href="#' . $record->coment_id . '" class="btn btn-xs btn-danger removeBtn" title="Hapus" style="width:80px;margin:5px 5px 5px;><i class="fa fa-trash-o"></i> Hapus</a>';
			}
			$data[] = array(
				'name' => $record->name,
				'email' => $record->email,
				'content' => $record->content,
				'is_approve' => statusComment($record->is_approve),
				'date' => $record->created_datetime,
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _rumah_sakit($records, $picker = 'no')
	{
		$data = array();

		$id_rs = $this->session->userdata('RS_ID');


		$records = $this->db->get_where('rumah_sakit', [
			'id_rumahsakit' => $id_rs
		])->result();
		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->id_rumahsakit . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->id_rumahsakit . '" class="btn btn-xs btn-info editBtn" title="View"><i class="fa fa-edit"></i>Edit</a>';
				$linkBtn .= ' <a href="#' . $record->id_rumahsakit . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"><i class="fa fa-trash-o"></i> Hapus</a>';
			}
			$data[] = array(
				'nama_rumahsakit' => $record->nama_rumahsakit,
				'alamat' => $record->alamat,
				'is_active' => statusData($record->is_active),
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _dokter($records, $picker = 'no')
	{

		$data = array();
		$id_rs = $this->session->userdata('RS_ID');

		$records = $this->db->get_where('v_dokter', ['id_rumahsakit' => $id_rs])->result();
		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->id_dokter . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->id_dokter . '" class="btn btn-xs btn-primary editBtn" title="Edit"<i class="fa fa-edit"></i> Edit</a>';
				$linkBtn .= ' <a href="#' . $record->id_dokter . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"><i class="fa fa-trash-o"></i> Hapus</a>';
			}

			$data[] = array(
				'nama_dokter' => $record->nama_dokter,
				'bagian' => $record->bagian,
				'alamat' => $record->alamat,
				'ket' => $record->ket,
				'nama_rumahsakit' => $record->nama_rumahsakit,
				'is_active' => statusData($record->is_active),
				'aksi' => $linkBtn
			);
		}

		return $data;
	}

	function _jadwaldokter($records, $picker = 'no')
	{

		$data = array();
		$id_rs = $this->session->userdata('RS_ID');
		$records  = $this->db->query('
		    SELECT
		    dokter.id_dokter, 
			dokter.img, 
			dokter.nama_dokter, 
			dokter.alamat, 
			dokter.id_bagian, 
			dokter.tmspesialis_id, 
			dokter.id_rumahsakit, 
			dokter.ket as ketdokter, 
			dokter.is_active, 
			rumah_sakit.nama_rumahsakit, 
			jadwal_dokter.id_jadwal, 
			jadwal_dokter.hari, 
			jadwal_dokter.jam, 
			jadwal_dokter.jam_pulang, 
			jadwal_dokter.id_dokter, 
			jadwal_dokter.bagian, 
			jadwal_dokter.id_rumahsakit, 
			jadwal_dokter.ket, 
			jadwal_dokter.oncall, 
			jadwal_dokter.is_active, 

			bagian.nama_bagian as bagian ,  
			bagian.id_rumahsakit, 
			bagian.deskripsi, 
			bagian.urutan, 
			bagian.is_publish
		FROM
			dokter
			INNER JOIN
			rumah_sakit
			ON 
				dokter.id_rumahsakit = rumah_sakit.id_rumahsakit
			INNER JOIN
			jadwal_dokter
			ON 
				dokter.id_dokter = jadwal_dokter.id_dokter AND
				rumah_sakit.id_rumahsakit = jadwal_dokter.id_rumahsakit
			INNER JOIN
			bagian
			ON 
				dokter.id_bagian = bagian.id_bagian
		where dokter.id_rumahsakit =' . $id_rs);

		if ($records->num_rows() > 0) {
			foreach ($records->result() as $record) {
				if ($picker == 'yes') {
					$linkBtn = '<a href="#' . $record->id_jadwal . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
				} else if ($picker == 'no') {
					$linkBtn = ' <a href="#' . $record->id_jadwal . '" class="btn btn-xs btn-primary editBtn" title="Edit"><i class="fa fa-edit"></i>Edit</a>';
					$linkBtn .= ' <a href="#' . $record->id_jadwal . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"><i class="fa fa-trash-o"></i> Hapus</a>';
				}
				$data[] = array(
					'nama_dokter' => $record->nama_dokter,
					'bagian' => $record->bagian,
					'hari' => $record->hari,
					'jam' => $record->jam,
					'jam_pulang' => $record->jam_pulang,
					'ket' => $record->ketdokter,
					'oncall' =>  statusYN($record->oncall),
					'nama_rumahsakit' => $record->nama_rumahsakit,
					'is_active' => statusData($record->is_active),
					'aksi' => $linkBtn
				);
			}
		} else {
			$data[] = array(
				'nama_dokter' => '',
				'bagian' => '',
				'hari' => '',
				'jam' => '',
				'jam_pulang' => '',
				'ket' => '',
				'oncall' =>  '',
				'nama_rumahsakit' => '',
				'is_active' => '',
				'aksi' => ''
			);
		}
		return $data;
	}

	function _bagian($records, $picker = 'no')
	{

		$data = array();
		$id_rs = $this->session->userdata('RS_ID');
		$records = $this->db->get_where('v_bagian', ['id_rumahsakit' => $id_rs])->result();
		foreach ($records as $record) {
			if ($picker == 'yes') {
				$linkBtn = '<a href="#' . $record->id_bagian . '" class="btn btn-xs btn-info pickBtn" title="Pilih"><i class="fa fa-thumb-tack"></i> Pilih</a>';
			} else if ($picker == 'no') {
				$linkBtn = ' <a href="#' . $record->id_bagian . '" class="btn btn-xs btn-primary editBtn" title="Edit"><i class="fa fa-edit"></i>Edit</a>';
				$linkBtn .= ' <a href="#' . $record->id_bagian . '" class="btn btn-xs btn-danger removeBtn" title="Hapus"><i class="fa fa-trash-o"></i> Hapus</a>';
			}
			$data[] = array(
				'nama_bagian' => $record->nama_bagian,
				'nama_rumahsakit' => $record->nama_rumahsakit,
				'deskripsi' => $record->deskripsi,
				'urutan' => $record->urutan,
				'is_publish' => statusData($record->is_publish),
				'aksi' => $linkBtn
			);
		}

		return $data;
	}
}
