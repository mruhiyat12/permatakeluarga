<script src='<?= base_url() ?>/assets/template/js/telekonsultasi.js'></script>

<div class="wrap container-full-default">
    <div class="lokasi-klinik">

        <div class="site-bg lazy" style="display: block; background-color: #f7f8f9;">
            <div class="row judul-back">
                <h1 class="title-lokasi-home col-md-6 col-xs-12">Buat Janji</h1>
            </div>
            <div class="change-search hidden-lg hidden-md">
                <a class="btn btn-blue-change-search" data-toggle="modal" href="">Ubah Pencarian</a>
            </div>
            <div class="modal fade hidden-lg hidden-md" role="dialog" id="changesearchmodal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header modal-header-custom">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fa fa-times" style="font-size: 30px;"></i></span>
                            </button>
                        </div>
                        <div class="modal-body" style="margin-top: -10px;">
                            <div class="row changesearchbody">
                                <h2 class="text-pink">Filter</h2>
                                <div class="col-xs-12">
                                    <form id="jadwal-form" action="<?= base_url('listdokter') ?>" method="post">
                                        <!-- <div class="form-group">
                                            <div class="form-group field-modal_id_clinic required">
                                                <label class="control-label" for="modal_id_clinic">Permata Keluarga</label>
                                                

                                            </div>
                                        </div> -->
                                        <input type="hidden" name="rumahsakit_id" value="<?= $id_rs ?>" />

                                        <h4>

                                            <?= ucfirst($rsname) ?>

                                        </h4>

                                        <div class="form-group">
                                            <div class="form-group field-modal_id_specialization required">
                                                <label class="control-label" for="modal_id_specialization">Poli</label>
                                                <p class="help-block help-block-error"></p>
                                                <select class="form-control" id="kliknik" name="klinik" style="
    border-radius: 30px;    
">
                                                    <option value="">Semua Jenis Klinik</option>

                                                    <?php

                                                    foreach ($clinic->result_array() as $clinics) : ?>
                                                        <option value="<?= $clinics['id'] ?>"><?= $clinics['nama_bagian'] ?></option>
                                                    <?php endforeach; ?>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="hidden hidden-jui">2021-05-30</div>
                                            <label class="control-label" for="jadwalform-tanggal-sr">Tanggal Konsultasi</label>
                                            <div class="overlay-calendar calendar-jui">
                                                <div class="input-group date" id="jadwalform-tanggal-disp-kvdate">
                                                    <input type="text" id="w0" class="select-info-dokter hasDatepicker" readonly="">
                                                    <span class="input-group-addon kv-date-calendar" title="Select date"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <div class="form-group field-jadwalform-tanggal-sr required">
                                                    <p class="help-block help-block-error"></p>
                                                    <input type="hidden" id="jadwalform-tanggal-sr" class="form-control" name="tgl_konsul" value="30 Mei 2021">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: none" ;="">
                                            <div class="form-group field-jadwalform-nama_dokter">
                                                <label class="control-label" for="jadwalform-nama_dokter">Nama Dokter (opsional)</label>
                                                <p class="help-block help-block-error"></p>
                                                <input type="text" id="jadwalform-nama_dokter" class="form-control select-info-dokter" name="dokter" value="" placeholder="" style="">
                                            </div>
                                        </div>
                                        <div class="center-btn">
                                            <input type="hidden" name="penjamin" value="U">
                                            <button type="submit" class="btn btn-pink button-register" name="save-button">Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search-buat-janji hidden-xs hidden-sm">
                <form id="jadwal-form-md" action="<?= base_url('listdokter') ?>" method="post">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group field-jadwalform-id_clinic required">
                                <p class="help-block help-block-error"></p>
                                <select class="form-control" name="rmahsakitid" id="rmahsakitid" style="
    border-radius: 30px;
" required>
                                    <option value="">Pilih Rumah Sakit</option>
                                    <?php
                                    $j = 1;
                                    foreach ($rmahsakit->result() as $list) :
                                        echo ($list->id_rumahsakit);
                                        switch ($list->id_rumahsakit) {
                                            case 1:
                                                $fnrmahsakit = 'lippo';
                                                break;
                                            case 2:
                                                $fnrmahsakit = 'jababeka';
                                                break;
                                            case 4:
                                                $fnrmahsakit = 'galuhmas';
                                                break;
                                        }
                                        $selected = ($this->uri->segment(1) == $fnrmahsakit) ? 'selected' : '';
                                        $actv = ($rmahsakit == $fnrmahsakit) ? 'active' : "";
                                    ?>
                                        <option value="<?= $list->id_rumahsakit ?>" <?= $selected ?>><?= ucfirst(strtoupper($list->nama_rumahsakit)) ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group field-jadwalform-id_specialization required">
                                <p class="help-block help-block-error"></p>
                                <select class="form-control" id="kliknik" name="klinik" style="
    border-radius: 30px;
">
                                    <option value="">Semua jenis Klinik.</option>
                                    <?php

                                    foreach ($clinic->result_array() as $clinics) : ?>

                                        <option value="<?= $clinics['id'] ?>"><?= $clinics['nama_bagian'] ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                        </div>
                        <div class="col-md-2" style="display :none;">
                            <div class="form-group field-jadwalform-nama_dokter">
                                <p class="help-block help-block-error"></p>
                                <input type="text" id="jadwalform-nama_dokter" class="form-control search-janji" name="dokter_json" value="" placeholder="Nama Dokter">
                            </div>
                        </div>
                        <div class="col-md-2 button-klinik nomargin">
                            <button type="submit" class="btn btn-pink button-search-klinik" name="save-button">Cari</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal modal-info-dokter" tabindex="-1" role="dialog" aria-hidden="true" id="info-dokter-modal">
                <div class="modal-dialog modal-dialog-custom">
                    <div class="model-content">
                        <div class="modal-header modal-header-custom">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fa fa-times" style="font-size: 30px;"></i></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h2 class="title-lokasi-home content-gray">Detail Dokter</h2>
                            <div class="row content-detail-dokter">
                                <div class="col-md-4 col-md-push-8 col-xs-12">
                                    <div class="crop-info-dokter">
                                        <img class="foto-info-dokter lazy" id="doctor-photo" src="" style="display: inline-block;">
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-pull-4 col-xs-12">
                                    <div class="title-jk"><b>Info Dokter</b></div>
                                    <div class="content-gray klinik-jk">Nama Dokter</div>
                                    <div class="nama-jk"><b id="doctor-name"></b></div>
                                    <div class="content-gray klinik-jk">Klinik</div>
                                    <div class="nama-jk"><b id="doctor-spesialization"></b></div>
                                    <div class="content-gray klinik-jk">Rumah Sakit</div>
                                    <div class="nama-jk"><b id="doctor-clinic"></b></div>
                                </div>
                            </div>
                            <div class="row content-detail-dokter">
                                <div class="col-xs-12">
                                    <div class="title-jk" id="title-tentangdokter"><b>Tentang dokter</b></div>
                                    <div class="scroll-cv-dokter content-gray ps-active-y">
                                        <p class="text-testimoni" id="doctor-cv"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="search-result-janji">
                <div class="loading-klinik text-center card card-site hidden">
                    <div class="row">
                        <h2><span class="fa fa-sync-alt is-loading"></span> Loading...</h2>
                    </div>
                </div>
                <div id="search-result-content">

                    <?php
                    if (count($jadwal_dokter) > 0) {

                        foreach ($jadwal_dokter as $k => $v) :
                            if ($k == 0 || (isset($jadwal_dokter[$k - 1]->id_dokter)) && $jadwal_dokter[$k - 1]->id_dokter != $v->id_dokter) {

                                $bagian = $this->db->get_where('bagian', [
                                    'id_bagian' => $v->id_bagian
                                ])->result_array();
                                $nama_bagiannya = isset($bagian[0]['nama_bagian']) ? $bagian[0]['nama_bagian'] : '';
                    ?>
                                <div class="card card-site card-search" id="search-card-340">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12 dokter-search">
                                            <div class="overlay-info-dokter col-md-12 col-xs-4">
                                                <a class="info-dokter" href="" data-id="HR80000891" data-name="2400" data-spesialis="<?= $nama_bagiannya ?>" data-cv="<p><strong>Riwayat Pendidikan :</strong></p>
                                                                    <ul>-
                                                                    </ul>
                                                                    <p><strong>Riwayat Pekerjaan :</strong></p>
                                                                    <ul>-
                                                                    </ul>" data-nama-rs="<?= ucfirst($v->nama_rumahsakit) ?>" data-idspesialis="24KANK00" data-idclinic="2400" data-src-photo="<?php echo base_url(); ?>/assets/image/dokter/<?= $v->img ?>" data-nametwo="<?= strip_tags($v->nama_dokter) ?>" data-toggle="modal" data-target="#info-dokter-modal">
                                                    <div class="crop-info-dokter">
                                                        <img class="foto-info-dokter lazy" src="<?php echo base_url(); ?>/assets/image/dokter/<?= $v->img ?>" style="display: inline-block;">
                                                    </div>
                                                </a>
                                                <div class="spesialis-bar hidden-xs hidden-sm"><?= $nama_bagiannya ?></div>
                                            </div>
                                            <div class="col-md-12 col-xs-7">
                                                <div class="name-info-dokter"><b><?= strip_tags($v->nama_dokter) ?></b></div>
                                            </div>
                                            <div class="alamat-info-dokter hidden-xs hidden-sm"><b><?= ucfirst($v->nama_rumahsakit) ?></b></div>
                                        </div>
                                        <div class="col-md-9 col-xs-12 part-jadwal-search hidden-xs hidden-sm">
                                            <div class="row pilih-waktu-search content-gray">
                                                <div class="col-md-3">
                                                    <h4><span id="bulan-searchresult"><?= date('M') ?></span> <span id="tahun-searchresult"> <?= date('Y') ?></span></h4>
                                                </div>

                                                <div class="col-md-9">
                                                    <b>Pilih periode waktu konsultasi yang anda inginkan untuk membuat janji</b>
                                                </div>
                                            </div>
                                            <div class="jadwal-search">
                                                <div class="jadwal-dokter" id="jadwal-buat-janji-BDE">
                                                    <div class="row">
                                                        <?php
                                                        $j = 1;
                                                        $detail = $this->frontmodel->get_listjadwal($v->id_dokter);
                                                        foreach ($detail->result_array() as $ls_s) {
                                                            $active = ($j == 1) ? 'highlighted-day' : '';
                                                            $a = substr($ls_s['jam'], 0, 5) . ' s/d ' . substr($ls_s['jam_pulang'], 0, 5);
                                                        ?>
                                                            <div class="col-md-1 <?= $active ?>">
                                                                <div>
                                                                    <?= $ls_s['hari'] ?>
                                                                </div>
                                                                <div class="day-text" id="day-text-HR80000891-2021-05-31-0"><button class="btn available" id="row1" data-jadwalid="HR8000089124KANK00090000140000" data-namahari="<?= $hari ?>" data-buatjanji="1" data-jam="09:00-14:00" data-toggle="modal" data-date="<?= date('Y-m-d') ?>" data-target="#buatjanjimodal" data-idmika="2400" data-doctorid="HR80000891" data-iddoctor="BDE" data-doctorname="<?= strip_tags($v->nama_dokter) ?>" data-doctorphoto="<?php echo base_url(); ?>/assets/image/dokter/<?= $v->img ?>" data-waktu="PAGI" data-poli="24KANK00" data-specialization="<?= $nama_bagiannya ?>"><?= $a ?></button></div>
                                                            </div>
                                                        <?php $j++;
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <p></p>
                                            <p>Keterangan :</p>
                                            <ul>
                                                <li>Jadwal dapat berubah sewaktu-waktu</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                        <?php  }
                        endforeach;
                    } else { ?>
                        <div class="alert alert-warning">
                            <center>
                                <h3>Maaf jadwal yang bapak ibu cari tidak ditemukan silahkn melakukan pencarian lagi : )</h3>
                            </center>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal detail if that clikked -->
<?php
// var_dump($par);
// die;
if ($par == 'janji_dokter') {
    $not = 'Janji Online';
} else {
    $not = 'Janji Dokter';
}
?>

<div id="buatjanjimodal" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="model-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times" style="font-size:30px;"></i></span>
                </button>
            </div>
            <div class="modal-body buat-janji-body" style="margin-top:-10px;">
                <h2 class="content-gray judul-popup"><?= $judulk ?> </h2>
                <p class="error-message"></p>
                <div class="row content-modal-janji">
                    <div class="col-md-5 dokter-jadwal">
                        <div class="crop-info-dokter">
                            <img class="foto-info-dokter lazy" src="" style="display: inline-block;">
                        </div>
                        <div class="nama-buat-janji">
                            <h3 id="nama-bj"></h3>
                        </div>
                        <div class="spesialisasi-buat-janji">
                            <h4></h4>
                        </div>
                        <div class="hari-tanggal-janji">
                            <span class="hari-janji"></span>, <span class="tanggal-janji"></span>
                        </div>
                        <div class="jadwal-janji">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <form id="form-buat-janji" action="" method="post" novalidate="true" enctype="multipart/form-data">
                            <input type="hidden" name="_csrf-frontend" value="<?= sha1(md5(123)) ?>">
                            <div class="form-group field-input-time required">
                                <input type="hidden" id="input-time" class="form-control" name="waktu">
                                <input type="hidden" id="input-idoctor" class="form-control" name="dokter_id">
                                <input type="hidden" id="input-jadwalid" class="form-control" name="jadwalid">
                                <!-- <input type="hidden" id="input-hari" class="form-control" name="hari">
                                <input type="hidden" id="input-hari" class="form-control" name="hari"> -->
                                <input type="hidden" id="rs_id" class="form-control" name="rs_id">
                                <input type="hidden" id="par" class="form-control" name="par" value="<?= $par ?>">
                                <input type="hidden" id="jenis_konsultasi" class="form-control" name="jenis_konsultasi" value="<?= $jenis_konsultasi ?>">
                                <input type="hidden" id="rumah_sakit_id" class="form-control" name="rumah_sakit_id" value="<?= $rumah_sakit_id ?>">

                            </div>
                            <div class="form-group field-datetimepicker-buat-janji required">
                                <input type="hidden" id="datetimepicker-buat-janji" class="form-control" name="tgl_konsul_bln">
                                <input type="hidden" id="datetimepicker-tgl-konsul" class="form-control" name="tgl_konsul">
                            </div>
                            <div class="row row-p-buat-janji-top">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group field-input-medrecord">
                                        <label class="control-label" for="input-medrecord">Nomor Rekam Medis</label>
                                        <input type="text" id="input-medrecord" class="form-control select-info-dokter" name="medrec" maxlength="10" />

                                        <p> * Silahkan kosongkan jika pasien lama </p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group field-input-name required">
                                        <label class="control-label" for="input-name">Nama Lengkap</label> *
                                        <input type="text" id="input-name" class="form-control select-info-dokter" name="nama_pasien" aria-required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="row row-p-buat-janji-top">
                                <div class="col-md-6 col-xs-12">
                                    <label class="control-label" for="wz">Tanggal Lahir *</label>
                                    <div class="overlay-calendar">
                                        <div class="input-group date" id="jadwalform-tanggal-disp-kvdate">
                                            <!-- <input type="text" id="wz" class="select-info-dokter" readonly /> -->
                                            <input name="tgl_lahir" type="date" class="select-info-dokter" data-date-format="DD MMMM YYYY" required />
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group field-input-jenis-kelamin required">
                                        <label class="control-label" for="input-jenis-kelamin">Jenis Kelamin</label> *
                                        <select id="input-jenis-kelamin" class="form-control" name="jenis_kelamin">
                                            <option value="">Pilih Jenis Kelamin...</option>
                                            <option value="L">Laki-laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row row-p-buat-janji-top">
                                <div class="col-md-6 col-xs-12 form-group">
                                    <div class="form-group field-input-whatsapp required" style="margin-bottom: 15px;">
                                        <label class="control-label" for="input-whatsapp">Nomor Whatsapp</label> *
                                        <input type="text" id="input-whatsapp" class="form-control select-info-dokter" name="whatsapp" aria-required="true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 form-group">
                                    <div class="form-group field-input-email required">
                                        <label class="control-label" for="input-email">Email</label> *
                                        <input type="text" id="input-email" class="form-control select-info-dokter" name="email" aria-required="true" required>
                                    </div>
                                </div>
                            </div>
                            <!-- end of image-->
                            <?php
                            if ($par == 'janji_dokter') { ?>
                                <!-- image-->
                                <div class="row row-p-buat-janji-top">

                                    <div class="col-md-12 col-xs-12 form-group img_ktp">
                                        <div class="form-group field-input-ktp required">
                                            <label class="control-label" for="input-ktp">KTP/Paspor</label> *
                                            <input type="file" id="input-ktp" class="form-control select-info-dokter" name="file1" style="padding-top:12px;" aria-required="true">
                                            <input type='hidden' id='ktp-valid' name='file1' value=''>
                                        </div>
                                    </div>
                                    <!-- The container for the uploaded files -->
                                    <div class="ktp-images">
                                        <img id="ktp-images" src="#" alt="KTP" style="width:200px;height:200px; display:none;">
                                    </div>
                                </div>

                            <?php
                            } else if ($par == 'telekonsultasi') { ?>

                                <div class="row row-p-buat-janji-top">
                                    <div class="col-md-12 col-xs-12 form-group img_ktp">
                                        <div class="form-group field-input-ktp required">
                                            <label class="control-label" for="input-ktp">Bukti bayar</label> *
                                            <input type="file" id="input-ktp" class="form-control select-info-dokter" name="file2" style="padding-top:12px;" aria-required="true">
                                            <input type='hidden' id='file2' name='file2' value=''>
                                        </div>
                                    </div>
                                    <!-- The container for the uploaded files -->
                                    <div class="ktp-images">
                                        <img id="ktp-images" src="#" alt="KTP" style="width:200px;height:200px; display:none;">
                                    </div>
                                </div>

                                <div class="row row-p-buat-janji-top">

                                    <div class="col-md-12 col-xs-12 form-group img_ktp">
                                        <div class="form-group field-input-ktp required">
                                            <label class="control-label" for="input-ktp">KTP/Paspor</label> *
                                            <input type="file" id="input-ktp" class="form-control select-info-dokter" name="file1" style="padding-top:12px;" aria-required="true">
                                            <input type='hidden' id='ktp-valid' name='file1' value=''>
                                        </div>
                                    </div>
                                    <!-- The container for the uploaded files -->
                                    <div class="ktp-images">
                                        <img id="ktp-images" src="#" alt="KTP" style="width:200px;height:200px; display:none;">
                                    </div>
                                </div>

                            <?php
                            } else {
                            ?>

                            <?php } ?>

                            <!-- image-->
                            <div class="row row-p-alamat-top">
                                <div class="col-md-12 col-xs-12 form-group alamat">
                                    <div class="form-group field-input-ktp required">
                                        <label class="control-label" for="input-ktp">Alamat</label> *
                                        <textarea class="form-control" name="alamat" id="alamat"></textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group field-input-idmika required">
                                <input type="hidden" id="input-idmika" class="form-control" name="mika">
                            </div>
                            <div class="form-group field-input-iddoctor required">
                                <input type="hidden" id="input-iddoctor" class="form-control" name="dokter">
                                <input type="hidden" id="input-doctorid" class="form-control" name="dokterid">
                                <input type="hidden" id="input-namadoctor" class="form-control" name="namadokter">
                                <input type="hidden" id="input-penjamin" class="form-control" name="penjamin" value="U">
                            </div>
                            <div class="form-group field-input-idclinic required">
                                <input type="hidden" id="input-idclinic" class="form-control" name="poli">
                                <input type="hidden" id="input-namaclinic" class="form-control" name="namapoli">
                            </div>
                            <div class="form-group field-waktu-kosul required">
                                <input type="hidden" id="waktu-kosul" class="form-control" name="waktu_konsul">
                            </div>
                            <div class="error-summary" style="display:none">Silahkan lengkapi kolom dengan data yang sesuai sebagai berikut :<ul></ul>
                            </div>
                            <div class="center-btn">
                                <button type="submit" class="btn btn-pink btn-buat-janji" name="save-button">Buat Janji</button>
                            </div>
                            <div class="form-group notif-wa"><span class="notif-wa-icon">*)</span> Harap pastikan Nomor terdaftar pada WhatsApp untuk komunikasi lebih lanjut</div>
                            <div class="form-group notif-wa"><span class="notif-wa-icon">*)</span> Upload KTP/Paspor diperlukan untuk keperluan pencocokan data dengan Rekam Medis, bila Anda tidak memasukkan nomor Rekam Medis</div>
                            <div class="form-group notif-wa"><span class="notif-wa-icon">*)</span> Pasien Jaminan Asuransi/Perusahaan wajib melampirkan foto Kartu Peserta Asuransi/Kartu Karyawan Perusahaan untuk mempermudah petugas admin identifikasi rekanan kerjasama</div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="modal-body buat-janji-disclaimer" style="margin-top:-10px;">
                <h2 class="content-gray judul-popup" ID="toTop"><span></span> Syarat & Ketentuan<br />Web Booking Konsultasi Rawat Jalan Permata Keluarga</h2>
                <div class="row content-modal-janji">
                    <p style="text-align:justify;">
                        Anda diwajibkan untuk membaca Syarat dan Ketentuan Layanan dan Kebijakan Privasi yang terlampir dengan hati-hati dan seksama
                        sebelum menggunakan layanan. Dengan menggunakan Layanan Kami, Anda setuju bahwa Anda telah membaca, memahami, dan menyetujui
                        seluruh informasi, syarat-syarat, dan ketentuan yang terdapat dalam lembar ini. Ketentuan Penggunaan ini merupakan suatu
                        perjanjian sah terkait tata cara dan persyaratan penggunaan layanan antara Anda dengan RS Permata Keluarga. Harap diperhatikan
                        bahwa Syarat dan Ketentuan layanan dapat diperbarui dari waktu ke waktu.
                    </p><br />

                    <p style="text-align:justify;font-weight:bold;">Anda mengetahui dan menyetujui bahwa :</p>
                    <ul style="list-style:decimal;font-weight:bold;">
                        <li>Untuk Pasien Jaminan Pribadi</li>
                        <ul style="font-weight:normal;list-style:disc;">
                            <li>Anda akan mendapatkan slot pendaftaran Konsultasi Dokter, setelah menyelesaikan pembayaran melalui payment gateway. Bila dalam tempo 3 jam Anda tidak menyelesaikan pembayaran melalui payment gateway, maka slot Anda akan hangus, dan Anda perlu melakukan pendaftaran ulang bila ingin melanjutkan appointment.</li>
                            <li>Bila oleh karena suatu hal apapun, Anda berhalangan hadir pada tanggal appointment, maka Anda dapat mengajukan pembatalan appointment dan mendapatkan pengembalian dana bila pemberitahuan dilakukan ke nomor Customer Service paling lambat pukul 07.00 pada tanggal pelaksanaan Konsultasi. Lewat dari tenggat waktu tersebut, pengembalian dana tidak dapat dilakukan, dan hanya akan diberikan pilihan 1 kali penjadwalan ulang dalam bulan yang sama</li>
                            <li>Pengembalian dana hanya akan di transfer ke nomor rekening Anda saat melakukan pembayaran melalui midtrans. Biaya transfer untuk pengembalian dana ke Bank yang berbeda dengan Bank yang digunakan oleh RS, akan dibebankan kepada Anda</li>
                        </ul>
                        <li>Untuk pasien dengan jaminan Asuransi/Perusahaan</li>
                        <ul style="font-weight:normal;list-style:disc;">
                            <li>Penjamin diizinkan untuk mendapat segala keterangan dari Permata Keluarga sehubungan dengan diagnosa dan atau pelayanan lain yang diberikan kepada pasien untuk keperluan proses klaim sesuai dengan ketentuan polis/jaminan yang berlaku baik secara lisan maupun tertulis. Penolakan terhadap hal ini dapat menyebabkan penjaminan Asuransi/Perusahaan gugur.
                                Bersedia bertanggung jawab membayar biaya yang ditagihkan oleh Permata Keluarga baik sebagian maupun seluruhnya meskipun pasien telah menyelesaikan seluruh layanannya, apabila dikemudian hari diagnosa atau layanan yang diberikan masuk dalam kategori yang tidak sesuai ketentuan polis/jaminan:</li>
                            <ul style="font-weight:normal;list-style:disc;">
                                <li>Melebihi batas ketentuan manfaat / plafon</li>
                                <li>Masuk dalam pengecualian pertanggungan</li>
                                <li>Tidak memenuhi persyaratan</li>
                            </ul>
                        </ul>
                        <li>Untuk pasien dengan jaminan BPJS</li>
                        <ul style="font-weight:normal;list-style:disc;">
                            <li>Seluruh penyelenggaraan Konsultasi Rawat Jalan mengacu kepada ketentuan BPJS yang berlaku</li>
                        </ul>
                    </ul>

                    <p>Bersama ini saya menyatakan telah membaca, memahami, dan menyetujui seluruh informasi, syarat-syarat, dan ketentuan yang terdapat dalam lembar ini</p>

                    <div class="cont-btn-setuju">
                        <button type="submit" class="btn btn-pink btn-buat-janji" id="btn-setuju" name="save-button">Setuju</button>
                        <button type="button" class="btn btn-grey btn-kembali" name="back-button">Tidak Setuju</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>


<script>
    //action after buat janji
    $(function() {
        $("#btn-setuju").click(function(a) {
            var fd = new FormData($("#form-buat-janji")[0]);
            // $("#buatjanjimodal").removeClass("in");
            // $("#buatjanjimodal").fadeOut(200);
            $("#loading-form-buat-janji").addClass("in");
            $("#loading-form-buat-janji").fadeIn(200);
            a.isDefaultPrevented() || (a.preventDefault(), a = $('#form-buat-janji').serializeArray(),
                $.ajax({
                    type: "POST",
                    url: '<?= base_url('insertjadwal') ?>',
                    data: fd,
                    dataType: 'json',
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(a) {
                        swal.fire('status', 'data berhasil di simpan', 'success');
                        if (a.par != '') {
                            window.location.href = '<?= base_url('telekonsultasi/berhasil') ?>/' + a.id + '/?par=' + a.par;
                        } else {
                            window.location.href = '<?= base_url('telekonsultasi/berhasil') ?>/' + a.id;
                        }
                    },
                    error: function(a, c, e) {

                    }
                }))
        });


        // on selected datas

        $('select[name="jenis_kelamin"]').on('change', function() {
            var in_medrec = $("#input-medrecord").val();
            var panjang_medrec = in_medrec.length;
            if (panjang_medrec < 6) {
                $(".field-input-ktp").delay(500).fadeIn();
                // $(".img_ktp").css('display', 'none');
                // $('#input-ktp').css('display', 'none');
                $(".field-input-ktp").css('display', 'block');
            }
        });



        $('.buat-janji-disclaimer').hide();
        $('#form-buat-janji').on('submit', function(event) {
            event.preventDefault();
            var phoneno = /^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$/;

            if ($('input[name="whatsapp"]').val() == '') {
                swal.fire('Nomor Whatsapp yang anda entrikan tidak sesuai', 'error', 'error');
            } else if ($('input[name="alamat"]').val() == '') {
                swal.fire('Alamat tidak boleh kosong', 'error', 'error');
            } else if (!$('input[name="whatsapp"]').val().match(phoneno)) {
                swal.fire('Format nomor wa salah', 'error', 'error');
            } else {
                $('.buat-janji-disclaimer').show();
                $('.buat-janji-body').hide();
                var datastring = new FormData(this);
                $.ajax({
                    url: '<?= base_url('') ?>',
                    data: datastring,
                    dataType: 'json',
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        window.location.href = '<?= base_url('telekonsultasi/berhasil') ?>/' + data.id;
                    },
                    error: function(jqxHr, status, error) {
                        // swal.fire('error', 'maaf  adad kesalah terkai penyimpanan data' + jqxHr, 'error');
                    }
                });
            }
        });

    });
</script>