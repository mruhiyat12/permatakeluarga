<?php

$this->load->helper('url');
$rs = $this->uri->segment(1);
$list_rs = $this->frontmodel->get_opt_rs();
?>
<link href="<?= base_url('assets/template/css/button.css') ?>" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet">
<div class="site-about">
    <div class="site-bg lazy" data-src="/img/bg3.jpg">
        <div class="row judul-back row-ne">
            <div class="row judul-back">
                <h1 class="title-lokasi-home col-md-6 col-xs-12"><?= $judul ?></h1>
            </div>
        </div>

        <div class="card card-site card-news-event">
            <div class="col-md-6 konten-janji">
                <div class="row row-buat-janji">
                    <!-- <div class="loading-klinik hidden text-center">
                        <h2><span class="glyphicon glyphicon-refresh is-loading"></span> Loading...</h2>
                    </div> -->
                    <div class="col-md-12 col-xs-12 news-part" id="kounsul">
                        <div class="text-left">
                            <?php
                            $get = isset($_SERVER['QUERY_STRING']) ?  '?' . $_SERVER['QUERY_STRING'] : '';
                            ?>
                            <form class="form-horizontal" action="<?= base_url('listdokter' . $get) ?>" method="POST" id="konsultasi">
                                <div class="form-group">
                                    <label class="col-md-3">Rumah Sakit</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="rmahsakitid" id="rmahsakitid" style="
    border-radius: 30px;
" required>
                                            <option value="">Pilih Rumah Sakit</option>
                                            <?php
                                            $j = 1;
                                            foreach ($rmahsakit->result() as $list) :
                                                echo ($list->id_rumahsakit);
                                                switch ($list->id_rumahsakit) {
                                                    case 1:
                                                        $fnrmahsakit = 'lippo';
                                                        break;
                                                    case 2:
                                                        $fnrmahsakit = 'jababeka';
                                                        break;
                                                    case 4:
                                                        $fnrmahsakit = 'karawang';
                                                        break;
                                                }
                                                $selected = ($this->uri->segment(1) == $fnrmahsakit) ? 'selected' : '';
                                                $actv = ($rmahsakit == $fnrmahsakit) ? 'active' : "";
                                            ?>
                                                <option value="<?= $list->id_rumahsakit ?>" <?= $selected ?>><?= ucfirst(strtoupper($list->nama_rumahsakit)) ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3">Klinik</label>
                                    <div class="col-md-9">
                                        <select class="form-control" id="kliknik" name="klinik" style="
    border-radius: 30px;
" required>
                                            <option value="">Semua Jenis Klinik</option>

                                            <?php

                                            foreach ($clinic->result_array() as $clinics) : ?>
                                                <option value="<?= $clinics['id_bagian'] ?>"><?= $clinics['nama_bagian'] ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3">Pilih Penjaminan</label>
                                    <div class="col-md-9">
                                        <select class="form-control" id="penjaminanid" style="
    border-radius: 30px;
" name="penjaminid" required>
                                            <option value="">-- Data Penjaminan --</option>
                                            <option value="pribadi">Pasien Jaminan /pribadi</option>
                                            <option value="perusahaan">Perusahaan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3">Tanggal konsultasi</label>
                                    <div class="col-md-9">
                                        <input type="date" class="form-control" name="tgl_konsultasi" required>
                                    </div>
                                </div>
                                <hr />
                                <div class="center-btn">
                                    <button type="submit" class="btn btn-pink button-register" name="save-button">Cari</button>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function() {
        // $('#konsultasi').on('submit', function(e) {
        //     e.preventDefault();
        //     Swal.fire({
        //         title: 'Are you sure?',
        //         text: "You won't be able to revert this!",
        //         icon: 'warning',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Yes, delete it!'
        //     }).then((result) => {
        //         /* Read more about isConfirmed, isDenied below */
        //         if (result.isConfirmed) {
        //             let action = $(this).attr('action');
        //             $.post(action, {
        //                 data: $(this).serialize()
        //             }, function(data) {
        //                 window.location.href = action;
        //             }).error(function(data) {
        //                 swal.fire('error', 'maaf jadwal sedang sibuk');
        //             });
        //         } else if (result.isDenied) {
        //             Swal.fire('Changes are not saved', '', 'info')
        //         }
        //     })


        // });

        $('.form-control').on("focus", function() {
            $(this).parent('.input-group').addClass("input-group-focus");
        }).on("blur", function() {
            $(this).parent(".input-group").removeClass("input-group-focus");
        });
        //if form submit action
        // $('#konsultasi').on('submit', function(e) {
        //     e.preventDefault();
        //     window.location.href = '';
        // });
    });
</script>