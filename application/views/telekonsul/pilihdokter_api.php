<?php
if (count($jadwal_dokter) > 0) {

    foreach ($jadwal_dokter as $k => $v) :
        if ($k == 0 || (isset($jadwal_dokter[$k - 1]->id_dokter)) && $jadwal_dokter[$k - 1]->id_dokter != $v->id_dokter) {

            $bagian = $this->db->get_where('bagian', [
                'id_bagian' => $v->id_bagian
            ])->result_array();
            $nama_bagiannya = isset($bagian[0]['nama_bagian']) ? $bagian[0]['nama_bagian'] : '';
?>
            <div class="card card-site card-search" id="search-card-340">
                <div class="row">
                    <div class="col-md-3 col-xs-12 dokter-search">
                        <div class="overlay-info-dokter col-md-12 col-xs-4">
                            <a class="info-dokter" href="" data-id="HR80000891" data-name="2400" data-spesialis="<?= $nama_bagiannya ?>" data-cv="<p><strong>Riwayat Pendidikan :</strong></p>
													<ul>-
													</ul>
													<p><strong>Riwayat Pekerjaan :</strong></p>
													<ul>-
													</ul>" data-nama-rs="<?= ucfirst($v->nama_rumahsakit) ?>" data-idspesialis="24KANK00" data-idclinic="2400" data-src-photo="<?php echo base_url(); ?>/assets/image/dokter/<?= $v->img ?>" data-nametwo="<?= strip_tags($v->nama_dokter) ?>" data-toggle="modal" data-target="#info-dokter-modal">
                                <div class="crop-info-dokter">
                                    <img class="foto-info-dokter lazy" src="<?php echo base_url(); ?>/assets/image/dokter/<?= $v->img ?>" style="display: inline-block;">
                                </div>
                            </a>
                            <div class="spesialis-bar hidden-xs hidden-sm"><?= $nama_bagiannya ?></div>
                        </div>
                        <div class="col-md-12 col-xs-7">
                            <div class="name-info-dokter"><b><?= strip_tags($v->nama_dokter) ?></b></div>
                        </div>
                        <div class="alamat-info-dokter hidden-xs hidden-sm"><b><?= ucfirst($v->nama_rumahsakit) ?></b></div>
                    </div>
                    <div class="col-md-9 col-xs-12 part-jadwal-search hidden-xs hidden-sm">
                        <div class="row pilih-waktu-search content-gray">
                            <div class="col-md-3">
                                <h4><span id="bulan-searchresult"><?= date('M') ?></span> <span id="tahun-searchresult"> <?= date('Y') ?></span></h4>
                            </div>

                            <div class="col-md-9">
                                <b>Pilih periode waktu konsultasi yang anda inginkan untuk membuat janji</b>
                            </div>
                        </div>
                        <div class="jadwal-search">
                            <div class="jadwal-dokter" id="jadwal-buat-janji-BDE">
                                <div class="row">
                                    <?php
                                    $j = 1;
                                    $detail = $this->frontmodel->get_listjadwal($v->id_dokter);
                                    foreach ($detail->result_array() as $ls_s) {
                                        $active = ($j == 1) ? 'highlighted-day' : '';
                                        $a = substr($ls_s['jam'], 0, 5) . ' s/d ' . substr($ls_s['jam_pulang'], 0, 5);
                                    ?>
                                        <div class="col-md-1 <?= $active ?>">
                                            <div>
                                                <?= $ls_s['hari'] ?>
                                            </div>
                                            <div class="day-text" id="day-text-HR80000891-2021-05-31-0"><button class="btn available" id="row1" data-jadwalid="HR8000089124KANK00090000140000" data-namahari="<?= $hari ?>" data-buatjanji="1" data-jam="09:00-14:00" data-toggle="modal" data-date="<?= date('Y-m-d') ?>" data-target="#buatjanjimodal" data-idmika="2400" data-doctorid="HR80000891" data-iddoctor="BDE" data-doctorname="<?= strip_tags($v->nama_dokter) ?>" data-doctorphoto="<?php echo base_url(); ?>/assets/image/dokter/<?= $v->img ?>" data-waktu="PAGI" data-poli="24KANK00" data-specialization="<?= $nama_bagiannya ?>"><?= $a ?></button></div>
                                        </div>
                                    <?php $j++;
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <p></p>
                        <p>Keterangan :</p>
                        <ul>
                            <li>Jadwal dapat berubah sewaktu-waktu</li>
                        </ul>
                    </div>
                </div>
            </div>
    <?php  }
    endforeach;
} else { ?>
    <div class="card card-site card-search" id="search-card-340">
        <div class="row">
            <div class="overlay-info-dokter col-md-12 col-xs-8">
                <div class="alert alert-info">
                    <tt>Maaf data dokter tidak di temukan</tt>
                </div>
            </div>
        </div>
    </div>

<?php } ?>