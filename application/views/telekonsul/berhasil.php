<div class="site-contact">
    <div class="lokasi-klinik">
        <div class="site-bg lazy">
            <div class="row judul-back">
                <h1 class="title-lokasi-home col-md-12 col-xs-12"><i class="fa fa-check" aria-hidden="true"></i>
                    Pendaftaran berhasil </h1>
            </div>
            <div class="card card-site card-contact">
                <div class="highlight-cp hidden-sm hidden-xs">
                    <div class="row">

                        <div class="col-md-12">
                            <?php
                            if ($this->session->userdata('_USER_ID') != '') { ?>
                                <h4>Kirim data pesan untuk konfirmasi user</h4>
                                <hr />
                                <form action="" method="POST" id="konfirmasi">

                                <?php
                            } else {  ?>
                                    <h4>Selamat pendaftaran berhasil , silahkan menunggu hasil dari rumahsakit terkait pendaftaran </h4>

                                <?php } ?>
                                <div class="col-md-8 text-center">
                                    <table class="table table-striped table-sm">
                                        <input type="hidden" name="parameter_id" value="<?= $params ?>">
                                        <input type="hidden" name="no_wa" value="<?= $konsultdata->hp ?>">
                                        <input type="hidden" name="no_wa" value="<?= $konsultdata->hp ?>">
                                        <tr>
                                            <td>Nomor Rekam Medis</td>
                                            <td>:</td>
                                            <td><?= ($konsultdata->no_rek_medis) ? $konsultdata->no_rek_medis : 'kosong'; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama lengkap</td>
                                            <td>:</td>

                                            <td><?= ($konsultdata->nama_lengkap) ? $konsultdata->nama_lengkap : 'kosong' ?></td>

                                        </tr>
                                        <tr>
                                            <td>Klinik</td>
                                            <td>:</td>

                                            <td><?= ($konsultdata->nama_bagian) ? $konsultdata->nama_bagian : 'Data Kosong' ?></td>

                                        </tr>
                                        <tr>
                                            <td>Dokter Yang di hubungi</td>
                                            <td>:</td>

                                            <td><?= ($konsultdata->nama_dokter) ? $konsultdata->nama_dokter : 'kosong'; ?></td>

                                        </tr>
                                        <tr>
                                            <td>Rumah Sakit </td>
                                            <td>:</td>

                                            <td><?= $konsultdata->nama_rumahsakit ?  $konsultdata->nama_rumahsakit : 'Data kosong' ?></td>

                                        </tr>
                                        <tr>
                                            <td>Tanggal lahir</td>
                                            <td>:</td>

                                            <td><?= !empty($konsultdata->tgl_lahir) ?  DateToIndo($konsultdata->tgl_lahir) : "data kosong"; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Whatsapp *</td>
                                            <td>:</td>

                                            <td><?= $konsultdata->hp ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email * </td>
                                            <td>:</td>

                                            <td><?= $konsultdata->email ?></td>
                                        </tr>
                                        <!-- <tr>
                                        <td>KTP/Paspor *</td>
                                        <td>:</td>
                                        <td><?php echo '<img src="' . base_url('assets/image/konsultasi/' . $data->ktp_passpor) . '" class="img-responsive" style="width:100px;height:100px" onerror="this.onerror=null;this.src=\'https://i.stack.imgur.com/6M513.png\';">'; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Bukti bayar</td>
                                        <td>:</td>
                                        <td><?php echo '<a href="' . base_url('assets/image/konsultasi/' . $data->bukti_bayar) . '" target="_blank"><img src="' . base_url('assets/image/konsultasi/' . $data->bukti_bayar) . '" class="img-responsive" style="width:100px;height:100px" onerror="this.onerror=null;this.src=\'https://i.stack.imgur.com/6M513.png\';"></a>' ?></td>
                                    </tr> -->
                                        <tr>
                                            <td>Alamat </td>
                                            <td>:</td>
                                            <td><?= $konsultdata->alamatpas ?></td>
                                        </tr>
                                        <?php
                                        if ($this->session->userdata('_USER_ID') != '') { ?>
                                            <tr>

                                                <td>
                                                    <br /><button class="btn btn-primary btn-md" id="konfirmasi"><i class="fa fa fa-user"></i>Konfirmasi</button> <a href="<?= $_SERVER['REQUEST_URI'] ?>&print=yes" target="_blank" class="btn btn-info btn-md"><i class="fa fa-print"></i>Print</a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                        </div>


                        <?php
                        if ($this->session->userdata('_USER_ID') != '') { ?>

                            </form>

                        <?php } ?>

                        <small>Tanggal dan waktu pendaftaran <?= date('y-M-D H:I:s');

                                                                ?>

                            <br />
                            Mohon bersabar untuk menunggu persetujuan admin , terim kasih
                        </small>
                    </div>
                </div>
            </div>
            <div class="row" id="text-info">

            </div>
        </div>
    </div>


    <?php
    if ($this->session->userdata('_USER_ID') != '') { ?>
        <script>
            $(function() {
                $('#konfirmasi').on('submit', function(e) {
                    e.preventDefault();

                    var nama_dokter = '<?= $konsultdata->nama_dokter ?>',
                        nama_lengkap = '<?= $konsultdata->nama_lengkap ?>',
                        tgl_lahir = '<?= $konsultdata->tgl_lahir ?>',
                        jk = '<?= ($konsultdata->jk == 'L') ?  "Laki -laki" : 'Perempuan' ?>',
                        hp = '<?= $konsultdata->hp ?>',
                        email = '<?= $konsultdata->email ?>',
                        alamatpas = '<?= $konsultdata->alamatpas ?>';

                    /* Pengaturan Whatsapp */
                    var walink = 'https://web.whatsapp.com/send',
                        phone = '62<?= $konsultdata->hp ?>',
                        text = '*Rumah sakit permata keluarga* ',
                        text_yes = 'Janji dokter berhail di buat.',
                        text_no = 'Isilah formulir terlebih dahulu.';

                    /* Smartphone Support */
                    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                        var walink = 'whatsapp://send';
                    }
                    if (nama_lengkap != "" && phone != "") {
                        /* Whatsapp URL */
                        var checkout_whatsapp = walink + '?phone=' + phone + '&text=' + text + '%0A%0A' +
                            '*Format Registrasi online Telekonsul*' + '%0A' +
                            '*Nama* : ' + nama_lengkap + '%0A' +
                            '*Tanggal Lahir* : ' + tgl_lahir + '%0A' +
                            '*Jenis kelamin* : ' + jk + '%0A' +
                            '*Alamat* : ' + alamatpas + '%0A' +
                            '*Nomor Kontak / Whatsapp* : ' + phone + '%0A' +
                            '*Email* : ' + email + '%0A' +
                            '- Wajib Sertakan foto idcard/kartu asuransi & KTP.' + '%0A' +
                            '- Pedaftaran pada hari /tgl yang sama.' + '%0A' +
                            '*Terimakasih🙏';

                        window.open(checkout_whatsapp, '_blank');
                        document.getElementById("text-info").innerHTML = '<div class="alert alert-success">' + text_yes + '</div>';
                    } else {
                        document.getElementById("text-info").innerHTML = '<div class="alert alert-danger">' + text_no + '</div>';
                    }
                });
            });
        </script>

    <?php } ?>