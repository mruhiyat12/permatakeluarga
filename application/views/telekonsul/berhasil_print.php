<style>
    body {
        background: rgb(204, 204, 204);
    }

    page[size="A4"] {
        background: white;
        width: 21cm;
        height: 29.7cm;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
    }

    @media print {

        body,
        page[size="A4"] {
            margin: 0;
            box-shadow: 0;
        }
    }

    .header {

        padding: 30px 20px 10px;

    }

    .body {

        padding: 10px 70px 0px;

    }
</style>

<page size="A4">

    <div class="header">
        <h3>Detail Pendaftaran Pasien</h3>
        <hr />
    </div>

    <div class="body">
        <table class="table table-striped">

            <tr>
                <td>Nomor Rekam Medis</td>
                <td>:</td>
                <td><?= $konsultdata->no_rek_medis; ?></td>
            </tr>
            <tr>
                <td>Nama lengkap</td>
                <td>:</td>

                <td><?= $konsultdata->nama_lengkap ?></td>

            </tr>

            <tr>
                <td>Tanggal lahir</td>
                <td>:</td>

                <td><?= $konsultdata->tgl_lahir; ?></td>
            </tr>
            <tr>
                <td>Nomor Whatsapp *</td>
                <td>:</td>

                <td><?= $konsultdata->hp ?></td>
            </tr>
            <tr>
                <td>Email * </td>
                <td>:</td>

                <td><?= $konsultdata->email ?></td>
            </tr>
            <tr>
                <td>KTP/Paspor *</td>
                <td>:</td>
                <td><?php echo '<img src="' . base_url('assets/image/konsultasi/' . $data->ktp_passpor) . '" class="img-responsive" style="width:250px;height:250px" onerror="this.onerror=null;this.src=\'https://i.stack.imgur.com/6M513.png\';">'; ?></td>
            </tr>

            <tr>
                <td>Bukti bayar</td>
                <td>:</td>
                <td><?php echo '<a href="' . base_url('assets/image/konsultasi/' . $data->bukti_bayar) . '" target="_blank"><img src="' . base_url('assets/image/konsultasi/' . $data->bukti_bayar) . '" class="img-responsive" style="width:250px;height:250px" onerror="this.onerror=null;this.src=\'https://i.stack.imgur.com/6M513.png\';"></a>' ?></td>
            </tr>
            <tr>
                <td>Alamat </td>
                <td>:</td>
                <td><?= $konsultdata->alamatpas ?></td>
            </tr>

        </table>
    </div>


</page>