 <div class="nav-tabs-custom">
     <ul class="nav nav-tabs pull-right">
         <li class="active">
             <a data-toggle="tab" href="#konsul-table-tab" title="Table View">
                 <i class="fa fa-table"></i>
             </a>
         </li>
         <li class="">
             <a data-toggle="tab" href="#konsul-form-tab" title="Form View">
                 <i class="fa fa-edit"></i>
             </a>
         </li>
         <li class="">
             <a data-toggle="tab" href="#konsul-detail-tab" title="Detail View">
                 <i class="fa fa-eye"></i>
             </a>
         </li>
         <li class="pull-left header"><i class="fa fa-file-text"></i> Data Konsultasi </li>

     </ul>
     <div class="tab-content">
         <div id="konsul-table-tab" class="tab-pane fade active in">

             <form class="form-horizontal">
                 <div class="form-group">
                     <label class="col-md-3">Jenis Konsultasi</label>
                     <div class=" col-md-4">
                         <select name="konsul_id" id="konsul_id" class="form-control">
                             <option value=""></option>
                             <?php
                                $jn = [
                                    '1' => 'Telekonsultasi Dokter',
                                    '2' => 'Konsultasi Online',
                                    '3' => 'Telekonsultasi'
                                ];
                                foreach ($jn as $jn => $jns) :
                                ?>
                                 <option value="<?= $jn ?>"><?= $jns ?></option>
                             <?php endforeach ?>
                         </select>
                     </div>
                 </div>
             </form>
             <table id="konsultasi" class="table table-bordered table-striped table-hover table-condensed">
                 <thead>
                     <tr>
                         <th>#</th>
                         <th>Penjaminan</th>
                         <th>Tgl konsul</th>
                         <th>No Rmedis</th>
                         <th>Nama Lengkap</th>
                         <th>Hp</th>
                         <th>Email</th>
                         <th>Status Konsultasi</th>
                         <th>Rumah sakit</th>
                         <th>Action</th>
                     </tr>
                 </thead>
                 <tbody></tbody>
             </table>
         </div>
         <div id="konsul-form-tab" class="tab-pane fade">
             <h1>Read only</h1>
             <form class="form-horizontal" method="post" id="form_konsultasi">
                 <div class="form-group">
                     <label class="col-md-3">Rumah sakit</label>
                     <div class=" col-md-8">
                         <select name="tmrs_id" id="tmrs_id" class="form-control">
                             <option value="">Pilih data rumah sakit</option>
                             <?php foreach ($this->db->get('rumah_sakit')->result_array() as $rs) :
                                    $selected = ($rs['id_rumahsakit'] == $tmrs_id) ? 'selected' : '';

                                ?>
                                 <option value="<?= $rs['id_rumahsakit'] ?>" <?= $selected ?>><?= $rs['nama_rumahsakit'] ?></option>
                             <?php endforeach ?>
                         </select>
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Penjaminan</label>
                     <div class=" col-md-8">
                         <input class="form-control" type="text" name='penjaminan' value="<?= $penjaminan ?>">
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Tanggal konsul</label>
                     <div class=" col-md-8">
                         <input class="form-control" type="date" name='tgl_konsul' value="<?= $tgl_konsul ?>">
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Pilih jadwal dokter</label>
                     <div class=" col-md-8">
                         <input class="form-control" type="date" name='jadwal_dokter_id'>
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Nomor rekam medis</label>
                     <div class=" col-md-8">
                         <input class="form-control" type="text" name='no_rek_medis' value="<?= $no_rek_medis ?>">
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Nama Lengkap</label>
                     <div class=" col-md-8">
                         <input class="form-control" type="text" name='nama_lengkap' value="<?= $nama_lengkap ?>">
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Tannggal lahir</label>
                     <div class=" col-md-8">
                         <input class="form-control" type="date" name='tgl_lahir' value="<?= $tgl_lahir ?>">
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Jenis Kelamin</label>
                     <div class=" col-md-8">
                         <select name="jk" class="form-control">
                             <?php $lk = ['l' => 'Laki -laki', 'p' => 'Perempuan'];
                                foreach ($lk as  $lf => $f) :
                                    $selected = ($jk == $lf) ? 'selected' : '';

                                ?>
                                 <option value="<?= $lf ?>"><?= ucfirst($f) ?></option>
                             <?php endforeach; ?>
                         </select>
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Hanphone</label>
                     <div class=" col-md-8">
                         <input class="form-control" type="number" name='hp' value="<?= $hp ?>">
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Email</label>
                     <div class=" col-md-8">
                         <input class="form-control" type="email" name='email' value="<?= $email ?>">
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="col-md-3">Ktp / Passport </label>
                     <div class=" col-md-8">
                         <input class="form-control" type="text" name='ktp_passpor' value="<?= $ktp_passpor ?>">
                     </div>
                 </div>

                 <div class="form-group">

                     <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                         <button type="submit" id="btn-save" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                         <button type="reset" class="btn btn-default" onclick="setActiveTab('konsul-table-tab');"><i class="fa fa-undo"></i> Cancel</button>
                     </div>
                 </div>


             </form>
         </div>
     </div>
 </div>
 <script>
     $(document).ready(function() {
         getData();
         <?php
            if ($param != null) {
                echo 'getData("' . $param . '");';
                echo 'setActiveTab("konsul-form-tab");';
                echo '$("#div-foto").show();';
            }
            ?>

         function newForm() {
             loadContent(base_url + "/konsuladmin/index", function() {
                 setActiveTab("konsul-form-tab");
             });
         }
         var tableArticle = $("#konsultasi").DataTable({
             oLanguage: {
                 sProcessing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
             },
             processing: true,
             serverSide: true,
             ajax: {
                 "url": base_url + '/objects/telekonsultasi',
                 "type": "POST",
                 "data": function(data) {
                     var konsul_id = $('#konsul_id').val();
                     data.konsul_id = konsul_id;
                 }
             },
             columns: [{
                     'data': 'no'
                 },
                 {
                     'data': 'penjaminan'
                 },
                 {
                     'data': 'tgl_konsul'
                 },
                 {
                     'data': 'no_rek_medis'
                 },
                 {
                     'data': 'nama_lengkap'
                 },
                 {
                     'data': 'hp'
                 },
                 {
                     'data': 'email'
                 },
                 {
                     'data': 'jenis_konsultasi',
                     "searchable": false,
                     "orderable": false,
                     "render": function(data, type, row) {

                         if (row.jenis_konsultasi = 1) {
                             return 'Telekonsultasi Dokter';
                         } else if (row.jenis_konsultasi = 2) {
                             return 'Konsultasi Online';
                         } else if (row.jenis_konsultasi = 3) {
                             return 'Telekonsultasi';

                         }
                     }
                 },
                 {
                     'data': 'nama_rumahsakit'
                 },
                 {
                     'data': 'aksi'
                 },

             ],
             order: [
                 [0, 'desc']
             ],
             rowCallback: function(row, data, iDisplayIndex) {
                 utilsArticle();
             }
         });
         $('#konsul_id').on('change', function() {
             $('#konsultasi').DataTable().ajax.reload();
         });
     });

     function utilsArticle() {
         $('#konsultasi').DataTable().on("click", ".editBtn", function(e) {
             e.preventDefault();
             loadContent(base_url + 'konsuladmin/index/' + $(this).attr('href').substring(1));
         });

         $('#konsultasi').DataTable().on("click", '.removeBtn', function() {
             confirmDelete($(this).attr('href').substring(1));
         });
         //konfirm konsultasi 
         $("#konsultasi .approveeBtn").on("click", function() {
             confirmApprove($(this).attr('href').substring(1), $(this).attr('href').substring(2));
         });

     }

     $('#form_konsultasi').on('submit', function(e) {
         e.preventDefault();
         //  loading('loading', true);
         $.ajax({
             url: '<?= $action ?>',
             data: $(this).serialize(),
             dataType: 'json',
             type: 'POST',
             cache: false,
             success: function(json) {
                 loading('loading', false);
                 if (json.data.code === 0) {
                     if (json.data.message == '') {
                         genericAlert('Penyimpanan data gagal!', 'error', 'Error');
                     } else {
                         genericAlert(json.data.message, 'warning', 'Peringatan');
                     }
                 } else {
                     genericAlert('Penyimpanan data berhasil', 'success', 'Sukses');
                     loadContent(base_url + '/konsuladmin/index');
                 }
             },
             error: function() {
                 loading('loading', false);
                 genericAlert('Terjadi kesalahan!', 'error', 'Error');
             }
         });

     });

     function getData(idx) {
         $.ajax({
             url: base_url + '/objects/telekonsultasi',
             data: 'model-input=konsul&key-input=konsul_id&value-input=' + idx,
             dataType: 'json',
             type: 'POST',
             cache: false,
             success: function(json) {
                 if (json.data.code === 0) {
                     loginAlert('Akses tidak sah');
                 } else {
                     //  $("#publish-input").val(json.data.object.is_publish);
                     //  $("#category-input").val(json.data.object.konsul_category_id);
                     //  $("#rumah_sakit-input").val(json.data.object.id_rumahsakit);
                     //  $("#action-input").val('2');
                     //  $("#value-input").val(idx);
                     //  if (json.data.object.img !== null) {
                     //      $("#foto-div").html('<img src="<?php echo base_url(); ?>assets/files/konsul/' + json.data.object.img + '" class="img img-thumbnail img-small">');
                 }

             }
         });
     }

     ///konfirmasi konsultasi user via wa web   
     function confirmApprove(n, g) {
         swal({
                 title: "Konfirmasi Konsultasi user akan di terima",
                 text: "apakah anda akan melanjutkan konfirmasi user ini ?",
                 type: "info",
                 showCancelButton: true,
                 confirmButtonClass: "btn-danger",
                 confirmButtonText: " Ya",
                 closeOnConfirm: true
             },
             function() {
                 loading('loading', true);
                 window.open(base_url + '/telekonsultasi/berhasil/' + n + '/' + g)
             });
     }

     function confirmDelete(n) {
         swal({
                 title: "Konfirmasi Hapus",
                 text: "Apakah anda yakin akan menghapus data ini?",
                 type: "warning",
                 showCancelButton: true,
                 confirmButtonClass: "btn-danger",
                 confirmButtonText: " Ya",
                 closeOnConfirm: false
             },
             function() {
                 loading('loading', true);
                 setTimeout(function() {
                     $.ajax({
                         url: base_url + '/konsuladmin/destroy/' + n,
                         data: 'id=' + n,
                         dataType: 'json',
                         type: 'POST',
                         cache: false,
                         success: function(json) {
                             loading('loading', false);
                             if (json.data.code === 1) {
                                 genericAlert('Hapus data berhasil', 'success', 'Sukses');
                                 refreshTable();
                             } else if (json.data.code === 2) {
                                 genericAlert('Hapus data gagal!', 'error', 'Error');
                             } else {
                                 genericAlert(json.data.message, 'warning', 'Perhatian');
                             }
                         },
                         error: function() {
                             loading('loading', false);
                             genericAlert('Tidak dapat hapus data!', 'error', 'Error');
                         }
                     });
                 }, 100);
             });
     }

     function refreshTable() {
         $('#konsultasi').DataTable().ajax.reload();
     }

     function CKupdate() {
         for (instance in CKEDITOR.instances)
             CKEDITOR.instances[instance].updateElement();
     }
 </script>



 <!-- Modal -->
 <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         <h3 id="myModalLabel">Modal header</h3>
     </div>
     <div class="modal-body">

     </div>
     <div class="modal-footer">
         <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
         <button class="btn btn-primary">Save changes</button>
     </div>
 </div>