<?php
// var_dump($param);
?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li class="active">
            <a data-toggle="tab" href="#article-table-tab" title="Table View">
                <i class="fa fa-table"></i>
            </a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#article-form-tab" title="Form View">
                <i class="fa fa-edit"></i>
            </a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#article-detail-tab" title="Detail View">
                <i class="fa fa-eye"></i>
            </a>
        </li>
        <li class="pull-left header"><i class="fa fa-file-text"></i> Testimoni User </li>
        <div id="loading"></div>
    </ul>
    <div class="tab-content">
        <div id="article-table-tab" class="tab-pane fade active in">
            <table id="testimoni_table" class="table table-bordered table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Gambar</th>
                        <th>Isi </th>
                        <th>Created </th>
                        <th>Rumah sakit </th>
                        <th><a href="#" class="btn btn-xs btn-success pull-right" onclick="newForm()"> <i class="fa fa-plus"></i> Add Data</a></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div id="article-form-tab" class="tab-pane fade">
            <form class="form-horizontal" role="form" id="testimonial_form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title-input" class="col-md-3 control-label">Isi testimonial</label>
                    <div class="col-md-6">
                        <textarea class="form-control" name="isi" id="content-input">
                         <?= $isi ?></textarea>
                    </div>
                </div>
                <?= parsing_rs_select()  ?>
                <div class="form-group">
                    <label for="title-input" class="col-md-3 control-label">Gambar</label>
                    <div class="col-md-6">
                        <?php
                        if ($gambar != '') {
                            $fld = 'assets/image/testimoni/';
                            echo '<img src="' . base_url($fld . '/' . $gambar) . '" class="img-responsive" style="width:100px;height:100px">';
                        }
                        ?>
                        <input type="file" class="form-control" id="title-input" name="gambar" placeholder="Title" value="" />
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                        <button type="submit" id="btn-save" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-default" onclick="setActiveTab('article-table-tab');"><i class="fa fa-undo"></i> Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script> 
    $(document).ready(function() {
         getArticle();
        <?php
        // var_dump($param);
        if ($param != null) {
            echo 'getData("' . $param . '");';
            echo 'setActiveTab("article-form-tab");';
            echo '$("#div-foto").show();';
        }
        ?>
    });

    function newForm() {
        loadContent(base_url + "testimoni/index", function() {
            setActiveTab("article-form-tab");
        });
    }

    function getArticle() {
        if ($.fn.dataTable.isDataTable('#testimoni_table')) {
            tableArticle = $('#testimoni_table').DataTable();
        } else {
            tableArticle = $('#testimoni_table').DataTable({
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }],
                "order": [
                    [1, 'asc']
                ],
                "ajax": base_url + 'objects/testimoni',
                "columns": [{
                        "data": "no"
                    },
                    {
                        "data": "gbr"
                    }, {
                        "data": "isi"
                    },
                    {
                        "data": "user"
                    },
                    {
                        "data": "rumahsakit",
                    },
                    {
                        "data": "aksi",
                        "width": "20%"
                    }
                ],
                "ordering": true,
                "deferRender": true,
                "order": [
                    [0, "asc"]
                ],
                "fnDrawCallback": function(oSettings) {
                    utilsArticle();
                }
            });
            $('#testimoni_table').DataTable().on('order.dt search.dt', function() {
                $('#testimoni_table').DataTable().column(0, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        }

    }
    getArticle();

    function utilsArticle() {
        $("#testimoni_table .editBtn").on("click", function(e) {
            e.preventDefault();
            loadContent(base_url + 'testimoni/index/' + $(this).attr('href').substring(1));
        });

        $("#testimoni_table .removeBtn").on("click", function() {
            confirmDelete($(this).attr('href').substring(1));
        });
    }
    // actiion submit
    $('#testimonial_form').on('submit', function(event) {
        event.preventDefault();
        var datastring = new FormData(this);
        loading('loading', true);

        $.ajax({
            url: '<?= $action ?>',
            data: datastring,
            dataType: 'json',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            success: function(json) {
                loading('loading', false);
                if (json.data.code === 0) {
                    if (json.data.message == '') {
                        genericAlert('Penyimpanan data gagal!', 'error', 'Error');
                    } else {
                        genericAlert(json.data.message, 'warning', 'Peringatan');
                    }
                } else {
                    var page = 'testimoni/';
                    page += json.data.last_id;
                    genericAlert('Penyimpanan data berhasil', 'success', 'Sukses');
                    loadContent(base_url + '/testimoni/index.html');
                }
            },
            error: function(data, error, jqxhr) {
                loading('loading', false);
                genericAlert('Terjadi kesalahan!', 'error' + jqxhr, 'Error');
            }
        });
    });

    function getData(idx) {
        $.ajax({
            url: base_url + 'objects/testimoni',
            data: 'model-input=article&key-input=article_id&value-input=' + idx,
            dataType: 'json',
            type: 'POST',
            cache: false,
            success: function(json) {
                if (json.data.code === 0) {
                    loginAlert('Akses tidak sah');
                } else {
                    $("#publish-input").val(json.data.object.is_publish);
                    $("#category-input").val(json.data.object.article_category_id);
                    $("#rumah_sakit-input").val(json.data.object.id_rumahsakit);
                    $("#action-input").val('2');
                    $("#value-input").val(idx);
                    if (json.data.object.img !== null) {
                        $("#foto-div").html('<img src="<?php echo base_url(); ?>assets/files/article/' + json.data.object.img + '" class="img img-thumbnail img-small">');
                    }
                }
            }
        });
    }

    function confirmDelete(n) {
        swal({
                title: "Konfirmasi Hapus",
                text: "Apakah anda yakin akan menghapus data ini?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: " Ya",
                closeOnConfirm: false
            },
            function() {
                loading('loading', true);
                setTimeout(function() {
                    $.ajax({
                        url: base_url + '/testimoni/destroy/' + n,
                        data: 'model-input=article&action-input=3&key-input=article_id&value-input=' + n,
                        dataType: 'json',
                        type: 'POST',
                        cache: false,
                        success: function(json) {
                            loading('loading', false);
                            if (json.data.code === 1) {
                                genericAlert('Hapus data berhasil', 'success', 'Sukses');
                                refreshTable();
                            } else if (json.data.code === 2) {
                                genericAlert('Hapus data gagal!', 'error', 'Error');
                            } else {
                                genericAlert(json.data.message, 'warning', 'Perhatian');
                            }
                        },
                        error: function() {
                            loading('loading', false);
                            genericAlert('Tidak dapat hapus data!', 'error', 'Error');
                        }
                    });
                }, 100);
            });
    }

    function refreshTable() {
        tableArticle.ajax.url(base_url + '/objects/testimoni').load();
    }

    function CKupdate() {
        for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();
    }
</script>