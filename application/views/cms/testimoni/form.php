<form class="form-horizontal" role="form" id="testimonial_form" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title-input" class="col-md-3 control-label">Isi testimonial</label>
        <div class="col-md-6">
            <textarea class="form-control" name="isi" id="content-input">
                         <?= $isi ?></textarea>
        </div>
    </div>
    <?= parsing_rs_select()  ?>
    <div class="form-group">
        <label for="title-input" class="col-md-3 control-label">Gambar</label>
        <div class="col-md-6">
            <?php
            if ($gambar != '') {
                $fld = 'assets/image/testimoni/';
                echo '<img src="' . $fld . '/' . $gambar . '" class="img-responsive" style="width:100px;height:100px">';
            }
            ?>
            <input type="file" class="form-control" id="title-input" name="gambar" placeholder="Title" value="" />
        </div>
    </div>
    <div class="form-group">

        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button type="submit" id="btn-save" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
            <button type="reset" class="btn btn-default" onclick="setActiveTab('article-table-tab');"><i class="fa fa-undo"></i> Cancel</button>
        </div>
    </div>
</form>