<ul class="breadcrumb">
    <li><a href="../../index.html">Home</a></li>
    <li><a href="../../artikel.html">Artikel Kesehatan</a></li>
    <li class="active">Artikel Kesehatan - Rumah Sakit Permata Keluarga - Mengenal Artroskopi, Pemeriksaan Penting untuk Kesehatan Sendi</li>
</ul>
<META http-equiv='Content-Type' content='text/html; charset=UTF-8'>

<head prefix="article: /artikel">
    <meta property="og:title" content="Mengenal Artroskopi, Pemeriksaan Penting untuk Kesehatan Sendi" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://permata.s3.ap-southeast-1.amazonaws.com/images/article/full/82_15_mengenal-artroskopi-pemeriksaan-penting-untuk-kesehatan-sendi.jpg" />
    <meta property="og:url" content="/artikel/artikel-kesehatan/artroskopi" />
    <meta property="og:description" content="Artroskopi adalah pembedahan kecil untuk mendiagnosis dan mengatasi berbagai masalah pada persendian. Inilah kriteria pasien yang perlu menjalani artroskopi serta prosesnya" />
    <meta property="og:site_name" content="Rumah Sakit Permata Keluarga" />
    <!--    <meta property="og:article:published_time" content="2020-12-15"/>-->
    <!--    <meta property="og:article:author" content="Rumah Sakit Permata Keluarga"/>-->
    <!DOCTYPE html>
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "../../../connect.facebook.net/en_US/sdk.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="site-about">
        <div class="site-bg lazy" data-src="/img/bg3.jpg">
            <div class="row judul-back">
                <h1 class="title-lokasi-home col-md-4 col-xs-12">Artikel</h1>
            </div>
            <div class="card card-site card-in-detail">
                <div class="share-top">
                    <div class="share-wrap">
                        <div class="share-in content-gray">
                            <div>SHARE</div>
                            <div class="icon-share">
                                <div class="fb-share-button" data-href="/artikel/artikel-kesehatan/ artroskopi" data-layout="button" data-size="small" data-mobile-iframe="true">
                                    <!--                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=/artikel/ artikel-kesehatan/ artroskopi&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">-->
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?app_id=151853772152611&amp;sdk=joey&amp;u=/artikel/artikel-kesehatan/artroskopi&amp;display=popup&amp;ref=plugin&amp;src=share_button">
                                        <span class="img-share-in share-fb-in"></span>
                                    </a>
                                </div>

                                <a target="_blank" class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Mengenal+Artroskopi%2C+Pemeriksaan+Penting+untuk+Kesehatan+Sendi+-+Baca+Selengkapnya+di+%2Fartikel%2Fartikel-kesehatan%2Fartroskopi">
                                    <div class="img-share-in share-twitter-in"></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-in-center-content">
                        <ul class="breadcrumb">
                            <?php
                            if ($this->uri->segment(2) == 'informasi') {
                                $name = 'Informasi Kesehatan';
                                $par  = $this->uri->segment(2);
                            } else if ($this->uri->segment(2) == 'promo') {
                                $name =  'Promosi Kesehatan';
                                $par  = $this->uri->segment(2);
                            } else if ($this->uri->segment(2) == 'loker') {
                                $name =  'Informasi lowongan kerja';
                                $par  = $this->uri->segment(2);
                            } else if ($this->uri->segment(2) == 'fasilitas') {
                                $name =  'Fasilitas rumah sakit';
                                $par  = $this->uri->segment(2);
                            }
                            ?>
                            <li><a href="<?= base_url($rs . '/home') ?>"><i class="icon icon-home"></i></a></li>
                            <li><a href="<?= base_url($rs . '/' . $par) ?>">
                                    <?= $name ?>
                                </a></li>
                            <li class="active"> <?= $value->title ?></li>
                        </ul>
                        <div class="title-in text-pink">
                            <h1>
                                <?= $value->title ?></h1>
                        </div>
                        <p class="author-name">Ditulis oleh:
                            Rumah Sakit Permata Keluarga </p>
                        <p class="date-posted">
                            Selasa, 15 Desember 2020 </p>
                        <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/image/article/<?= $value->img ?>" />
                        <div class="content-in content-gray"> <?= ($value->content); ?></div>
                    </div>
                    <div class="in-change"></div>
                </div>

                <?php if ($this->uri->segment(2) == 'informasi' || $this->uri->segment(2) == 'fasilitas') {  ?>
                    <div class="baca-juga-in hidden-xs hidden-sm">
                        <h1 class="title-lokasi-home"><?= ucfirst($this->uri->segment(2)) ?> Lainnya </h1>
                        <main class="grid-main-article baca-content-in">
                            <?php foreach ($infos->result() as $info) :
                                switch ($info->rs_id) {
                                    case 1:
                                        $nrs = 'lippo';
                                        break;
                                    case 2:
                                        $nrs = 'jababeka';
                                        break;
                                    case 4:
                                        $nrs = 'galuhmas';
                                        break;
                                }
                            ?>
                                <article class="grid-article" onclick="location.href='<?= base_url(); ?><?= $nrs ?>/informasi/<?= $info->link ?>';">
                                    <div class="overlay-tips">
                                        <div class="crop-card">
                                            <img class="card-img-top lazy" data-src="<?php echo base_url(); ?>assets/image/article/<?= $info->img ?>" />
                                        </div>
                                        <div class="card-health-overlay">
                                            <button class="btn btn-white btn-health button-read-more">Baca</button>
                                        </div>
                                    </div>
                                    <div class="card-body card-body-tips">
                                        <h4 class="card-title-tips"><a href="<?= base_url(); ?><?= $nrs ?>/informasi/<?= $info->link ?>"><?= $info->title ?></a></h4>
                                        <small class="text-muted"><?= $info->created_datetime ?></small>
                                        <p class="card-text card-text-tips"><?= strip_tags(substr($info->content, 0, 300)) . '....'; ?> </p>
                                    </div>
                                </article>
                            <?php endforeach ?>
                        </main>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    </div>