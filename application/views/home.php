<meta property="og:title" content="Home Rumah Sakit Permata Keluarga" />
<meta property="og:type" content="website">
<meta property="og:image" content="/img/logo.png" />
<meta property="og:url" content="" />
<div class="site-index">
    <div class="overlay-image">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators carousel-indicators-top">
                <?php $j = 0;
                foreach ($sliders as $f => $slider) :
                    $active = ($j == 0) ? 'class="active"' : '';
                ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?= $j ?>" <?= $active ?>></li>
                <?php $j++;
                endforeach ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner carousel-radius">
                <?php $j = 0;
                foreach ($sliders as $f => $slider) :
                    $active = ($j == 0) ? 'active' : '';
                ?>
                    <div class="item <?= $active ?>">
                        <div class="overlay-carousel-home" itemscope itemtype="https://schema.org/Organization">
                            <!-- <a href="#">
                                        <img class="white-mask-home lazy" data-src="<?php echo base_url(); ?>assets/image/slide/<?= $slider->img ?>" media="(max-width: 767px)" />
                                    </a> -->
                            <div class="crop-carousel" onclick="location.href='#'" itemprop="url" content="#">
                                <a href="#">
                                    <picture class="lazy">
                                        <data-src srcset="<?php echo base_url(); ?>assets/image/slide/<?= $slider->img ?>" media="(max-width: 767px)"></data-src>
                                        <img class="img-responsive image-carousel-border" itemprop="image" content="<?php echo base_url(); ?>assets/image/slide/<?= $slider->img ?>" src="<?php echo base_url(); ?>assets/image/slide/<?= $slider->img ?>" alt="<?= $slider->title ?>" />
                                    </picture>
                                </a>

                                <!-- <picture>
                                            <source media="(max-width: 767px)" srcset="<?php echo base_url(); ?>assets/image/slide/<?= $slider->img ?>">
                                            <img class="img-responsive image-carousel-border" src="<?php echo base_url(); ?>assets/image/slide/<?= $slider->img ?>" alt="Terpercaya mengutamakan pasien">
                                        </picture> -->

                            </div>
                        </div>
                    </div>
                <?php $j++;
                endforeach ?>
            </div>

            <!-- Controls -->
            <a class="left carousel-control carousel" href="#carousel-example-generic" role="button" data-slide="prev">
                <div class="left-icon"></div>
                <span class="sr-only">Previous</span>
            </a>
            <a id="startchange" class="right carousel-control carousel" href="#carousel-example-generic" role="button" data-slide="next">
                <div class="right-icon"></div>
                <span class="sr-only">Next</span>
            </a>
            <!-- end of Carousel Controls -->
            <div class="card-deck">
                <div>
                    <div class="card-top card-radius layanan1">
                        <div class="icon-arrow"></div>
                        <div class="card-body">
                            <h4 class="card-title text-pink">Gawat Darurat</h4>
                            <p class="card-text hidden-sm hidden-xs">Kami siap memberikan pertolongan pertama hingga penanganan intensif selama 24 jam.</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card-top card-radius janji1">
                        <div class="icon-arrow"></div>
                        <div class="card-body">
                            <h4 class="card-title text-pink">Buat Janji Dokter</h4>
                            <p class="card-text hidden-sm hidden-xs">Pastikan kunjungan anda lebih nyaman dengan mengatur jadwal terlebih dahulu dengan dokter-dokter kami.</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card-top card-radius konsul1">
                        <div class="icon-arrow"></div>
                        <div class="card-body">
                            <h4 class="card-title text-pink">Konsultasi Dokter Online</h4>
                            <p class="card-text hidden-sm hidden-xs">Kemudahan ada dalam genggaman Anda, konsultasikan masalah kesehatan Anda dengan dokter kami</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card-top card-radius korona1" onclick="location.href='#">
                        <div class="icon-arrow"></div>
                        <div class="card-body">
                            <h4 class="card-title text-pink">Buat janji Tes Covid 19</h4>
                            <p class="card-text hidden-sm hidden-xs">Buat janji dan pembayaran Tes Covid 19</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="lokasi-kami-home">
        <h2 class="title-lokasi-home text-center">Lokasi Kami</h2>
        <div class="card card-lokasi-kami">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-3 col-xs-12 card card-content card-home-lk">
                        <div class="dropdown dropdown-res">
                            <button class="btn btn-dropdown-res dropdown-toggle" type="button" data-toggle="collapse" id="name-klinik-home">
                                Bekasi
                                <!-- <span id="name-klinik-home">Bekasi</span> -->
                                <!-- <span class="caret"></span> -->
                            </button>
                            <div class="scroll-dropdown-lkhome">
                                <ul class="dropdown-menu container-res-klinik-home nav flex-column nav-pills nav-justified" id="res-klinik-home">
                                    <?php
                                    $j = 1;
                                    foreach ($rmahsakit->result() as $list) :
                                        switch ($list->id_rumahsakit) {
                                            case 1:
                                                $fnrs = 'lippo';
                                                break;
                                            case 2:
                                                $fnrs = 'jababeka';
                                                break;
                                            case 4:
                                                $fnrs = 'galuhmas';
                                                break;
                                        }
                                        $actv = ($rs == $fnrs) ? 'active' : "";
                                    ?>
                                        <li class="nav-item <?= $actv ?>">
                                            <a class="nav-link" to="<?= base_url($fnrs . '/home') ?>" data-toggle="tab" role="tab"><?= strtoupper(strtolower($list->nama_rumahsakit)) ?></a>
                                        </li>
                                    <?php $j++;
                                    endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <?php
                    error_reporting(0);
                    $this->load->helper('url');
                    $rs = $this->uri->segment(1);
                    $list_rs = $this->frontmodel->get_opt_rs();
                    $id_rumahsakit = ($this->uri->segment(1)) ?  $this->uri->segment(1) : 'lippo';
                    switch ($id_rumahsakit) {
                        case 'lippo':
                            $alamat = '15';
                            $tlp = '18';
                            $igd = '11';
                            $fax = '12';
                            $wa  = '46';
                            $fb  = '29';
                            $ig  = '47';
                            $ytb = '48';
                            $tiktok = '48';
                            $email = '26';
                            $fanspage = '41';
                            $id_resultrs = '1';
                            break;
                        case 'jababeka':
                            $alamat = '16';
                            $tlp = '24';
                            $igd = '1';
                            $fb  = '30';
                            $wa  = '51';
                            $ig  = '47';
                            $ytb = '39';
                            $tiktok = '52';
                            $fax = '2';
                            $email = '27';
                            $fanspage = '42';
                            $id_resultrs = '2';
                            break;
                        case 'galuhmas':
                            $alamat = '52';
                            $tlp = '53';
                            $igd = '55';
                            $fax = '54';
                            $wa  = '56';
                            $email = '57';
                            $fb  = '58';
                            $ig  = '59';
                            $ytb = '62';
                            $tiktok = '59';
                            $fanspage = '43';
                            $id_resultrs = '4';
                            break;
                    }
                    $alamats = $this->frontmodel->getSingleSettingKontak($alamat, $id_resultrs);
                    $tlps = $this->frontmodel->getSingleSettingKontak($tlp, $id_resultrs);
                    $igds = $this->frontmodel->getSingleSettingKontak($igd, $id_resultrs);
                    $faxs = $this->frontmodel->getSingleSettingKontak($fax, $id_resultrs);
                    $was = $this->frontmodel->getSingleSettingKontak($wa, $id_resultrs);
                    $fbs = $this->frontmodel->getSingleSettingKontak($fb, $id_resultrs);
                    $igs = $this->frontmodel->getSingleSettingKontak($ig, $id_resultrs);
                    $tiks = $this->frontmodel->getSingleSettingKontak($tiktok, $id_resultrs);
                    $ytbs = $this->frontmodel->getSingleSettingKontak($ytb, $id_resultrs);
                    $emails = $this->frontmodel->getSingleSettingKontak($email, $id_resultrs);
                    $fanspages = $this->frontmodel->getSingleSettingKontak($fanspage, $id_resultrs);
                    ?>

                    <div class="col-md-9 col-xs-12">
                        <div class="rumah-sakit-home tab-content">
                            <?php $y = 1;
                            foreach ($rmahsakit->result()  as $rsm) {

                                switch ($rsm->id_rumahsakit) {
                                    case 1:
                                        $rsname = 'lippo';
                                        $gambar =  base_url("assets/img/rumahsakit/rspklippo.png");

                                        break;
                                    case 2:
                                        $rsname = 'jababeka';
                                        $gambar =  base_url("assets/img/rumahsakit/rspkjb.jpg");

                                        break;
                                    case 4:
                                        $rsname = 'galuhmas';
                                        $gambar = base_url("assets/img/rumahsakit/rspkkrw.png");
                                        break;
                                }
                                // var_dump($rs . '<br />' . $rsname);
                                if ($this->uri->segment(1) == '') {
                                    $sactive = ('lippo' == $rsname) ? 'active' : '';
                                } else {
                                    $sactive = ($rs == $rsname) ? 'active' : '';
                                }

                            ?>
                                <div class="clearfix"></div>
                                <div class="tab-pane fade in <?= $sactive . ' ' . $y ?>" id="<?= $rsname ?>">
                                    <section class="home-image-klinik  lazy" data-src="<?= $gambar
                                                                                        ?>" data-imgresponsive="<?= $gambar
                                                                                                                ?>" style="background-size:cover; background-position: center center; background-repeat: no-repeat;" itemprop="image" content="<?= $gambar
                                                                                                                                                                                                                                                ?>">

                                        <div class="nama-klinik">
                                            <h3 class="white" itemprop="name">
                                                Rumah Sakit Permata Keluarga <?= $rs ?> </h3>
                                            <div class="row">
                                                <div class="col-md-5 col-xs-12">
                                                    <div class="detail-rs-home">
                                                        <div class="row">
                                                            <div class="col-md-1 col-xs-2">
                                                                <span class="glyphicon glyphicon-map-marker white" aria-hidden="true"></span>
                                                            </div>
                                                            <div class="col-md-10 col-xs-9">
                                                                <span class="white" itemprop="address"><?= $alamats[0]->value_set  ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 hidden-sm hidden-xs">
                                                    <div class="detail-rs-home">
                                                        <div class="col-md-1 col-xs-2">
                                                            <span class="glyphicon glyphicon-earphone white" aria-hidden="true"></span>
                                                        </div>
                                                        <div class="col-md-10 col-xs-8">
                                                            <span class="white" itemprop="description">
                                                                <p>Informasi :&nbsp;<span style="font-weight: 400;"><?= $tlps[0]->value_set  ?></span></p>
                                                                <p>IGD :&nbsp;<span style="font-weight: 400;"><?= $igds[0]->value_set ?></span></p>
                                                                <p>Fax :&nbsp;<span style="font-weight: 400;"><?= $faxs[0]->value_set ?></span></p>
                                                                <p><span style="font-weight: 400;">Whatsapp : <?= $was[0]->value_set ?></span></p>
                                                            </span>
                                                            <meta itemprop="Telephone" content="<?= $tlps[0]->value_set ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <meta itemprop="hasMap" content="https://www.google.co.id/maps/place?q=place_id:ChIJC5Y_5DqMaS4RaIQfCdVU3JQ" />
                                                <meta itemprop="email" content="<?= $emails[0]->value_set ?>" />
                                                <meta itemprop="" content="img/logo.png" />
                                                <meta itemprop="openingHours" content="24:00" />
                                                <meta itemprop="sameAs" content="https://www.facebook.com/" />
                                                <div class="col-md-3 col-xs-12">
                                                    <a class="btn btn-border-white button-selengkapnya" itemprop="url" href="<?= base_url($rsname . '/jadwal') ?>">Selengkapnya</a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            <?php $y++;
                            } ?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="promotion-home">
            <div class="row row-see-all">
                <h2 class="title-lokasi-home text-center col-md-9 col-xs-12">Promosi</h2>
            </div>
            <div class="row row-promotion-home">
                <?php foreach ($promos->result() as $promo) : ?>
                    <div class="card-health col-md-4 col-xs-12" itemscope itemtype="https://schema.org/Thing" onclick="location.href='<?= base_url(); ?><?= $rs ?>/informasi/<?= $promo->link ?>';">
                        <div class="overlay-tips">
                            <div class="crop-card">
                                <img class="card-img-top lazy" itemprop="image" content="<?php echo base_url(); ?>assets/image/article/<?= $promo->img ?>" src="<?php echo base_url(); ?>assets/image/article/<?= $promo->img ?>" alt="Gambar promo Promo Upgrade Kamar Perawatan" />
                            </div>
                            <div class="card-health-overlay">
                                <a class="btn btn-white btn-health button-read-more" href="<?= base_url(); ?><?= $rs ?>/informasi/<?= $promo->link ?>">Baca</a>
                            </div>
                        </div>
                        <div class="card-body card-body-tips">
                            <h4 class="card-title-tips"><a itemprop="url" href="<?= base_url(); ?><?= $rs ?>/informasi/<?= $promo->link ?>"><span itemprop="name"><?= $promo->title ?></span></a></h4>
                            <!--<a class="hidden-sm hidden-xs" href=""><small class="text-muted">Rs. Rumah Sakit Permata Keluarga </small></a>-->
                            <div class="location-promotion">
                                <span class="glyphicon glyphicon-map-marker blue" aria-hidden="true"></span>
                                <span class="content-gray">Bintaro</span>
                            </div>
                            <p class="card-text card-text-tips hidden-sm hidden-xs" itemprop="description">

                                <?= strip_tags(substr($promo->content, 0, 300)) . '....'; ?> </p>
                        </div>
                    </div>
                <?php endforeach ?>

                <h5 class="relative-see-all col-md-1 col-xs-12">
                    <a class="text-pink" href="promo.html"><b>Lihat Semua</b></a>
                </h5>
            </div>

            <div class="pasien-kami-home">
                <h2 class="title-lokasi-home text-center">Pasien Kami</h2>
                <div id="carouselpasien" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <p>
                    <ol class="carousel-indicators carousel-indicators-pasien">
                        <?php $j = 0;
                        foreach ($testimoni->result_array() as  $testimonis) {
                            $active = ($j == 0) ? 'class="active"' : '';
                        ?>
                            <li data-target="#carouselpasien" data-slide-to="<?= $j ?>" <?= $active ?>></li>
                        <?php $j++;
                        } ?>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner carousel-testimoni" style="background-color: #0099ff;">
                        <?php
                        // print_r($testimoni);
                        $no = 1;
                        foreach ($testimoni->result_array() as  $testimonis) {
                            $facti = ($no == 1) ? 'active' : '';
                            $fld =   'assets/image/testimoni/';
                            $gambar = $testimonis['gambar'];
                        ?>

                            <div class="item item-pasien <?= $facti ?>" itemscope itemtype="https://schema.org/Review">
                                <!-- <picture class="lazy">
                                <data-src srcset="<?= base_url($fld . '/' . $gambar) ?>" media="(max-width: 767px)"></data-src>
                                <data-img class="bg-carousel-pasien" src="<?= base_url($fld . '/' . $gambar) ?>" alt="Gradient background" onerror="this.onerror=null;this.src='https://1.bp.blogspot.com/-QpypoqMi4kk/XztIPVa6PgI/AAAAAAAAB0c/sMbi7Ue688IvBBm32pMpIl3ZOqw5D7nDwCLcBGAsYHQ/s2048/blue%2Babstract%2Bbackground.jpg';"></data-img>
                            </picture> -->
                                <div class="carousel-caption carousel-caption-pasien">
                                    <!--<div class="row">-->
                                    <div class="col-md-3">
                                        <div class="crop-info-dokter"> <img class="foto-info-dokter foto-testimoni lazy" data-src="<?= base_url($fld . '/' . $gambar) ?>" itemprop="image" content="<?= base_url($fld . '/' . $gambar) ?>" alt="<?= $testimonis->isi ?>"></div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="testimoni-text" itemprop="description">
                                            <p>
                                                <?= ($testimonis['isi']) ?></p>
                                        </div>
                                        <div class="text-left">
                                            <button class="btn btn-white-testimoni button-read-more" data-toggle="modal" data-target="#modal-testimonial-detail" data-foto="<?= base_url($fld . '/' . $gambar) ?>" data-nama="Testimonial Astira Vern" data-spesialis="" data-text='<?= strip_tags($testimonis['isi']) ?>'>Baca Selengkapnya</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                <?php $no++;
                        } ?>
                </div>
                <!-- Controls -->
                <a class="left carousel-control carousel-control-pasien" href="#carouselpasien" role="button" data-slide="prev">
                    <div class="left-icon-pasien"></div>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control carousel-control-pasien" href="#carouselpasien" role="button" data-slide="next">
                    <div class="right-icon-pasien"></div>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div class="modal" tabindex="-1" role="dialog" id="modal-testimonial-detail" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="model-content">
                    <div class="modal-header modal-header-custom">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row testimoni-modal">
                            <div class="col-md-4 foto-detail">
                                <img class="foto-testimoni-detail lazy" data-src="" />
                            </div>
                            <div class="col-md-8">
                                <h2 class="text-pink" id="nama-testimoni"><b>Ny. Ratih Wulandari</b></h2>
                                <!--                        <h3 class="content-gray" id="spesialis-testimoni">Diabetes</h3>-->
                                <div class="pre-scrollable">
                                    <div class="scroll-testimoni">
                                        <p class="text-testimoni">Saya dan suami baru dikaruniai buah hati pada tahun ke-3 pernikahan kami setelah menjalani proses inseminasi dibantu oleh Dr. Antony Atmadja, Sp.OG</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="health-tips-home">
            <div class="row row-see-all">
                <h2 class="title-lokasi-home text-center col-md-9 col-xs-12">Youtube Feeds</h2>
            </div>
            <div class="row card-health-tips">


                <?php
                // var_dump($video->result_array());
                // die;
                foreach ($video->result_array() as $vid) : ?>
                    <div class="card-top card-top-health card-top-health-youtube col-md-4 col-xs-12">
                        <div class="overlay-tips">
                            <a href="<?= $vid['content'] ?>" target="_blank" rel="noopener noreferrer" title="<?= $vid['title'] ?>" data-lity>
                                <div class="crop-card">
                                    <img class="card-img-top lazy" data-src="<?php echo base_url(); ?>assets/image/article/<?= $vid['img'] ?>" alt="Thumbnail Youtube <?= $vid['title'] ?>" itemprop="image" content="<?php echo base_url(); ?>assets/image/article/<?= $vid['img'] ?>" />
                                </div>
                                <div class="card-health-overlay">
                                    <button class="btn btn-white btn-health button-read-more">Lihat</button>
                                </div>
                            </a>
                        </div>
                        <div class="card-body card-body-tips">
                            <small class="text-muted" itemprop="datePublished" content="<?= $vid['created_datetime'] ?> ">
                                <?= $vid['created_datetime'] ?> </small>
                            <h4 class="card-title-tips">
                                <a href="<?= $vid['link'] ?>" target="_blank" rel="noopener noreferrer" title="<?= $vid['title'] ?>" data-lity>
                                    <?= $vid['title'] ?></a>
                            </h4>

                        </div>
                        <p class="card-text card-text-tips hidden-sm hidden-xs" itemprop="text">
                            <?= strip_tags(substr($info->content, 0, 300)) . '....'; ?> </p>
                    </div>

                <?php endforeach ?>
                <!-- <div class="card-top card-top-health card-top-health-youtube col-md-4 col-xs-12">
                    <div class="overlay-tips">
                        <a href="http://www.youtube.com/watch?v=Qb2GX7MLcGs" target="_blank" rel="noopener noreferrer" title="Rumah Sakit Permata Keluarga : Intip Rahasia dibalik Keajaiban Bayi Tabung" data-lity>
                            <div class="crop-card">
                                <img class="card-img-top lazy" data-src="https://i.ytimg.com/vi/Qb2GX7MLcGs/hqdefault.jpg" alt="Thumbnail Youtube Rumah Sakit Permata Keluarga : Intip Rahasia dibalik Keajaiban Bayi Tabung" itemprop="image" content="../i.ytimg.com/vi/Qb2GX7MLcGs/hqdefault.jpg" />
                            </div>
                            <div class="card-health-overlay">
                                <button class="btn btn-white btn-health button-read-more">Lihat</button>
                            </div>
                        </a>
                    </div>
                    <div class="card-body card-body-tips">
                        <small class="text-muted" itemprop="datePublished" content="25 Januari 2021">
                            25 Januari 2021 </small>
                        <h4 class="card-title-tips">
                            <a href="http://www.youtube.com/watch?v=Qb2GX7MLcGs" target="_blank" rel="noopener noreferrer" title="Rumah Sakit Permata Keluarga : Intip Rahasia dibalik Keajaiban Bayi Tabung" data-lity>
                                Rumah Sakit Permata Keluarga : Intip Rahasia dibalik Keajaiban Bayi Tabung </a>
                        </h4>
                    </div>
                </div>
                <div class="card-top card-top-health card-top-health-youtube col-md-4 col-xs-12">
                    <div class="overlay-tips">
                        <a href="http://www.youtube.com/watch?v=UefphUlTkqA" target="_blank" rel="noopener noreferrer" title="Rumah Sakit Permata Keluarga : Gizi Seimbang untuk Daya Tahan Tubuh Optimal" data-lity>
                            <div class="crop-card">
                                <img class="card-img-top lazy" data-src="https://i.ytimg.com/vi/UefphUlTkqA/hqdefault.jpg" alt="Thumbnail Youtube Rumah Sakit Permata Keluarga : Gizi Seimbang untuk Daya Tahan Tubuh Optimal" itemprop="image" content="../i.ytimg.com/vi/UefphUlTkqA/hqdefault.jpg" />
                            </div>
                            <div class="card-health-overlay">
                                <button class="btn btn-white btn-health button-read-more">Lihat</button>
                            </div>
                        </a>
                    </div>
                    <div class="card-body card-body-tips">
                        <small class="text-muted" itemprop="datePublished" content="25 Januari 2021">
                            25 Januari 2021 </small>
                        <h4 class="card-title-tips">
                            <a href="http://www.youtube.com/watch?v=UefphUlTkqA" target="_blank" rel="noopener noreferrer" title="Rumah Sakit Permata Keluarga : Gizi Seimbang untuk Daya Tahan Tubuh Optimal" data-lity>
                                Rumah Sakit Permata Keluarga : Gizi Seimbang untuk Daya Tahan Tubuh Optimal </a>
                        </h4>
                    </div>
                </div> -->
                <h5 class="relative-see-all-ha col-md-1">
                    <a class="text-pink" target="_blank" href="https://www.youtube.com/channel/UCaBz2_Zlfe-GPYTu8f4jaBw" title="Channel Youtube Rumah Sakit Permata Keluarga"><b>Lihat Semua</b></a>
                </h5>
            </div>
        </div>
        <!-- end of Youtube-->

        <div class="health-tips-home">
            <div class="row row-see-all">
                <h2 class="title-lokasi-home text-center col-md-9 col-xs-12">Artikel</h2>
            </div>
            <div class="row card-health-tips">

                <head prefix="artikel: /artikel">
                    <?php foreach ($infos->result() as $info) :
                        switch ($info->rs_id) {
                            case 1:
                                $nrs = 'lippo';
                                break;
                            case 2:
                                $nrs = 'jababeka';
                                break;
                            case 4:
                                $nrs = 'galuhmas';
                                break;
                        }
                    ?>

                        <div class="card-top card-top-health col-md-4 col-xs-12" onclick="location.href='<?= base_url(); ?><?= $nrs ?>/informasi/<?= $info->link ?>';" itemscope itemtype="https://schema.org/Article">
                            <meta itemprop="headline" content="Artikel" />
                            <meta itemprop="author" content="Rumah Sakit Permata Keluarga" />
                            <meta itemprop="dateModified" content="2020-12-15" />
                            <meta itemprop="mainEntityOfPage" content="/artikel" />
                            <div class="overlay-tips">
                                <div class="hidden" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                    <meta itemprop="name" content="Rumah Sakit Permata Keluarga" />
                                    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                        <meta itemprop="url" content="img/logo.png" />
                                    </div>
                                </div>
                                <div class="crop-card">
                                    <img class="card-img-top lazy" data-src="<?php echo base_url(); ?>assets/image/article/<?= $info->img ?>" />
                                </div>
                                <div class="card-health-overlay">
                                    <button class="btn btn-white btn-health button-read-more">Baca</button>
                                </div>
                            </div>
                            <div class="card-body card-body-tips">
                                <h4 class="card-title-tips">
                                    <a href="<?= base_url(); ?><?= $nrs ?>/informasi/<?= $info->link ?>" itemprop="url"><span itemprop="name"><?= $info->title ?></span></a>
                                </h4>
                                <small class="text-muted" itemprop="datePublished" content="2020-12-15"><?= $info->created_datetime ?></small>
                                <p class="card-text card-text-tips hidden-sm hidden-xs" itemprop="text">
                                    <?= strip_tags(substr($info->content, 0, 300)) . '....'; ?> </p>
                            </div>
                        </div>

                    <?php endforeach ?>

                    <h5 class="relative-see-all-ha col-md-1">
                        <a class="text-pink" href="<?= base_url(); ?><?= $rs ?>/informasi/"><b>Lihat Semua</b></a>
                    </h5>
            </div>
        </div>
        <!-- end of Health Tips Home-->

    </div>
    <!-- end of SITE-INDEX-->

    <script>
        (function() {
            $('.nav-link').on('click', function() {
                var x = $(this).attr('to');
                window.location.href = x;
                // alert('ss');
            });

        })();
    </script>