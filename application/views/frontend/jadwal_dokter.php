<style>
  .tabs-left,
  .tabs-right {
    border-bottom: none;
    padding-top: 2px;
  }

  .tabs-left {
    border-right: 1px solid #ddd;
  }

  .tabs-right {
    border-left: 1px solid #ddd;
  }

  .tabs-left>li,
  .tabs-right>li {
    float: none;
    margin-bottom: 2px;
  }

  .tabs-left>li {
    margin-right: -1px;
  }

  .tabs-right>li {
    margin-left: -1px;
  }

  .tabs-left>li.active>a,
  .tabs-left>li.active>a:hover,
  .tabs-left>li.active>a:focus {
    border-bottom-color: #ddd;
    border-right-color: transparent;
  }

  .tabs-right>li.active>a,
  .tabs-right>li.active>a:hover,
  .tabs-right>li.active>a:focus {
    border-bottom: 1px solid #ddd;
    border-left-color: transparent;
  }

  .tabs-left>li>a {
    border-radius: 4px 0 0 4px;
    margin-right: 0;
    display: block;
  }

  .tabs-right>li>a {
    border-radius: 0 4px 4px 0;
    margin-right: 0;
  }
</style>
<div class="wrap container-full-default">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
  <div class="lokasi-klinik">
    <div class="site-bg lazy" data-src="/img/bg3.jpg">
      <div class="row judul-back">
        <h1 class="title-lokasi-home col-md-9 col-xs-12">Jadwal Dokter</h1>
        <!-- <div class="search-lk col-md-3 col-xs-12">
          <div class="row">
            <div class="col-md-12 search-top" id="search-direktori">
              <span class="glyphicon glyphicon-map-marker content-gray" aria-hidden="true" style="display: inline;"></span>
              <span>
                <div class="kv-plugin-loading loading-w0">&nbsp;</div><select id="w0" class="form-control" name="lokasi_klinik" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_9930966c" style="display:none">
                  <option value="3">Bekasi</option>
                  <option value="2">Bekasi Timur</option>
                  <option value="18">Bina Husada</option>
                  <option value="15">Bintaro</option>
                  <option value="13">Cibubur</option>
                  <option value="4">Cikarang</option>
                  <option value="5">Depok</option>
                  <option value="14">Gading Serpong</option>
                  <option value="6">Kalideres</option>
                  <option value="1">Kelapa Gading</option>
                  <option value="8">Kemayoran</option>
                  <option value="12">Kenjeran</option>
                  <option value="17">Pondok Tjandra</option>
                  <option value="16">Pratama Jatiasih</option>
                  <option value="9">Surabaya</option>
                  <option value="10">Tegal</option>
                  <option value="11" selected="">Waru</option>
                </select>
              </span>

            </div>
          </div>
        </div> -->
      </div>
      <div class="card card-site card-bottom-klinik">
        <div class="image-full-klinik">
          <!--<div id="carousel-gallery-klinik" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" id="lightgallery">
              <?php $y = 1;
              foreach ($rmahsakit->result()  as $rsm) {
                switch ($rsm->id_rumahsakit) {
                  case 1:
                    $rsname = 'lippo';
                    $gambar = base_url("assets/img/rumahsakit/rspklippo.png");
                    break;
                  case 2:
                    $rsname = 'jababeka';
                    $gambar = base_url("assets/img/rumahsakit/rspkjb.jpg");
                    break;
                  case 4:
                    $rsname = 'galuhmas';
                    $gambar =  base_url("assets/img/rumahsakit/rspkkrw.png");
                    break;
                }
                $sactive = ($rs == $rsname) ? 'active' : '';
              ?>
                <img class="img-responsive" src="<?= $gambar ?>">
              <?php $y++;
              } ?>

            </div>-->
        </div>
      </div>
      <div class="loading-klinik text-center hidden">
        <h2><span class="glyphicon glyphicon-refresh is-loading"></span> Loading...</h2>
      </div>
      <div class="klinik-content" style="display: block;">
        <div class="nav-klinik">
          <ul class="nav nav-pills nav-klinik-top" id="pills-klinik-tab" role="tablist">

            <li class="nav-item active" style="left: 0%;">
              <a class="nav-link" id="pills-profile-tab" data-toggle="pill" data-target="#informasi-dokter" href="<?= base_url('/' . $rs . '/jadwal') ?>#informasi-dokter" role="tab" aria-controls="informasi-dokter" aria-expanded="true">Informasi Dokter</a>
            </li>
            <li class="nav-item" style="left: 0%;">
              <a class="nav-link" id="lokasi-rumah-sakit" data-toggle="pill" data-target="#lokasi-rs" href="<?= base_url('/' . $rs . '/jadwal') ?>#lokasi-rs" role="tab" aria-controls="lokasi-rs" aria-expanded="true">Kontak</a>
            </li>
            <!-- Controls -->
            <!--<button class="hidden-md hidden-lg btn left carousel-control clinic-location" role="button" data-slide="prev">
                <div class="left-icon"></div>
                <span class="sr-only">Previous</span>
              </button>
              <button id="startchange" class="hidden-md hidden-lg btn right carousel-control clinic-location" role="button" data-slide="next">
                <div class="right-icon"></div>
                <span class="sr-only">Next</span>
              </button>-->
          </ul>

          <div class="tab-content" id="pills-tabContent">


            <div class="tab-pane active" id="informasi-dokter" role="tabpanel" aria-labelledby="pills-profile-tab">
              <div class="row">
                <div class="col-md-3 col-sm-5 col-xs-12 title-lokasi-klinik">
                  <h2>Dokter</h2>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12 col-md-push-2 form-info-dokter">
                  <div class="form-group">
                    <!-- <label for="spesialisasi-info-dokter"><b>Klinik</b></label> -->
                    <div class="hidden">
                      <form id="jadwal-form" action="https://www.permata.com/buat-janji/search-result" method="get">
                        <div class="form-group field-jadwalform-id_clinic required">

                          <p class="help-block help-block-error"></p>

                          <input type="hidden" id="jadwalform-id_clinic" class="form-control" name="JadwalForm[id_clinic]" value="11">

                        </div>
                        <div class="form-group field-form-specialization required">

                          <p class="help-block help-block-error"></p>

                          <input type="hidden" id="form-specialization" class="form-control" name="JadwalForm[id_specialization]" value="4">

                        </div>
                        <div class="form-group field-jadwalform-tanggal required">

                          <p class="help-block help-block-error"></p>

                          <input type="hidden" id="jadwalform-tanggal" class="form-control" name="JadwalForm[tanggal]" value="2021-03-02">

                        </div>
                        <div class="form-group field-form-namadokter">

                          <p class="help-block help-block-error"></p>

                          <input type="hidden" id="form-namadokter" class="form-control" name="JadwalForm[nama_dokter]" value="">

                        </div><button type="submit" id="form-submit" name="save-button">Cari</button>
                      </form>
                    </div>
                    <select onchange="hide_table('.'+this.value)" class="form-control" id="bag_ch" style="width: 200px;float: right;margin-top:-50px;">
                      <option value="">Semua Spesialis</option>
                      <?php
                      $bag = $this->frontmodel->get_opt_bagian($this->uri->segment(1));
                      foreach ($bag as $key => $v) {
                      ?>
                        <option data-spesial="<?= $v->nama_bagian; ?>" value="<?= 'bag_' . $v->id_bagian; ?>"><?= $v->nama_bagian; ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="loading-dokter text-center hidden">
                <h2><span class="glyphicon glyphicon-refresh is-loading"></span> Loading...</h2>
              </div>
              <div class="row list-info-dokter" id="info-dokter-content">

                <!-- looping data -->

                <?php foreach ($jadwal_dokter as $k => $v) :
                  if ($k == 0 || (isset($jadwal_dokter[$k - 1]->id_dokter)) && $jadwal_dokter[$k - 1]->id_dokter != $v->id_dokter) {
                ?>
                    <div class="row bag_<?= $v->id_bagian ?>" id="bag_<?= $v->id_bagian ?>" itemscope="" itemtype="https://schema.org/Physician" itemid="/<?= $rs ?>/">
                      <div class="col-md-3 col-sm-3 col-xs-12 center-info-dokter">
                        <div class="row">
                          <div class="accordion">
                            <div class="overlay-info-dokter col-xs-4 col-sm-12 col-md-12">
                              <div class="crop-info-dokter">
                                <img class="foto-info-dokter lazy" alt="<?= strip_tags($v->nama_dokter) ?>" itemprop="image" src="<?php echo base_url(); ?>/assets/image/dokter/<?= $v->img ?>" style="display: inline;">
                              </div>
                              <div class="spesialis-bar hidden-xs" itemprop="medicalSpecialty"><?= $v->bagian ?></div>
                            </div>
                            <div class="name-info-dokter col-xs-7 col-sm-12 col-md-12" itemprop="name"><b><?= strip_tags($v->nama_dokter) ?></b></div>

                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12 col-xs-6 col-sm-12">
                            <button class="btn btn-border-pink btn-lihat-info-dokter-v2 info-dokter wekew" data-slug="" data-id="6" data-name="<?= strip_tags($v->nama_dokter) ?>" data-toggle="modal" data-target="#info-dokter-modal<?= $v->id_dokter ?>">
                              <span class="hidden-xs hidden-sm">Lihat</span> Info Dokter
                            </button>
                          </div>
                          <div class="text-right hidden-md hidden-lg hidden-sm col-xs-6">
                            <div class="btn btn-pink button-buat-janji-detail pull-right col-xs-12" data-idsp="4" data-nama="<?= strip_tags($v->nama_dokter) ?>">
                              Buat Janji
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-9 col-sm-9 col-xs-12 hidden-xs">
                        <div class="row content-detail-rs">
                          <div class="col-md-12">
                            <div class="row">
                              <?php $detail = $this->frontmodel->get_listjadwal($v->id_dokter);
                              foreach ($detail->result_array() as $ls_s) { ?>
                                <div class="col-md-2">
                                  <p style="font-weight:bold"><?= $ls_s['hari'] ?></p>
                                  <hr />
                                  <p>
                                    <?php
                                    $a = substr($ls_s['jam'], 0, 5) . ' s/d ' . substr($ls_s['jam_pulang'], 0, 5);
                                    if ($ls_s['oncall']) {
                                      echo '';
                                    } else {
                                      echo $a;
                                    } ?>
                                  </p>
                                </div>
                              <?php  } ?>

                            </div>

                          </div>
                        </div>
                      </div>
                      <div class="row row-bottom hidden">
                        <div class="col-md-12 col-xs-6 col-sm-4 hidden-md hidden-lg">
                          <button class="btn btn-border-pink btn-lihat-info-dokter-v2 info-dokter" data-slug="" data-id="6" data-name="<?= strip_tags($v->nama_dokter) ?>" data-cv="<p><strong>Riwayat Pendidikan :</strong></p><ul><li>SD - SMA : Kediri</li><li>1993&nbsp; : Lulus Dokter, FK Universitas Airlangga Surabaya</li><li>2005&nbsp;: Lulus Spesialis Anak, FK Universitas Airlangga / RSUD Dr.Soetomo Surabaya</li></ul><p><strong>Riwayat Pekerjaan :</strong></p><ul><li>1995 : Dokter Puskesmas Birayang, Hulu Sungai Tengah, Kalimantan Selatan</li><li>2009 : Dokter Spesialis Anak RS Permata Keluarga <?= strtoupper($rs) ?></li></ul><p>&nbsp;</p>" data-spesialis="Anak" data-slugspesialis="anak" data-idspesialis="4" data-idclinic="11" data-nama-rs="Waru" data-src-photo="https://permata.s3.ap-southeast-1.amazonaws.com/images/doctor/full/ahmad-fauzin.jpg" data-toggle="modal" data-target="#info-dokter-modal">
                            <span class="hidden-xs hidden-sm">Lihat</span> Info Dokter
                          </button>
                        </div>
                        <div class="text-right hidden-md hidden-lg col-sm-4 col-xs-6">
                          <div class="btn btn-pink button-buat-janji-detail pull-right col-xs-12" data-idsp="4" data-nama="<?= strip_tags($v->nama_dokter) ?>">
                            Buat Janji
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- modal -->
                    <div class="modal modal-info-dokter" tabindex="-1" role="dialog" aria-hidden="true" id="info-dokter-modal<?= $v->id_dokter ?>" style="display: none;">
                      <div class="modal-dialog modal-dialog-custom">
                        <div class="model-content">
                          <div class="modal-header modal-header-custom">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <h2 class="title-lokasi-home content-gray">Detail Dokter</h2>
                            <div class="row content-detail-dokter">
                              <div class="col-md-4 col-md-push-8 col-xs-12">
                                <div class="crop-info-dokter">
                                  <img class="foto-info-dokter lazy" id="doctor-photo" data-src="../img/user2.png" src="<?php echo base_url(); ?>/assets/image/dokter/<?= $v->img ?>">
                                </div>
                              </div>
                              <div class="col-md-8 col-md-pull-4 col-xs-12">
                                <div class="title-jk"><b>Info Dokter</b></div>
                                <div class="content-gray klinik-jk">Nama Dokter</div>
                                <div class="nama-jk"><b id="doctor-name"><?= strip_tags($v->nama_dokter) ?></b></div>
                                <div class="content-gray klinik-jk">Klinik</div>
                                <div class="nama-jk"><b id="doctor-spesialization"><?= strip_tags($v->ket) ?></b>
                                </div>
                                <div class="content-gray klinik-jk">Rumah Sakit</div>
                                <div class="nama-jk"><b id="doctor-clinic"><?= strtoupper(strip_tags($v->nama_rumahsakit)) ?></b></div>
                              </div>
                            </div>
                            <div class="row content-detail-dokter">
                              <!-- <div class="col-xs-12">
                                  <div class="title-jk" id="title-tentangdokter"><b>Tentang
                                      dokter</b></div>
                                  <div class="scroll-cv-dokter content-gray ps-active-y">
                                    <p class="text-testimoni" id="doctor-cv">
                                    <p><strong>Riwayat Pendidikan :</strong></p>
                                    <ul>
                                      <li>SD - SMA : Kediri</li>
                                      <li>1993&nbsp; : Lulus Dokter, FK Universitas Airlangga Surabaya</li>
                                      <li>2005&nbsp;: Lulus Spesialis Anak, FK Universitas Airlangga / RSUD Dr.Soetomo Surabaya</li>
                                    </ul>
                                    <p><strong>Riwayat Pekerjaan :</strong></p>
                                    <ul>
                                      <li>1995 : Dokter Puskesmas Birayang, Hulu Sungai Tengah, Kalimantan Selatan</li>
                                      <li>2009 : Dokter Spesialis Anak RS Permata Keluarga <?= strtoupper($rs) ?></li>
                                    </ul>
                                    <p>&nbsp;</p>
                                    </p>
                                  </div>
                                </div> -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                <?php  }
                endforeach; ?>
                <!-- end parser loop -->
              </div>

            </div>
            <div class="tab-pane" id="lokasi-rs" role="tabpanel" aria-labelledby="lokasi-rumah-sakit">
              <div class="">
                <div class="rumah-sakit-klinik">
                  <div class="row content-rs-klinik">
                    <div class="col-md-7 col-xs-12 img-rs-klinik">
                      <?php
                      $maps = $this->frontmodel->getSingleSettingMaps($id_rs);
                      if ($maps) {
                        echo
                        "<iframe src='" . $maps[0]->value_set . "' width='1140' height='360' frameborder='0' style='border:0' allowfullscreen></iframe>";
                      }
                      ?>
                    </div>
                    <div class="col-md-4 col-xs-12 detail-rs-lokasi">


                      <h4 class="title-footer-top">Kontak Kami :</h4>
                      <?php
                      $this->load->helper('url');
                      $rs = $this->uri->segment(1);
                      $list_rs = $this->frontmodel->get_opt_rs();
                      $id_rumahsakit = ($this->uri->segment(1)) ?  $this->uri->segment(1) : 'lippo';
                      switch ($id_rumahsakit) {
                        case 'lippo':
                          $alamat = '15';
                          $tlp = '18';
                          $igd = '11';
                          $fax = '12';
                          $wa  = '46';
                          $fb  = '29';
                          $ig  = '47';
                          $ytb = '48';
                          $tiktok = '48';
                          $email = '26';
                          $fanspage = '41';
                          $id_resultrs = '1';
                          break;
                        case 'jababeka':
                          $alamat = '16';
                          $tlp = '24';
                          $igd = '1';
                          $fb  = '30';
                          $wa  = '50';
                          $ig  = '51';
                          $ytb = '39';
                          $tiktok = '52';
                          $fax = '2';
                          $email = '27';
                          $fanspage = '42';
                          $id_resultrs = '2';
                          break;
                        case 'galuhmas':
                          $alamat = '52';
                          $tlp = '53';
                          $igd = '55';
                          $fax = '54';
                          $wa  = '56';
                          $email = '57';
                          $fb  = '58';
                          $ig  = '59';
                          $ytb = '62';
                          $tiktok = '59';
                          $fanspage = '43';
                          $id_resultrs = '4';
                          break;
                      }

                      $alamats = $this->frontmodel->getSingleSettingKontak($alamat, $id_rs);
                      $tlps = $this->frontmodel->getSingleSettingKontak($tlp, $id_rs);
                      $igds = $this->frontmodel->getSingleSettingKontak($igd, $id_rs);
                      $faxs = $this->frontmodel->getSingleSettingKontak($fax, $id_rs);
                      $emails = $this->frontmodel->getSingleSettingKontak($email, $id_rs);
                      if (isset($alamats)) {
                        $fhome = "<li><h5><i class='fa fa-home'></i>&nbsp; " . $alamats[0]->value_set . " </h5></li>";
                      }
                      if (isset($tlps)) {
                        $ftelp = "<li><h5><i class='fa fa-phone'></i>&nbsp; " . $tlps[0]->value_set . " </h5></li>";
                      }
                      if (isset($igds)) {
                        $figds = "<li><h5><i class='fa fa-phone'></i>&nbsp; " . $igds[0]->value_set . " </h5></li>";
                      }
                      if (isset($faxs)) {
                        $ffax = "<li><h5><i class='fa fa-phone'></i>&nbsp; " . $faxs[0]->value_set . " </h5></li>";
                      }
                      if (isset($emails)) {
                        $femails =  "<li><h5><i class='fa fa-envelope'></i>&nbsp; " . $emails[0]->value_set . " </h5></li>";
                      }
                      ?>
                      <h3 class="klinik-title blue"><b>Permata Keluarga <?= strtoupper($rs) ?></b></h3>
                      <div class="detail-rs-home">
                        <div class="row">
                          <div class="col-md-1 col-xs-1">
                            <span class="glyphicon glyphicon-map-marker blue" aria-hidden="true" style="display: inline;"></span>
                          </div>
                          <div class="col-md-10 col-xs-10">
                            <span><?= $alamats[0]->value_set  ?></span>
                          </div>
                        </div>
                      </div>
                      <div class="detail-rs-home">
                        <div class="row">
                          <div class="col-md-1 col-xs-1">
                            <span class="glyphicon glyphicon-earphone blue" aria-hidden="true"></span>
                          </div>
                          <div class="col-md-10 col-xs-10">
                            <span>
                              <p>Informasi :&nbsp;<span style="font-weight: 400;"><?= $tlps[0]->value_set  ?></span></p>
                              <p>IGD :&nbsp;<span style="font-weight: 400;"><?= $igds[0]->value_set ?></span></p>
                              <p>Fax :&nbsp;<span style="font-weight: 400;"><?= $faxs[0]->value_set ?></span></p>
                              <p><span style="font-weight: 400;">Whatsapp : <?= $tlps[0]->value_set ?></span></p>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="detail-rs-home">
                        <div class="row">
                          <div class="col-md-1 col-xs-1">
                            <span class="glyphicon glyphicon-envelope blue" aria-hidden="true"></span>
                          </div>
                          <div class="col-md-10 col-xs-10">
                            <span><?= $emails[0]->value_set ?></span>
                          </div>
                        </div>
                      </div>
                      <div class="detail-rs-home">
                        <div class="row">
                          <div class="col-md-1 col-xs-1">
                            <i class="fab fa-facebook-square blue"></i>
                          </div>
                          <div class="col-md-10 col-xs-10"><span><a href="#" target="_blank">Permata Keluarga <?= strtoupper($rs) ?></a></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script defer="" src="https://cdn.jsdelivr.net/combine/npm/lightgallery,npm/lg-autoplay,npm/lg-fullscreen,npm/lg-hash,npm/lg-pager,npm/lg-share,npm/lg-thumbnail,npm/lg-video,npm/lg-zoom"></script>
<script id="jadwal-dokter-template" type="text/template">
  <div class="col-md-1 col-xs-6">
                                        <div class="hidden-lg hidden-md">
                                            {{HARI}}
                                        </div>
                                        <div class="day-text" id="jam-container-{{ID}}">
                                        </div>
        </div>
</script>

<script id="jam-jadwal-dokter-template" type="text/template">
  <p id='row1'>{{JAM}}</p>
</script>
<script id="info-dokter-template" type="text/template">
  <div class="row" id="doctor-info-{{DOCTORID}}">
    <div class="col-md-3 col-xs-12 center-info-dokter">
        <div class="row">
            <div class="accordion">
                <div class="overlay-info-dokter col-xs-4 col-md-12">
                    <div class="crop-info-dokter">
                        <img class="foto-info-dokter" alt="{{DOCTORNAME}}" src="{{IMAGE}}"/>
                    </div>
                    <div class="spesialis-bar hidden-xs hidden-sm">{{SPNAME}}</div>
                </div>  
                <div class="name-info-dokter col-xs-7 col-sm-4 col-md-12"><b>{{DOCTORNAME}}</b></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-4 col-xs-6">
                <button class="btn btn-border-pink btn-lihat-info-dokter-v2 info-dokter" data-slug="{{SLUG}}" data-id="{{DOCTORID}}" data-name="{{DOCTORNAME}}" data-spesialis="{{SPNAME}}" data-slugspesialis="{{SSPNAME}}" data-cv="{{DOCTORCV}}" data-nama-rs="{{CLINICNAME}}" data-idspesialis="{{SPID}}" data-idclinic="{{CLINICID}}" data-src-photo="{{IMAGE}}" data-toggle="modal" data-target="#info-dokter-modal">
                    <span class="hidden-sm hidden-xs">Lihat</span> Info Dokter
                </button>
            </div>
            <div class="text-right hidden-md hidden-lg col-sm-4 col-xs-6">
                <a href="#" class="btn btn-pink button-buat-janji-detail col-xs-12 pull-right col-md-2" data-idsp="{{SPID}}" data-nama="{{DOCTORNAME}}" >Buat Janji</a>
            </div>
        </div>
    </div>
    <div class="col-md-9 col-xs-12 hidden-sm hidden-xs">
        <div class="row content-detail-rs">
            <div class="col-md-12">
                <div class="row jadwal-klinik-hari hidden-sm hidden-xs">
                    <div class="col-md-1">
                        Senin
                    </div>
                    <div class="col-md-1">
                        Selasa
                    </div>
                    <div class="col-md-1">
                        Rabu
                    </div>
                    <div class="col-md-1">
                        Kamis
                    </div>
                    <div class="col-md-1">
                        Jumat
                    </div>
                    <div class="col-md-1">
                        Sabtu
                    </div>
                    <div class="col-md-1">
                        Minggu
                    </div>
                </div>
                <div class="row jadwal-klinik-row">
                    <div id="jadwal-dokter-popup" class="jadwal-dokter-modal">
                    </div>
                </div>
                <p class="remarks hidden" id="remarks-{{DOCTORID}}">{{REMARKS}}</p>
                <div class="text-right hidden-sm hidden-xs col-md-4 col-sm-4 pull-right">
                    <a href="#" class="btn btn-pink button-buat-janji-detail" data-idsp="{{SPID}}" data-nama="{{DOCTORNAME}}" ></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-bottom hidden">
            <div class="col-md-12 col-sm-4 col-xs-6">
                <button class="btn btn-border-pink btn-lihat-info-dokter-v2 info-dokter" data-id="{{DOCTORID}}" data-name="{{DOCTORNAME}}" data-spesialis="{{SPNAME}}" data-slugspesialis="{{SSPNAME}}" data-cv="{{DOCTORCV}}" data-nama-rs="{{CLINICNAME}}" data-idspesialis="{{SPID}}" data-idclinic="{{CLINICID}}" data-src-photo="{{IMAGE}}" data-toggle="modal" data-target="#info-dokter-modal">
                    <span class="hidden-sm hidden-xs">Lihat</span> Info Dokter
                </button>
            </div>
            <div class="text-right hidden-md hidden-lg col-sm-4 col-xs-6">
                <a href="#" class="btn btn-pink button-buat-janji-detail col-xs-12 pull-right col-md-2" data-idsp="{{SPID}}" data-nama="{{DOCTORNAME}}" >Buat Janji</a>
            </div>
        </div>
</div>
</script>
<script id="jadwal-klinik-template" type="text/template">
  <div class="row list-jk">
    <div class="nama-klinik-jk blue"><b>{{SPNAME}}</b></div>
    <div class="col-md-12">
        <div class="title-jk">Jam Operasional</div>
            <div class="row jadwal-klinik-hari hidden-xs hidden-sm">
                <div class="col-md-1">Senin</div>
                <div class="col-md-1">Selasa</div>
                <div class="col-md-1">Rabu</div>
                <div class="col-md-1">Kamis</div>
                <div class="col-md-1">Jumat</div>
                <div class="col-md-1">Sabtu</div>
                <div class="col-md-1">Minggu</div>
            </div>
            <div class="row jadwal-klinik-row" id="jadwal-klinik-template-content-{{IDSCH}}">
            </div>
    </div>
    <div class="pull-right btn btn-pink btn-lihat-info-dokter-v2" onclick="location.href='{{URL}}'">Buat Janji</div>
</div>
</script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="text/javascript">
  // load page 
  function hide_table(v) {
    // var vc = this.getAttribute('data-spesial');
    swal.fire(
      'info', 'Dokter yang di pilih', 'success'
    );

    <?php
    foreach ($bag as $key => $v) {
    ?>
      $(".bag_<?= $v->id_bagian ?>").hide();
    <?php
    }
    ?>
    if (v == '.') {
      <?php
      foreach ($bag as $key => $v) {
      ?>
        $(".bag_<?= $v->id_bagian ?>").show();
      <?php
      }
      ?>
    } else {
      $("" + v + "").show();
    }
  }
  // 
  // end function
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

</div>