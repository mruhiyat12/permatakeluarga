<div class="site-about">
    <div class="site-bg lazy" style="display: block; background-image: url(<?= base_url('assets/img/background1.jpg') ?>);">
        <div class="row judul-back">
            <h1 class="title-lokasi-home col-md-12 col-xs-12">Layanan Gawat Darurat</h1>
        </div>
        <div class="card card-site card-gd">
            <div class="row row-form-gd">
                <div class="col-md-12">
                    <h2 class="blue header-igd">No Telepon Darurat</h2>
                    <div class="row">

                        <?php foreach ($data->result_array() as $datas) {  ?>
                            <div class="col-md-4">
                                <div class="call-gd">
                                    <h4><b><?= ucfirst($datas['nama_rumahsakit']) ?></b></h4>
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2 phone-icon-gd"></div>
                                        <div class="col-md-10 col-xs-10"><a href="tel:+<?= ucfirst($datas['value_set']) ?>">
                                                <h4><?= ucfirst($datas['value_set']) ?></h4>
                                            </a></div>
                                    </div>
                                </div>

                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>