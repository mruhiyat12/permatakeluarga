 <footer>
   <div class="footer">
     <div class="container">
       <div class="social-icon">
         <div class="col-md-4">
           <ul class="social-network">
             <?php
              $id_rumahsakit = ($this->uri->segment(2)) ?  $this->uri->segment(2) : 'lippo';
              switch ($id_rumahsakit) {
                case 'lippo':
                  $alamat = '15';
                  $tlp = '18';
                  $igd = '11';
                  $fax = '12';
                  $wa  = '46';
                  $fb  = '29';
                  $ig  = '47';
                  $ytb = '48';
                  $tiktok = '49';
                  $email = '26';
                  $fanspage = '41';
                  $id_resultrs = '1';
                  break;
                case 'jababeka':
                  $alamat = '16';
                  $tlp = '24';
                  $igd = '1';
                  $fb  = '30';
                  $wa  = '51';
                  $ig  = '44';
                  $ytb = '39';
                  $tiktok = '50';
                  $fax = '2';
                  $email = '27';
                  $fanspage = '42';
                  $id_resultrs = '2';
                  break;
                case 'galuhmas':
                  $alamat = '52';
                  $tlp = '53';
                  $igd = '55';
                  $fax = '54';
                  $wa  = '56';
                  $email = '57';
                  $fb  = '58';
                  $ig  = '59';
                  $ytb = '62';
                  $tiktok = '59';
                  $fanspage = '43';
                  $id_resultrs = '4';
                  break;
              }
              // var_dump($id_rumahsakit);
              // exit;


              $alamats = $this->frontmodel->getSingleSettingKontak($alamat, $id_resultrs);
              $tlps = $this->frontmodel->getSingleSettingKontak($tlp, $id_resultrs);
              $igds = $this->frontmodel->getSingleSettingKontak($igd, $id_resultrs);
              $faxs = $this->frontmodel->getSingleSettingKontak($fax, $id_resultrs);
              $was = $this->frontmodel->getSingleSettingKontak($wa, $id_resultrs);
              $fbs = $this->frontmodel->getSingleSettingKontak($fb, $id_resultrs);
              $igs = $this->frontmodel->getSingleSettingKontak($ig, $id_resultrs);
              $tiks = $this->frontmodel->getSingleSettingKontak($tiktok, $id_resultrs);
              $ytbs = $this->frontmodel->getSingleSettingKontak($ytb, $id_resultrs);
              $emails = $this->frontmodel->getSingleSettingKontak($email, $id_resultrs);
              $fanspages = $this->frontmodel->getSingleSettingKontak($fanspage, $id_resultrs);
              // echo $this->db->last_query();
              if (isset($alamats)) {
                echo "<li><h5><i class='fa fa-home'></i>&nbsp; " . $alamats[0]->value_set . " </h5></li>";
              }


              // if (isset($fanspages)) {
              //     echo "<li><h5>&nbsp;<a href='" . $fanspages[0]->value_set . "'> " . $fanspages[0]->value_set . " </a></h5></li>";
              // }
              ?>

           </ul>

           <!--<meta itemprop="streetAddress" content="<?= $alamats[0]->value_set ?>-->
           <meta itemprop="addressLocality" content="Kelapa Gading" />
           <meta itemprop="addressRegion" content="Jakarta" />
           <meta itemprop="addressCountry" content="Indonesia" />
           <meta itemprop="postalCode" content="14240" />
           </p>
         </div>
         <div class="col-lg-2 col-md-6 footer-links">
           <h4>Rumah Sakit Kami</h4>
           <ul>
             <li><i class="bx bx-chevron-right"></i> <a href="#">RS Permata Keluarga Lippo Cikarang</a></li>
             <li><i class="bx bx-chevron-right"></i> <a href="#">RS Permata Keluarga Jababeka</a></li>
             <li><i class="bx bx-chevron-right"></i> <a href="#">RS Permata Keluarga galuhmas</a></li>
           </ul>
         </div>
         <div class="col-lg-3 col-md-6">
           <h4 class="title-footer-top">
             Telepon
           </h4>
           <p class="text-footer-top">
           <ul class="nav">
             <?php
              if (isset($tlps)) {
                echo "<li><h5><i class='fa fa-phone'></i>&nbsp; " . $tlps[0]->value_set . " </h5></li>";
              }
              if (isset($igds)) {
                echo "<li><h5><i class='fa fa-phone'></i>&nbsp; " . $igds[0]->value_set . " </h5></li>";
              }
              if (isset($faxs)) {
                echo "<li><h5><i class='fa fa-phone'></i>&nbsp; " . $faxs[0]->value_set . " </h5></li>";
              }
              if (isset($was)) {
                echo "<li><h5><i class='fa fa-phone'></i>&nbsp; " . $was[0]->value_set . " </h5></li>";
              }

              ?>
           </ul>
           </p>
           <div class="hidden" itemprop="geo" itemscope itemtype="https://schema.org/GeoCoordinates">
             <meta itemprop="latitude" content="-6.151829" />
             <meta itemprop="longitude" content="106.897097" />
           </div>
         </div>
         <div class="col-lg-4 col-md-6">
           <h4 class="title-footer-top">
             Media Sosial
           </h4>
           <ul class="nav"> <?php
                            if (isset($emails)) {
                              $rsnya = ($emails[0]->value_set) ? $emails[0]->value_set : 'Kosong';
                              echo "<li><h5><i class='fa fa-envelope'></i>&nbsp; <a href='mailto:" . $rsnya . "'>" . $rsnya . " </a></h5></li>";
                            }
                            if (isset($fbs)) {
                              $rsnya = ($fbs[0]->value_set) ? $fbs[0]->value_set : 'Kosong';
                              echo "<li><h5><i class='fab fa-facebook-square'></i>&nbsp; <a href='mailto:" . $rsnya . "'>" . $rsnya . " </a></h5></li>";
                            }
                            if (isset($igs)) {
                              $rsnya = ($igs[0]->value_set) ? $igs[0]->value_set : 'Kosong';
                              echo "<li><h5><i class='fab fa-instagram'></i>&nbsp; <a href='mailto:" . $rsnya . "'>" . $rsnya . " </a></h5></li>";
                            }
                            if (isset($ytbs)) {
                              $rsnya = ($ytbs[0]->value_set) ? $ytbs[0]->value_set : 'Kosong';
                              echo "<li><h5><i class='fab fa-youtube'></i>&nbsp; <a href='mailto:" . $rsnya . "'>" . $rsnya . " </a></h5></li>";
                            }
                            if (isset($tiks)) {
                              $rsnya = ($tiks[0]->value_set) ? $tiks[0]->value_set : 'Kosong';
                              echo "<li><h5><i class='fab fa-tiktok'></i>&nbsp; <a href='mailto:" . $rsnya . "'>" . $rsnya . " </a></h5></li>";
                            }
                            ?>
           </ul>

         </div>
       </div>
     </div>
     <a href="<?= get_whasappnumber($this->uri->segment(1)) ?>" class="float" target="_blank">
       <span class="tip-content">Daftar Pasien</span>
     </a>
     <div class="container footer-bottom">
       <p class="pull-left" itemprop="name" content="Permata Keluarga">&copy; PT PERMATA PRIMA HUSADA All Rights Reserved </p>

       <div class="pull-right">
         <a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a>
       </div>
     </div>
   </div>
   </div>
   </div>
   </div>
 </footer>

 ?>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 <!-- Include all compiled plugins (below), or include individual files as needed -->
 <script src="<?php echo base_url(); ?>assets/ui/js/rcs_front/bootstrap.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/ui/js/rcs_front/jquery.prettyPhoto.js"></script>
 <script src="<?php echo base_url(); ?>assets/ui/js/rcs_front/jquery.isotope.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/ui/js/rcs_front/wow.min.js"></script>
 <!-- jQuery 2.1.4 -->
 <script src="<?php echo base_url(); ?>assets/ui/js/rcs_front/functions.js"></script>
 <!--Jquery Datatable-->
 <script src="<?php echo base_url(); ?>assets/cms/js/plugins/datatables/jquery.dataTables.js"></script>
 <script>
   function va(v) {
     window.open(base_url + v, "_self");
   }

   function vax(v, x) {
     if (x == '/') {
       x = '/home';
     }
     window.open(base_url + v + x, "_self");
   }

   function openDaftarModal(id) {
     console.log(id)
     $('#modal-daftar').modal('show');
     $('#modal-daftar').appendTo("body")
     //  $("#modal-daftar").modal('show')
     //  $('.modal-backdrop').remove();
   }

   function sendMessage(id) {
     var text = {
       //text: "Saya ingin daftar di Rumah sakit " + id
     }

     var param = $.param(text);
     window.open('https://wa.me/6285604081560?text=PASIEN%20BARU%0ANama%20Lengkap%20%3A%0ATempat%2FTanggal%20lahir%20%3A%0Ajenis%20Kelamin%20%3A%20laki-laki%2Fperempuan%0AStatus%20%3A%0ADokter%20yang%20dituju%20%3A%0AWaktu%20berobat%20%3A%20(pagi%2Fsore)%0AAlamat%20Domisili%20%3A%0APenjamin%20%3A%20Tunai%2FAsuransi%2FPerusahaan%2FBpjs%20Kesehatan%0ANo%20telepon%3A%0A%0APASIEN%20LAMA%0ANama%20Lengkap%20%3A%0ATempat%2FTanggal%20lahir%20%3A%0ADokter%20yang%20dituju%20%3A%0AWaktu%20berobat%20%3A%20(pagi%2Fsore)%0AAlamat%20Domisili%20%3A%0APenjamin%20%3A%20Tunai%2FAsuransi%2FPerusahaan%2FBpjs%20Kesehatan%0ANo%20telepon%3A%0A%0A' + param);
   }
 </script>
 </body>

 </html>