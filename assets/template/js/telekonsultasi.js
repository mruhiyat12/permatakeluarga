     var url_post = '';

     function replaceText(t, u, v) {
         return t.split(u).join(v)
     }

     function ChangeUrl(t, u, v) {
         "undefined" != typeof history.pushState ? (t = {
             Page: t,
             Title: u,
             Url: v
         }, history.pushState(t, t.Title, t.Url)) : console.log("Browser does not support HTML5. Can not change URL State")
     }
     $(document).ready(function () {
         function t(a) {
             if (a.files && a.files[0]) {
                 var b = new FileReader;
                 b.onload = function (a) {
                     $("#photo-profil-nextr").attr("src", a.target.result)
                 };
                 b.readAsDataURL(a.files[0])
             }
         }

         function u(a, b, c) {
             $.ajax({
                 type: "GET",
                 url: domain + "/api/" + a,
                 data: b,
                 success: function (a) {
                     c(a)
                 },
                 contentType: "application/json"
             })
         }

         function v(a) {
             $("#news-content").html("");
             if (void 0 != a)
                 for (var b = $("#news-content-template").html(), c = 0; c < a.length; c++) {
                     var e = domain + "/detail/" + a[c].slug,
                         l = new Date(a[c].date_posted),
                         d =
                         replaceText(b, "{{ID}}", a[c].id);
                     d = replaceText(d, "SLUG", e);
                     d = replaceText(d, "{{IMAGE}}", a[c].image_full);
                     d = replaceText(d, "{{TITLE}}", a[c].title);
                     d = replaceText(d, "{{DATE}}", l.toLocaleString("id-ID", {
                         year: "numeric",
                         day: "numeric",
                         month: "long"
                     }));
                     d = replaceText(d, "{{EXCERPT}}", a[c].excerpt.replace(/^(.{134}).+/, "$1&hellip;"));
                     $("#news-content").append(d)
                 }
         }
         $(".lazy").Lazy({
             effect: "fadeIn",
             asyncLoader: function (a, b) {
                 setTimeout(function () {
                     a.html('element handled by "asyncLoader"');
                     b(!0)
                 }, 1E3)
             }
         });
         window.addEventListener("popstate",
             function (a) {
                 a = window.location.pathname;
                 0 <= a.toLowerCase().indexOf("promo") || 0 <= a.toLowerCase().indexOf("news") || 0 <= a.toLowerCase().indexOf("artikel") ? ($(".load-more").hide(), $(".loading-klinik").toggleClass("hidden"), $(".news-part").toggleClass("hidden"), setTimeout(function () {
                     window.location.replace(window.location.href)
                 }, 500)) : 0 <= a.toLowerCase().indexOf("search-result") ? ($("#search-result-content").hide(), $(".loading-klinik").toggleClass("hidden"), setTimeout(function () {
                         window.location.replace(window.location.href)
                     },
                     500)) : 0 <= a.toLowerCase().indexOf("investor-relation") ? ($(".content-about-us>.loading-klinik").toggleClass("hidden"), setTimeout(function () {
                     window.location.replace(window.location.href)
                 }, 500)) : $(35) ? ($(".card-bottom-klinik>.loading-klinik").toggleClass("hidden"), $(".klinik-content").hide(), setTimeout(function () {
                     window.location.assign(window.location.href)
                 }, 500)) : window.location.replace(window.location.href)
             });
         767 < $(window).width() && $("#milestone-about .item").each(function () {
             var a = $(this).next();
             a.length ?
                 (a.children(":first-child").clone().appendTo($(this)), 0 < a.next().length ? a.next().children(":first-child").clone().appendTo($(this)) : $(this).siblings(":first").children(":first-child").clone().empty().appendTo($(this))) : (a = $(this).siblings(":first"), a.children(":first-child").clone().empty().appendTo($(this)), $(this).siblings(":first").children(":first-child").clone().empty().appendTo($(this)));
             $(".lazy").Lazy({
                 effect: "fadeIn"
             })
         });
         if (768 > $(window).width()) {
             if (0 < $("#lightgallery").length) {
                 var m = $("#lightgallery .image-gallery").data("imgresponsive");
                 $("#lightgallery .top-image-klinik").css("background-image", "url(" + m + ")")
             }
             0 < $(".rumah-sakit-home").length && (m = $(".rumah-sakit-home .active .home-image-klinik").data("imgresponsive"), $(".rumah-sakit-home .active .home-image-klinik").css("background-image", "url(" + m + ")"));
             $("#res-klinik-home").addClass("dropdown-menu")
         }
         $(window).resize(function () {
             if (767 == $("header").width()) $("#res-klinik-home li").click(function (a) {
                 $("#res-klinik-home").hide()
             });
             else if (767 < $(window).width()) $("#res-klinik-home").length &&
                 $("#res-klinik-home").show(), 0 < $("#pills-klinik-tab").length && $("#pills-klinik-tab li").css("left", "0%"), $("#info-dokter-content .row").first().find(".col-md-3 .row:last-child").removeClass("hidden");
             else if (767 > $(window).width()) {
                 if ($(".rumah-sakit-home").length) {
                     var a = $(".rumah-sakit-home .active .home-image-klinik").data("imgresponsive");
                     $(".rumah-sakit-home .active .home-image-klinik").css("background-image", "url(" + a + ")")
                 }
                 $("#pills-klinik-tab").fadeIn(500)
             }
             a = $(this);
             767 < a.width() ? ($("#res-klinik-home").show(),
                 0 < $(".center-btn-res").length && $("div > .btn-border-pink").unwrap()) : $(".btn-border-pink").wrap('<div class="center-btn-res"></div>');
             a = $(this);
             767 < a.width() ? $("#milestone-about .item").each(function () {
                 var a = $(this).next();
                 a.length ? (a.children(":first-child").clone().addClass("hidden").appendTo($(this)), 0 < a.next().length ? a.next().children(":first-child").clone().addClass("hidden").appendTo($(this)) : $(this).siblings(":first").children(":first-child").clone().empty().addClass("hidden").appendTo($(this))) :
                     (a = $(this).siblings(":first"), a.children(":first-child").clone().empty().addClass("hidden").appendTo($(this)), $(this).siblings(":first").children(":first-child").clone().empty().addClass("hidden").appendTo($(this)));
                 $(".lazy").Lazy({
                     effect: "fadeIn"
                 })
             }) : (0 < $("#lightgallery").length && (a = $("#lightgallery .image-gallery").data("imgresponsive"), $("#lightgallery .top-image-klinik").css("background-image", "url(" + a + ")")), 0 < $(".rumah-sakit-home").length && (a = $(".rumah-sakit-home .active .home-image-klinik").data("imgresponsive"),
                 $(".rumah-sakit-home .active .home-image-klinik").css("background-image", "url(" + a + ")")))
         });
         0 < $("#toggle-about").length && ($("#menu-about-us li.active").hasClass("open") ? $("#current-about").text($("#menu-about-us li.active .open").text()) : $("#current-about").text($("#menu-about-us li.active").text()));
         $("#menu-about-us .nav-item").click(function (a) {
             0 != $("#menu-about-us li.active .open").length ? (a = $("#menu-about-us li.active .open").text(), $("#current-about").text(a)) : (a = $(this).text(), $("#current-about").text(a));
             $("#toggle-about").click()
         });
         $("#menu-about-us a").on("click", function (a) {
             $(".nav-item").hasClass("open") && a.stopPropagation();
             a.preventDefault()
         });
         0 < $("#boarddetail-modal").length && $(".button-detail").click(function (a) {
             $("#boarddetail-modal .nama-testimoni-detail").text($(this).data("nama-lengkap"));
             $("#boarddetail-modal .role").text($(this).data("role"));
             $("#boarddetail-modal .foto-modal-board").attr("src", $(this).data("photo"));

             $("#boarddetail-modal .text-testimoni").html(b)
         });
         0 < $("#toggle-infors").length && $("#current-infors").text($("#menu-info-rs li.active").text());
         0 < $("#pills-klinik-tab").length && 767 >= $(window).width() && ($("#pills-klinik-tab").fadeOut(), $(window).load(function () {
             $("#pills-klinik-tab").fadeIn(500)
         }));
         0 < $(".calendar-jui").length && ($(".calendar-jui input").val((new Date($(".hidden-jui").text())).toLocaleString("id-ID", {
                 day: "2-digit",
                 month: "long",
                 year: "numeric"
             })), $("#tanggal-jadwal-sr").length ? $("#tanggal-jadwal-sr").val($(".hidden-jui").text()) : $("#jadwalform-tanggal").val($(".hidden-jui").text()),
             $(".calendar-jui .input-group-addon").click(function (a) {
                 a.preventDefault();
                 $(".calendar-jui #jadwalform-tanggal-disp-kvdate input").datepicker("show")
             }));
         $(".overlay-calendar-search").length && ($(".overlay-calendar-search input").val((new Date($("#search-tanggal-jadwal").val())).toLocaleString("id-ID", {
             day: "2-digit",
             month: "long",
             year: "numeric"
         })), $(".overlay-calendar-search .input-group-addon").click(function (a) {
             a.preventDefault();
             $(".overlay-calendar-search #jadwalform-tanggal-disp-kvdate input").datepicker("show")
         }));
         if (0 < $("#form-buat-janji").length) {
             var A = url_post;
             $("#jenis-janji li a").click(function (a) {
                 $("#buatjanjimodal #input-category").val($(this).data("id"))
             });
             $(".btn-kembali").click(function (a) {
                 $('#buatjanjimodal .buat-janji-body').show();
                 $('#buatjanjimodal .buat-janji-disclaimer').hide();
             });


         }
         m = new Date($("#datetimepicker-sr").val());
         m.setSeconds(m.getSeconds() + 10);
         $("#datetimepicker-sr").datetimepicker({
             locale: "id",
             format: "DD MMMM YYYY",
             widgetPositioning: {
                 vertical: "bottom"
             },
             useCurrent: !1,
             defaultDate: m
         }).on("dp.change", function () {
             $("#datetimepicker-sr").closest(".row").find(".obsolete-error").addClass("hide")
         });
         0 < $("#datetimepicker-sr").length && (m = new Date, m.setHours(0, 0, 0, 0), $("#datetimepicker-sr").data("DateTimePicker").minDate(m),
             m = new Date($("#datetimepicker-sr").val()), $("#datetimepicker-sr").data("DateTimePicker").date(m));
         $("#cabang-rumah-sakit-sr li a").click(function (a) {
             $("#btn-search-result").data("idclinic", $(this).data("id"))
         });
         $("#spesialisasi-sr li a").click(function (a) {
             $("#btn-search-result").data("idspesialis", $(this).data("id"))
         });
         $("#btn-search-result").click(function (a) {
             a = $(this).data("idclinic");
             var b = $(this).data("idspesialis"),
                 c = new Date($("#datetimepicker-sr").val()),
                 e;
             0 == c.getDay() && (e = "sun");
             1 == c.getDay() &&
                 (e = "mon");
             2 == c.getDay() && (e = "tue");
             3 == c.getDay() && (e = "wed");
             4 == c.getDay() && (e = "thu");
             5 == c.getDay() && (e = "fri");
             6 == c.getDay() && (e = "sat");
             var l = $("#nama-dokter-janji").val();
             $("#nama-dokter-janji-res").val();
             c = c.getDate() + "-" + c.getMonth() + 1 + "-" + c.getFullYear();
             window.location = l ? window.location.origin + window.location.pathname + "?clinic=" + a + "&specialization=" + b + "&day=" + e + "&date=" + c + "&doctorname=" + l : window.location.origin + window.location.pathname + "?clinic=" + a + "&specialization=" + b + "&day=" + e + "&date=" + c
         });
         0 < $("#nav-buat-janji-content").length && ($("#cabang-rs-select li a").click(function (a) {
             $("#btn-buat-janji-sr").data("idclinic", $(this).data("id"))
         }), $("#spesialisasi-select li a").click(function (a) {
             $("#btn-buat-janji-sr").data("idspesialis", $(this).data("id"))
         }), $("#btn-buat-janji-sr").click(function (a) {
             a = $(this).data("idclinic");
             var b = $(this).data("idspesialis"),
                 c = new Date($("#datetimepicker1").val()),
                 e;
             0 == c.getDay() && (e = "sun");
             1 == c.getDay() && (e = "mon");
             2 == c.getDay() && (e = "tue");
             3 == c.getDay() && (e = "wed");
             4 == c.getDay() && (e = "thu");
             5 == c.getDay() && (e = "fri");
             6 == c.getDay() && (e = "sat");
             var l = $("#nama-janji").val();
             c = c.getDate() + "-" + c.getMonth() + 1 + "-" + c.getFullYear();
             window.location = l ? window.location.origin + window.location.pathname + "/search-result?clinic=" + a + "&specialization=" + b + "&day=" + e + "&date=" + c + "&doctorname=" + l : window.location.origin + window.location.pathname + "/search-result?clinic=" + a + "&specialization=" + b + "&day=" + e + "&date=" + c
         }));
         $(document).off("submit").on("submit", "#jadwal-form-md", function (a) {
             a.preventDefault();
             $("#search-result-content").hide();
             $(".loading-klinik").removeClass("hidden");
             var mika = $("select[name=mika_json]").val();
             var poli = $("select[name=poli_json]").val();
             var tgl_konsul = $("input[name=tgl_konsul_json]").val();
             var dokter = $("input[name=dokter_json]").val();
             /*alert(mika+" | "+poli+" | "+tgl_konsul+" | "+dokter);*/
             $.ajax({
                 type: "GET",
                 url: base_url() + "/getdata/apidokter",
                 data: $(this).serialize(),
                 contentType: "application/json",
                 success: function (a) {
                     $("#search-result-content").html(a);
                     setTimeout(function () {
                        //  ChangeUrl("searchresult", "title", base_url() + "/listdokter"); //UAT
                         $(".loading-klinik").addClass("hidden");
                         $("#search-result-content").show()
                     }, 1E3)
                 }
             })
             /* a.preventDefault();
        $("#search-result-content").hide();
        $(".loading-klinik").removeClass("hidden");
		$(this).serializeArray();
        var b = {};
        $.each($(this).serializeArray(), function() {
            b[this.name.replace(/[\[\]']+/g, "")] = this.value
        });
        a = b.JadwalFormtanggal;
        var c = new Date(a);
        $.ajax({
            type: "GET",
            url: domain + "/api/search-doctor",
            data: "search[clinic]=" + b.JadwalFormid_clinic + "&search[specialization]=" + b.JadwalFormid_specialization + "&search[date]=" + a + "&search[doctorName]=" + b.JadwalFormnama_dokter,
            contentType: "application/json",
            success: function(a) {
                $("#search-result-content").html("");
                var b = $("#search-results-template").html(),
                    d = $("#jadwal-searchresult-template").html();
                if (200 == a.status && 0 < a.data.doctors.length)
                    for (var e = 0; e < a.data.doctors.length; e++) {
                        var k = a.data.doctors[e];
                        $("#buatjanjimodal #input-idclinic").val(k.clinic);
                        var h = replaceText(b, "{{DOCTORID}}", k.doctor.id);
                        h = replaceText(h, "{{DOCTORNAME}}", k.doctor.name);
                        h = replaceText(h, "{{DOCTORIDSP}}", k.doctor.specialization.id);
                        h = replaceText(h, "{{DOCTORSP}}", k.doctor.specialization.name);
                        h = replaceText(h, "{{DOCTORCV}}", k.doctor.description);
                        h = replaceText(h, "{{DOCTORPHOTO}}", k.doctor.photo_full);
                        h = replaceText(h, "{{CLINICID}}", k.clinic);
                        h = replaceText(h, "{{CLINICNAME}}", $("#select2-jadwalform-id_clinic-container").text());
                        h = replaceText(h, "{{MONTH}}", c.toLocaleString(p, {
                            month: "long"
                        }));
                        h = replaceText(h, "{{YEAR}}", c.toLocaleString(p, {
                            year: "numeric"
                        }));
                        null != k.schedule.remarks && (h = replaceText(h, "{{REMARKS}}", k.schedule.remarks));
                        $("#search-result-content").append(h);
                        null != k.schedule.remarks && $("#remarks-" + k.doctor.id).removeClass("hidden");
                        for (h = 0; 7 > h; h++) {
                            var f = new Date(a.data.doctors[e].schedule.schedule[h].date),
                                p = "id-ID",
                                n = replaceText(d, "{{DOCTORID}}", k.doctor.id);
                            n = replaceText(n, "{{DATE}}", f.toLocaleString(p, {
                                day: "2-digit"
                            }));
                            n = replaceText(n, "{{DAYTEXT}}", f.toLocaleString(p, {
                                weekday: "long"
                            }));
                            n = replaceText(n, "{{ID}}", h);
                            $("#jadwal-buat-janji-" + k.doctor.id).find(".row").append(n);
                            0 == h && $("#jadwal-buat-janji-" + k.doctor.id).find(".col-md-1").addClass("highlighted-day");
                            n = $("#button-jam-searchresult-template").html();
                            n = replaceText(n,
                                "{{FULLDATE}}", f.getMonth() + 1 + "/" + f.toLocaleString(p, {
                                    day: "2-digit"
                                }) + "/" + f.getFullYear());
                            n = replaceText(n, "{{DOCTORID}}", k.doctor.id);
                            n = replaceText(n, "{{DOCTORNAME}}", k.doctor.name);
                            n = replaceText(n, "{{DOCTORPHOTO}}", k.doctor.photo_full);
                            n = replaceText(n, "{{DOCTORSP}}", k.doctor.specialization.name);
                            if (-1 < a.data.doctors[e].schedule.schedule[h].time.indexOf("--")) n = replaceText(n, "{{TIME}}", "---"), $("#day-text-" + k.doctor.id + "-" + f.toLocaleString(p, {
                                day: "2-digit"
                            }) + "-" + h).append(n), $("#day-text-" + k.doctor.id +
                                "-" + f.toLocaleString(p, {
                                    day: "2-digit"
                                }) + "-" + h).find("button").attr("disabled", !0);
                            else
                                for (var z = a.data.doctors[e].schedule.schedule[h].time.split(","), m = 0; m < z.length; m++) {
                                    var g = n;
                                    g = replaceText(g, "{{TIME}}", z[m]);
                                    g = replaceText(g, "{{DAY}}", (new Date(a.data.doctors[e].schedule.schedule[h].date)).getDay());
                                    $("#day-text-" + k.doctor.id + "-" + f.toLocaleString(p, {
                                        day: "2-digit"
                                    }) + "-" + h).append(g)
                                }
                        }
                    }
                setTimeout(function() {
                    ChangeUrl("searchresult", "title", domain + "/buat-janji/search-result?" + $("#jadwal-form-md").serialize());
                    $(".loading-klinik").addClass("hidden");
                    $("#search-result-content").show()
                }, 1E3)
            }
        })*/
         });
         0 <= window.location.pathname.toLowerCase().indexOf("promo") && ($(".loading-klinik").addClass("hidden"), $(".news-part").removeClass("hidden"));
         var w = 0;
         m = $("#startchange");
         var g = $(".in-change"),
             B = g.offset();
         m.offset();
         g.length && $(document).scroll(function () {
             w = $(this).scrollTop();
             w > B.top - 545 ? $(".share-in").hide() : $(".share-in").show()
         });
         0 < $("#toggle-career").length && $("#current-career").text($("#careerTab .active a").text());
         $("#careerTab .nav-item").click(function (a) {
             $("#current-career").text($("#careerTab .active a").text());
             $("#toggle-career").click()
         });
         $(".button-cv-career").click(function () {
             $("#upload-cv").trigger("click")
         });
         0 < $("#toggle-investor").length && ($("#menu-investor-news li.active").has(".open").length ? $("#current-investor").text($("#menu-investor-news li.active .open").text()) : $("#current-investor").text($("#menu-investor-news li.active").text()));
         $(".shareholder-composition").length && $(".shareholder-composition table").each(function () {
             $(this).addClass("col-md-12");
             $(this).wrap("<div class='table-responsive'></div>")
         });
         0 < $(".content-fh").length && $(".content-fh").find("table").each(function () {
             $(this).addClass("table");
             $(this).wrap('<div class="table-responsive sc-table"></div>')
         });
         $(document).on("click", ".shareholder-tab", function (a) {
             var b = $(this).data("target").replace("-content", "-detail"),
                 c = $(this).data("id-category");
             $.ajax({
                 type: "GET",
                 url: domain_investor_shareholder,
                 dataType: "json",
                 data: "search[category]=" + c + "&lang=" + lang,
                 success: function (a) {
                     if (200 == a.status) {
                         a =
                             a.data.investor_shareholder;
                         for (var e = 0; e < a.length; e++) a[e].id == c && $(b).html(a[e].shareholder_composition)
                     }
                 }
             });
             $(b).find("table").each(function () {
                 $(this).wrap("<div class='table-responsive'></div>")
             })
         });
         0 < $(".panel-hs").length && $(".panel-hs").each(function (a, b) {
             if ("0" != $(this).find(".accordion-paket").data("accordion")) {
                 var c = $(this).find(".accordion-paket").attr("id");
                 $(this).find("h4").each(function (a, b) {
                     $(this).attr("data-toggle", "collapse");
                     $(this).attr("data-parent", "#" + c);
                     $(this).addClass("collapsed");
                     $(this).addClass("panel-title-sc");
                     $(this).addClass("panel-title");
                     var d = $(this).text().toLowerCase().replace(/&nbsp;/g, "").replace(/[_\W]+/g, "").replace(/[_\s]/g, "-") + a;
                     "" === $.trim($(this).siblings("p").text()) && ($(this).siblings("p").css("margin-bottom", "0"), $(this).siblings("p").css("line-height", "0"));
                     $(this).nextUntil("h4").andSelf().wrapAll('<div class="panel panel-default panel-default-sc"></div>');
                     0 === a ? ($(this).removeClass("collapsed"), $(this).nextUntil("h4").wrapAll("<div id='paket-" + d +
                         "' class='panel-collapse collapse in'><div class='panel-body panel-body-sc'></div></div>")) : $(this).nextUntil("h4").wrapAll("<div id='paket-" + d + "' class='panel-collapse collapse'><div class='panel-body panel-body-sc'></div></div>");
                     $(this).wrap("<div class='panel-heading panel-heading-sc'></div>");
                     $(this).attr("data-target", "#paket-" + d)
                 })
             }
         });
         0 < $("#accordion-corporate-information").length && ($("#accordion-corporate-information").children("h4").each(function (a, b) {
             $(this).attr("data-toggle", "collapse");
             $(this).attr("data-parent", "#accordion-corporate-information");
             var c = "ci-" + a;
             $(this).nextUntil("h4").andSelf().wrapAll('<div class="panel panel-default panel-default-ci"></div>');
             0 === a ? $(this).nextUntil("h4").wrapAll('<div id="' + c + '" class="panel-collapse collapse in"></div>') : ($(this).nextUntil("h4").wrapAll('<div id="' + c + '" class="panel-collapse collapse"></div>'), $(this).addClass("collapsed"));
             $(this).wrap('<div class="panel-heading panel-heading-ci"></div>');
             $(this).attr("href", "#" + c);
             $(this).addClass("panel-title");
             $(this).addClass("panel-title-ci")
         }), $("#accordion-corporate-information").find("table").each(function () {
             $(this).addClass("table");
             $(this).wrap('<div class="table-responsive fh-table"></div>')
         }));
         0 < $("#accordion-shareholder-composition").length && ($("#accordion-shareholder-composition").children("h4").each(function (a, b) {
             $(this).attr("data-toggle", "collapse");
             $(this).attr("data-parent", "#accordion-shareholder-composition");
             var c = "sc-" + a;
             $(this).nextUntil("h4").andSelf().wrapAll('<div class="panel panel-default panel-default-sc"></div>');
             0 === a ? $(this).nextUntil("h4").wrapAll('<div id="' + c + '" class="panel-collapse collapse in"><div class="panel-body panel-body-sc"></div></div>') : $(this).nextUntil("h4").wrapAll('<div id="' + c + '" class="panel-collapse collapse"><div class="panel-body panel-body-sc"></div></div>');
             $(this).wrap('<div class="panel-heading panel-heading-sc"></div>');
             $(this).attr("href", "#" + c);
             $(this).addClass("panel-title");
             $(this).addClass("panel-title-sc")
         }), $("#accordion-shareholder-composition").find("table").each(function () {
             $(this).addClass("table");
             $(this).wrap('<div class="table-responsive sc-table"></div>')
         }));
         $("#search-fh li a").click(function (a) {
             $(this).attr("id");
             var b = $(this).attr("href");
             $(".content-fh div").removeClass("show");
             $(b).hasClass("show") || $(b).addClass("show");
             a.preventDefault();
             $(".content-fh").hide();
             $("#financial-highlights-content .loading-klinik").removeClass("hidden");
             history.pushState({}, "", b);
             setTimeout(function () {
                     $(".content-fh").fadeIn(200);
                     $("#financial-highlights-content .loading-klinik").addClass("hidden")
                 },
                 500)
         });
         if (0 < $(".shareholder-tab").length) {
             g = $(".shareholder-tab").first();
             var C = g.data("target").replace("-content", "-detail");
             g = g.data("id-category");
             g = "search[category]=" + g + "&lang=" + lang;
             $.ajax({
                 type: "GET",
                 url: domain_investor_shareholder,
                 dataType: "json",
                 data: g,
                 success: function (a) {
                     if (200 == a.status) {
                         a = a.data.investor_shareholder;
                         for (var b = $(".shareholder-tab").first().attr("data-id-category"), c = 0; c < a.length; c++) a[c].id == b && $(C).html(a[c].shareholder_composition)
                     }
                 }
             })
         }
         if (0 < $(".report-presentation-tab").length) {
             g =
                 $(".report-presentation-tab").eq(0);
             m = g.data("target");
             var x = m.replace("-content", "-detail");
             g = g.data("id-category");
             g = "search[category]=" + g + "&lang=" + lang;
             $(m).find(".year-rp").length && (g += "&search[year]=" + $(m).find(".year-rp").find("button").text());
             $.ajax({
                 type: "GET",
                 url: domain_investor_report,
                 dataType: "json",
                 data: g,
                 success: function (a) {
                     if (200 == a.status && (a = a.data.investor_reports, void 0 != a)) {
                         $(x).html("");
                         for (var b = $("#reports-presentations-template").html(), c = 0; c < a.length; c++) {
                             var e = replaceText(b,
                                 "{{TITLE}}", a[c].title);
                             e = replaceText(e, "{{URL}}", a[c].pdf_url);
                             e = replaceText(e, "{{ID}}", a[c].id);
                             null != a[c].cover_thumb ? (e = replaceText(e, "{{IMG}}", a[c].cover_thumb), e = replaceText(e, "hidden", "")) : e = replaceText(e, "{{IMG}}", "");
                             "en" == lang && (e = replaceText(e, "Unduh", "Download"));
                             $(x).append(e)
                         }
                     }
                 }
             })
         }
         $(document).on("click", ".report-presentation-tab", function (a) {
             a = $(this).data("target");
             var b = a.replace("-content", "-detail"),
                 c = "search[category]=" + $(this).data("id-category") + "&lang=" + lang;
             $(a).find(".year-rp").length &&
                 (c += "&search[year]=" + $(a).find(".year-rp").find("button").text());
             $.ajax({
                 type: "GET",
                 url: domain_investor_report,
                 dataType: "json",
                 data: c,
                 success: function (a) {
                     if (200 == a.status && (a = a.data.investor_reports, void 0 != a)) {
                         $(b).html("");
                         for (var c = $("#reports-presentations-template").html(), d = 0; d < a.length; d++) {
                             var e = replaceText(c, "{{TITLE}}", a[d].title);
                             e = replaceText(e, "{{URL}}", a[d].pdf_url);
                             e = replaceText(e, "{{ID}}", a[d].id);
                             null != a[d].cover_thumb ? (e = replaceText(e, "{{IMG}}", a[d].cover_thumb), e = replaceText(e,
                                 "hidden", "")) : e = replaceText(e, "{{IMG}}", "");
                             "en" == lang && (e = replaceText(e, "Unduh", "Download"));
                             $(b).append(e)
                         }
                     }
                 }
             })
         });
         $(document).keydown(function (a) {
             27 == a.keyCode && ($(".close").trigger("click"), window.close())
         });
         $("#thanksmodal button").click(function (a) {
             if ($(this).attr('success') == 1) {
                 window.location = domain + "/wbs/konsul"; //UAT
                 //window.location = domain + "/ga/wbs/konsul"; //PROD
             } else {
                 $("#thanksmodal").removeClass("in");
                 $(".modal-backdrop.in").remove();
                 $("body").removeClass("modal-open");
                 $("#thanksmodal").fadeOut(200)
             }
         });
         $(".modal").on("show.bs.modal", function () {
             0 < $(".pre-scrollable").length && (204 < $(".text-testimoni").height() ? ($(".pre-scrollable").scrollTop(-100),
                 $(".pre-scrollable").scrollTop(0)) : ($(".ps-scrollbar-y-rail").css("height", "0px"), $(".ps-scrollbar-y").css("height", "0px")))
         });
         $(".modal").on("shown.bs.modal", function () {
             0 < $(".pre-scrollable").length && $(".pre-scrollable").scrollTop(0)
         });
         0 != $(".navbar-custom .dropdown-menu").find(".active").length && $(".navbar-custom .dropdown-menu").find(".active").closest(".dropdown").addClass("active");
         $(".dropdown-menu-custom li").hover(function (a) {
             $(this).children("a").css("color", "#33A4FF")
         }, function (a) {
             $(this).children("a").css("color",
                 "#333333")
         });
         $("#lang-about-dd.dropdown").click(function (a) {
             a.preventDefault()
         });
         $("#dd-lang li a").click(function (a) {
             window.location.href = $(this).attr("href");
             a.preventDefault()
         });
         $("#promotion-search #search-lokasi-klinik li a").click(function (a) {
             location.href = $(this).attr("href")
         });
         $("#search-category-article li a").click(function (a) {
             location.href = $(this).attr("href")
         });
         $(".dropdown-menu li a").click(function (a) {
             a = $(this).closest(".dropdown").attr("id");
             if ("spesialisasi-sr" == a) {
                 var b = "#" + a + " .dropdown-button:first-child";
                 0 > $("#" + a + " span.text-elipsis").length ? $(b).append("<span class='text-elipsis'>" + $(this).text() + "</span>") : $("#spesialisasi-sr span.text-elipsis").text($(this).text());
                 $(b).val($(this).text())
             } else a = "#" + a + " .dropdown-button:first-child", $(a).text($(this).text()), $(a).val($(this).text())
         });
         $(".dropdown-res button").click(function (a) {
             $("#res-klinik-home").is(":hidden") ? $("#res-klinik-home").show() : ($("#res-klinik-home").hide(), 768 < $(window).width() && $("#res-klinik-home").show());
             a.preventDefault()
         });
         $("#submit-contact-button").click(function (a) {
             $("#contact-form").yiiActiveForm("validate", !0)
         });
         $(".form-group.has-error input").on("input", function (a) {
             $(this).parents(".has-error").removeClass("has-error")
         });
         $(".form-group.has-error textarea").on("input", function (a) {
             $(this).parents(".has-error").removeClass("has-error")
         });
         $(".nav-fasilitas-rs li").click(function (a) {
             $(".nav-fasilitas-rs li.active").removeClass("active");
             var b = $(this);
             b.hasClass("active") || b.addClass("active");
             a.preventDefault()
         });
         $("#submenu2 li a").click(function (a) {
             $("#submenu2 li a.open").removeClass("open");
             var b = $(this);
             b.hasClass("open") || b.addClass("open");
             b = b.text();
             $("#current-infors").text(b);
             767 >= $(window).width() && $("#toggle-infors").click();
             $("#submenu2").addClass("in");
             $("html, body").animate({
                 scrollTop: $("#menu-fasilitas-rs").offset().top
             }, 1E3);
             a.preventDefault()
         });
         $("#submenu4 li a").click(function (a) {
             $("#submenu4 li a.open").removeClass("open");
             var b = $(this);
             b.hasClass("open") || b.addClass("open");
             b = b.text();
             $("#current-infors").text(b);
             767 >= $(window).width() && $("#toggle-infors").click();
             $("html, body").animate({
                 scrollTop: $("#menu-fasilitas-rs").offset().top
             }, 1E3);
             a.preventDefault()
         });
         $("#sub-our-profile li a").click(function (a) {
             $("#sub-our-profile li a.open").removeClass("open");
             var b = $(this);
             b.hasClass("open") || b.addClass("open");
             a.preventDefault()
         });
         $("#sub-csr li a").click(function (a) {
             $("#sub-csr li a.open").removeClass("open");
             var b = $(this);
             b.hasClass("open") || b.addClass("open");
             a.preventDefault()
         });
         $("#sub-our-management li a").click(function (a) {
             $("#sub-our-management li a.open").removeClass("open");
             var b = $(this);
             b.hasClass("open") || b.addClass("open");
             $("html, body").animate({
                 scrollTop: $(".content-about-us").offset().top - 200
             }, 1E3);
             a.preventDefault()
         });
         $("#sub-corporate-governance li a").click(function (a) {
             $("#sub-corporate-governance li a.open").removeClass("open");
             var b = $(this);
             b.hasClass("open") || b.addClass("open");
             a.preventDefault()
         });
         $("#sub-investor-relation li a").click(function (a) {
             $("#sub-investor-relation li a.open").removeClass("open");
             var b = $(this);
             b.hasClass("open") || b.addClass("open");
             a.preventDefault()
         });
         $("#rp-menu li a").click(function (a) {
             $("#rp-menu li a.open").removeClass("open");
             var b = $(this);
             b.hasClass("open") || b.addClass("open");
             b = b.text();
             $("#current-investor").text(b);
             767 >= $(window).width() && $("#toggle-investor").click();
             a.preventDefault()
         });
         $("#sc-menu li a").click(function (a) {
             $("#sc-menu li a.open").removeClass("open");
             var b = $(this);
             b.hasClass("open") || b.addClass("open");
             b = b.text();
             $("#current-investor").text(b);
             767 >= $(window).width() && $("#toggle-investor").click();
             a.preventDefault()
         });
         $("#toggle-investor").click(function () {
             0 < $("#sc-menu .open").length && ($("#sc-menu").addClass("in"), $("#sc-menu").collapse());
             0 < $("#rp-menu .open").length && ($("#rp-menu").addClass("in"), $("#rp-menu").collapse())
         });
         $("#menu-investor-news>li>a").click(function (a) {
             if ("rp-tab" == $(this).attr("id")) {
                 $("#rp-menu li a").removeClass("open");
                 if (768 < $(window).width()) {
                     $("#rp-menu .nav li:first a").first().addClass("open");
                     var b = $(".report-presentation-tab").first().data("target");
                     $(".tab-pane").not(b).removeClass("in");
                     $(".tab-pane").not(b).removeClass("active");
                     $(b).addClass("in");
                     $(b).addClass("active")
                 }
                 $("#rp-tab").removeClass("collapsed");
                 $("#menu-investor-news li a").not("#rp-tab").removeClass("active");
                 $("#sc-menu").removeClass("in");
                 $("#sc-menu").removeClass("active");
                 $("#sc-menu").find(".open").removeClass("open");
                 $("#rp-menu li:first a").addClass("open");
                 $("#rp-menu").collapse();
                 $("#rp-menu").addClass("in");
                 $("#rp-menu").addClass("active")
             } else "shareholder-composition-tab" ==
                 $(this).attr("id") ? ($("#shareholder-composition-tab").removeClass("collapsed"), $("#menu-investor-news li a").not("#shareholder-composition-tab").removeClass("active"), $("#sc-menu li a").not(":first").removeClass("open"), $("#sc-menu li a:first").removeClass("open"), $("#sc-menu").collapse(), $("#rp-menu").removeClass("in"), $("#rp-menu").removeClass("active"), $("#rp-menu").find(".open").removeClass("open"), $("#sc-menu").addClass("in"), $("#sc-menu").addClass("active"), 768 < $(window).width() && ($(".shareholder-tab").first().addClass("open"),
                     b = $(".shareholder-tab").first().data("target"), $(".tab-pane").not(b).removeClass("in"), $(".tab-pane").not(b).removeClass("active"), $(b).addClass("in"), $(b).addClass("active"))) : ($("#sc-menu").find(".open").removeClass("open"), $("#rp-menu").find(".open").removeClass("open"), $("#rp-menu").removeClass("in"), $("#sc-menu").removeClass("in"), $("#rp-menu").removeClass("active"), $("#sc-menu").removeClass("active"), b = $(this).text(), $("#current-investor").text(b), 767 >= $(window).width() && $("#toggle-investor").click());
             $("html, body").animate({
                 scrollTop: 100
             }, 1E3);
             a.preventDefault()
         });
         $("#fk-tab").click(function (a) {
             $("#fk-tab").removeClass("collapsed");
             $("#menu-info-rs li a").not("#fk-tab").removeClass("active");
             $("a.nav-link").not("#pelayanan-umum-tab").removeClass("open");
             $("#submenu4.collapse").removeClass("in");
             $("#submenu4.collapse").removeClass("active");
             $("#submenu2.collapse").addClass("in");
             $("#submenu2.collapse").addClass("active");
             $("#pelayanan-umum-tab").removeClass("open");
             768 < $(window) && ($("#layanan-unggulan-konten").removeClass("active"),
                 $("#rekanan-perusahaan-konten").removeClass("active"), $(".content-fasilitas-rs .tab-pane").not("#pelayanan-umum-content").removeClass("in"), $(".content-fasilitas-rs .tab-pane").not("#pelayanan-umum-content").removeClass("active"), $("#pelayanan-umum-tab").addClass("open"), $("#pelayanan-umum-content").addClass("in"), $("#pelayanan-umum-content").addClass("active"));
             a.preventDefault()
         });
         $("#rekanan-tab").click(function (a) {
             $("#rekanan-tab").removeClass("collapsed");
             $("#menu-info-rs li a").not("#rekanan-tab").removeClass("active");
             $("a.nav-link").not("#sub-rekanan-perusahaan").removeClass("open");
             $("#submenu2.collapse").removeClass("in");
             $("#submenu2.collapse").removeClass("active");
             $("#submenu4.collapse").addClass("in");
             $("#submenu4.collapse").addClass("active");
             $("#sub-rekanan-perusahaan").removeClass("open");
             768 < $(window) && ($("#layanan-unggulan-konten").removeClass("active"), $("#pelayanan-umum-content").removeClass("active"), $(".content-fasilitas-rs .tab-pane").not("#rekanan-perusahaan-konten").removeClass("in"), $(".content-fasilitas-rs .tab-pane").not("#rekanan-perusahaan-konten").removeClass("active"),
                 $("#rekanan-perusahaan-konten").addClass("in"), $("#rekanan-perusahaan-konten").addClass("active"), $("#sub-rekanan-perusahaan").addClass("open"));
             a.preventDefault()
         });
         $("#sub-our-brand").click(function () {
             $("#menu-about us li").not("#sub-our-brand").removeClass("active");
             $("#sub-our-management").removeClass("in");
             $("#sub-our-management").removeClass("active");
             $("#sub-our-management li a.open").removeClass("open");
             $("html, body").animate({
                 scrollTop: $(".content-about-us").offset().top - 200
             }, 1E3)
         });
         $("#sub-vision-mission").click(function () {
             $("#menu-about us li").not("#sub-vision-mission").removeClass("active");
             $("#sub-our-management").removeClass("in");
             $("#sub-our-management").removeClass("active");
             $("#sub-our-management li a.open").removeClass("open");
             $("html, body").animate({
                 scrollTop: $(".content-about-us").offset().top - 200
             }, 1E3)
         });
         $("#sub-milestone").click(function () {
             $("#menu-about us li").not("#sub-milestone").removeClass("active");
             $("#sub-our-management").removeClass("in");
             $("#sub-our-management").removeClass("active");
             $("#sub-our-management li a.open").removeClass("open");
             $("html, body").animate({
                 scrollTop: $(".content-about-us").offset().top -
                     200
             }, 1E3)
         });
         $("#our-profile-tab").click(function (a) {
             $("div.tab-pane").not("#our-brand-content").removeClass("in");
             $("div.tab-pane").not("#our-brand-content").removeClass("active");
             $("a.nav-link").not("#sub-our-brand").removeClass("open");
             $("#sub-our-brand").addClass("open");
             $("#our-brand-content").addClass("in");
             $("#our-brand-content").addClass("active");
             a.preventDefault()
         });
         $("#our-management-tab").click(function (a) {
             768 < $(window).width() && ($("div.tab-pane").not("#board-directors-content").removeClass("in"),
                 $("div.tab-pane").not("#board-directors-content").removeClass("active"), $("#board-directors-content").addClass("in"), $("#board-directors-content").addClass("active"));
             $("a.nav-link").not("#sub-board-directors").removeClass("open");
             $("#sub-board-directors").addClass("open");
             $("html, body").animate({
                 scrollTop: $(".content-about-us").offset().top - 200
             }, 1E3);
             a.stopPropagation();
             a.preventDefault();
             $("#sub-our-management").addClass("in")
         });
         $("#investor-relation-tab").click(function (a) {
             $("div.tab-pane").not("#investor-news").removeClass("in");
             $("div.tab-pane").not("#investor-news").removeClass("active");
             $("a.nav-link").not("#sub-investor-news").removeClass("open");
             $("#sub-investor-news").addClass("open");
             $("#investor-news").addClass("in");
             $("#investor-news").addClass("active");
             a.preventDefault()
         });
         $("#corporate-governance-tab").click(function (a) {
             $("div.tab-pane").not("#corporate-governance-content").removeClass("in");
             $("div.tab-pane").not("#corporate-governance-content").removeClass("active");
             $("a.nav-link").not("#sub-structure").removeClass("open");
             $("#sub-structure").addClass("open");
             $("#structure-content").addClass("in");
             $("#structure-content").addClass("active");
             a.preventDefault()
         });
         $("#csr-tab").click(function (a) {
             $("div.tab-pane").not("#csr-programs-content").removeClass("in");
             $("div.tab-pane").not("#csr-programs-content").removeClass("active");
             $("a.nav-link").not("#sub-csr-programs").removeClass("open");
             $("#sub-csr-programs").addClass("open");
             $("#csr-programs-content").addClass("in");
             $("#csr-programs-content").addClass("active");
             a.preventDefault()
         });
         $("#fasilitas-kami-tab").click(function (a) {
             $("#fasilitas-kami-tab").removeClass("collapsed");
             $("#menu-info-rs li a").not("#fasilitas-kami-tab").removeClass("active");
             $("#submenu2.collapse").removeClass("in");
             $("#submenu2.collapse").removeClass("active");
             $("#submenu4.collapse").removeClass("in");
             $("#submenu4.collapse").removeClass("active");
             $("#layanan-unggulan-konten").addClass("in");
             $("#layanan-unggulan-konten").addClass("active");
             var b = $(this).text();
             $("#current-infors").text(b);
             $("#toggle-infors").click();
             $("html, body").animate({
                 scrollTop: $("#menu-fasilitas-rs").offset().top
             }, 1E3);
             a.preventDefault()
         });
         $("#indikator-mutu-tab").click(function (a) {
             $(this).removeClass("collapsed");
             $("#menu-info-rs li a").not(this).removeClass("active");
             $("#submenu2.collapse").removeClass("in");
             $("#submenu2.collapse").removeClass("active");
             $("#submenu4.collapse").removeClass("in");
             $("#submenu4.collapse").removeClass("active");
             $("#indikator-mutu-konten").addClass("in");
             $("#indikator-mutu-konten").addClass("active");
             var b =
                 $(this).text();
             $("#current-infors").text(b);
             $("#toggle-infors").click();
             $("html, body").animate({
                 scrollTop: $("#menu-fasilitas-rs").offset().top
             }, 1E3);
             a.preventDefault()
         });
         $("#sertifikasi-tab").click(function (a) {
             $(this).removeClass("collapsed");
             $("#menu-info-rs li a").not(this).removeClass("active");
             $("#submenu2.collapse").removeClass("in");
             $("#submenu2.collapse").removeClass("active");
             $("#submenu4.collapse").removeClass("in");
             $("#submenu4.collapse").removeClass("active");
             $("#sertifikasi-konten").addClass("in");
             $("#sertifikasi-konten").addClass("active");
             var b = $(this).text();
             $("#current-infors").text(b);
             $("#toggle-infors").click();
             $("html, body").animate({
                 scrollTop: $("#menu-fasilitas-rs").offset().top
             }, 1E3);
             a.preventDefault()
         });
         $("#nav-hs li").on("click", function (a) {
             var b = $(this);
             $("#nav-hs li").removeClass("active");
             b.hasClass("active") || b.addClass("active");
             b = $(this).data("href");
             $(".panel-hs").removeClass("in");
             $(b).addClass("in");
             a.preventDefault()
         });
         $("#menu-fasilitas-rs").on("show.bs.collapse", ".collapse",
             function () {
                 $("#menu-fasilitas-rs").find(".col-md-3 .collapse.in").collapse("hide")
             });
         $("#menu-about-us").on("show.bs.collapse", ".collapse", function () {
             $("#menu-about-us").find(".collapse.in").collapse("hide")
         });
         $("ul.nav-pills a").click(function (a) {
             a.preventDefault();
             $(this).tab("show")
         });
         $(document).click(function (a) {
             a = $(a.target);
             !0 !== $("#res-klinik-home").is(":visible") || a.hasClass("dropdown-toggle") || $("button.dropdown-toggle").click()
         });
         $("#milestone-about").carousel({
             interval: 0
         });
         $("#carousel-gallery-klinik").carousel({
             interval: 0
         });
         0 < $("#lightgallery").length && $("#lightgallery").lightGallery({
             selector: ".image-gallery"
         });
         $(".pre-scrollable").perfectScrollbar();
         $(".container-res-klinik-home").perfectScrollbar();
         0 < $(".scroll-cv-dokter").length && ($(".scroll-cv-dokter").scrollTop($(".container-res-klinik-home").scrollTop() + 1), $(".scroll-cv-dokter").scrollTop($(".container-res-klinik-home").scrollTop() - 1), $(".scroll-cv-dokter").addClass("ps-active-y"), $(".ps-scrollbar-y-rail").css("height", "250px"), $(".ps-scrollbar-y").css("height",
             "35px"));
         $(".card-home-lk .btn-dropdown-res").click(function () {
             $(".container-res-klinik-home").scrollTop($(".container-res-klinik-home").scrollTop() + 1);
             $(".container-res-klinik-home").scrollTop($(".container-res-klinik-home").scrollTop() - 1);
             $(".container-res-klinik-home").addClass("ps-active-y")
         });
         $(".accordion-paket").on("shown.bs.collapse", function () {
             var a = $(this).find(".in");
             $("html, body").animate({
                 scrollTop: a.offset().top - 100
             }, 1E3)
         });
         $("#accordion-corporate-information").on("shown.bs.collapse",
             function () {
                 $("html, body").animate({
                     scrollTop: $("#accordion-corporate-information").offset().top - 100
                 }, 1500)
             });
         $("#accordion-shareholder-composition").on("shown.bs.collapse", function () {
             var a = $(this).find(".in");
             $("html, body").animate({
                 scrollTop: a.offset().top - 100
             }, 500)
         });
         $(".card-home-lk .dropdown-res button").click(function () {
             $(".container-res-klinik-home:visible").length ? $(this).find(".caret").css("top", "10%") : $(this).find(".caret").css("top", "50%")
         });
         $("#res-klinik-home li").click(function (a) {
             $("#res-klinik-home li.active").removeClass("active");
             var b = $(this);
             b.hasClass("active") || b.addClass("active");
             $("#name-klinik-home").text($(this).children("a").text());
             $("#res-klinik-home").hide();
             a.preventDefault();
             767 < $(window).width() ? ($("#res-klinik-home").show(), $(".lazy-lokasiklinik-home").lazy({
                 bind: "event",
                 asyncLoader: function (a, b) {
                     setTimeout(function () {
                         b(!0)
                     }, 1E3)
                 }
             })) : (a = $(this).find("a").attr("href"), b = $(".rumah-sakit-home " + a + " .home-image-klinik").data("imgresponsive"), $(".rumah-sakit-home " + a + " .home-image-klinik").css("background-image",
                 "url(" + b + ")"));
             $(".card-home-lk .dropdown-res button .caret").css("top", "50%")
         });
         $(document).on("click", ".jadwal-dokter .day-text .available", function () {
             $(this).closest(".card-search").attr("id");
             var a = $(this).data("buatjanji"),
                 b = $(this).data("date"),
                 c = [],
                 f = $(this).data("date"),
                 g = $(this).data("poli"),
                 h = $(this).data("waktu"),
                 j = $(this).data("idmika");
             for (i = 0; 6 >= i; i++) i != a && c.push(i);
             var e = new Date(b);
             e.setDate(e.getDate());
             var l = {
                     locale: "id",
                     format: "DD MMMM YYYY",
                     widgetPositioning: {
                         vertical: "bottom"
                     },
                     defaultDate: e
                 },
                 d = null;
             switch (a) {
                 case 0:
                     d = "Minggu";
                     break;
                 case 1:
                     d = "Senin";
                     break;
                 case 2:
                     d = "Selasa";
                     break;
                 case 3:
                     d = "Rabu";
                     break;
                 case 4:
                     d = "Kamis";
                     break;
                 case 5:
                     d = "Jumat";
                     break;
                 case 6:
                     d = "Sabtu"
             }
             $("#datetimepicker-buat-janji").datetimepicker(l);
             $("#datetimepicker-buat-janji").data("DateTimePicker").daysOfWeekDisabled(c);
             $("#datetimepicker-buat-janji").data("DateTimePicker").date(e);
             $("#buatjanjimodal #input-date").val(b);
             a = e.toLocaleString("id-ID", {
                 year: "numeric",
                 day: "numeric",
                 month: "long"
             });
             $("#buatjanjimodal .hari-janji").text(d);
             d = $(this).data("jam");
             $("#jam-buat-janji .dropdown-button:first-child").text(d);
             $("#jam-buat-janji .dropdown-button:first-child").val(d);
             $("#buatjanjimodal #input-time").val(d);
             $("#buatjanjimodal #input-jadwalid").val($(this).data("jadwalid"));

             $("#buatjanjimodal #rs_id").val($(this).data("rs_id"));

             $("#buatjanjimodal #input-hari").val($(this).data("namahari"));
             $("#buatjanjimodal .jadwal-janji").text(d);
             $("#buatjanjimodal .tanggal-janji").text(a);
             $("#buatjanjimodal #nama-bj").text($(this).data("doctorname"));
             $("#buatjanjimodal .spesialisasi-buat-janji h4").text($(this).data("specialization"));
             $("#buatjanjimodal .foto-info-dokter").attr("src", $(this).data("doctorphoto"));
             $("#buatjanjimodal #input-iddoctor").val($(this).data("iddoctor"));
             $("#buatjanjimodal #input-doctorid").val($(this).data("doctorid"));
             $("#buatjanjimodal #input-namadoctor").val($(this).data("doctorname"));
             $("#datetimepicker-tgl-konsul").val(f);
             $("#buatjanjimodal #input-idclinic").val(g);
             $("#buatjanjimodal #input-namaclinic").val($(this).data("specialization"));
             $("#buatjanjimodal #waktu-kosul").val(h);
             $("#buatjanjimodal #input-idmika").val(j);
             $("#thanksmodal-header").html("");
             $('#buatjanjimodal .buat-janji-body').show();
             $('#buatjanjimodal .buat-janji-disclaimer').hide();

         });
         $("#jadwal-lokasi-klinik .day-text .available").click(function (a) {
             a.stopPropagation();
             $(this).closest(".jadwal-detail-dokter").attr("id");
             a = $(this).data("buatjanji");
             var b = $(this).data("jam"),
                 c = null;
             switch (a) {
                 case 0:
                     c = "Senin";
                     break;
                 case 1:
                     c = "Selasa";
                     break;
                 case 2:
                     c = "Rabu";
                     break;
                 case 3:
                     c = "Kamis";
                     break;
                 case 4:
                     c = "Jumat";
                     break;
                 case 5:
                     c = "Sabtu"
             }
             $("#buatjanjimodal1 .hari-janji").html(c);
             $("#buatjanjimodal1 .jadwal-janji").html(b);
             $("#buatjanjimodal .hari-janji").html(c);
             $("#buatjanjimodal .jadwal-janji").html(b)
         });
         $(document).click(function (a) {
             $(a.target).is(".day-text .available") || $(".button-buat-janji").attr("disabled", !0)
         });
         $("#datetimepicker1").datetimepicker({
             locale: "id",
             format: "DD MMMM YYYY",
             daysOfWeekDisabled: [0],
             widgetPositioning: {
                 vertical: "bottom"
             },
             minDate: new Date
         });
         $("#tanggal-lahir-date").datetimepicker({
             locale: "id",
             format: "DD MMMM YYYY",
             widgetPositioning: {
                 vertical: "bottom"
             }
         });
         $(".btn-career").click(function () {
             var a = $(this).data("title-career");
             $("#input-idcareer").val($(this).data("id"));
             $(".apply-career-title").text(a)
         });
         $("#upload-cv").change(function () {
             $(".button-cv-career").text($(this).val().replace(/C:\\fakepath\\/i,
                 ""))
         });
         $(".pull-down-promotion").each(function () {
             var a = $(this);
             a.css("margin-top", a.parent().height() - a.prev(".content-gray").height() - a.height())
         });
         $("#search-lokasi-klinik li a").click(function () {
             var a = $(this).data("href");
             $("#search-lokasi-klinik").data("href", a)
         });
         $(".search-klinik .button-klinik .button-search-klinik").click(function () {
             var a = $("#search-lokasi-klinik").data("href");
             location.href = a
         });
         (m = document.location.hash) && 0 < $(".nav-klinik-top").length && $('.nav-klinik-top a[href="' + m + '"]').tab("show");
         $(".navbar-custom .dropdown-menu a").on("click", function () {
             window.location.href = $(this).attr("href")
         });
         $(".klinik-content").hide();
         $(".klinik-content").fadeIn(200);
         $(".loading-klinik").addClass("hidden");
         $(window).load(function () {
             $(".tab-pane.active").fadeIn(200);
             $(".loading-klinik").addClass("hidden");
             $(".klinik-content").fadeIn(200)
         });
         $(".lokasi-klinik .glyphicon-map-marker").fadeIn(200);
         $("#search-promo .glyphicon-map-marker").fadeIn(200);
         $("#search-news .glyphicon-map-marker").fadeIn(200);
         $(".nav-klinik-top a").on("shown.bs.tab",
             function (a) {
                 a.preventDefault()
             }).on("click", function (a) {
             a.preventDefault()
         });
         $(".navbar-custom .dropdown ul li a").click(function () {
             location.href = $(this).attr("href")
         });
         $(".milestone-indicator").on("click", ".prev", function (a) {
             a.preventDefault();
             a = $("#milestone-about .carousel-indicators").width() - 15;
             a = $("#milestone-about .carousel-indicators").scrollLeft() - a;
             $("#milestone-about .carousel-indicators").animate({
                 scrollLeft: a
             }, 2E3)
         });
         $(".milestone-indicator").on("click", ".next", function (a) {
             a.preventDefault();
             a = $("#milestone-about .carousel-indicators").width() - 15;
             a = $("#milestone-about .carousel-indicators").scrollLeft() + a;
             $("#milestone-about .carousel-indicators").animate({
                 scrollLeft: a
             }, 2E3)
         });
         $(".clinic-location.right").click(function () {
             var a = $("#pills-klinik-tab .active").next("li").length ? $("#pills-klinik-tab .active").next("li") : $("#pills-klinik-tab li:first");
             $("#pills-klinik-tab").hide();
             $("#pills-klinik-tab .active").removeClass("active");
             a.addClass("active");
             $('.nav-klinik-top a[href="#' + a.children("a").attr("href") +
                 '"]').tab("show");
             $("#pills-tabContent .tab-pane").removeClass("in");
             $("#pills-tabContent .tab-pane").removeClass("active");
             $(a.children("a").attr("href")).addClass("in");
             $(a.children("a").attr("href")).addClass("active");
             a = $(a.children("a")).attr("href").substring(1);
             ChangeUrl("klinik", a, url_changeurl + a);
             $("#pills-klinik-tab").fadeIn(500)
         });
         $(".clinic-location.left").click(function () {
             var a = $("#pills-klinik-tab .active").prev("li").length ? $("#pills-klinik-tab .active").prev("li") : $("#pills-klinik-tab li:last");
             $("#pills-klinik-tab").hide();
             $("#pills-klinik-tab .active").removeClass("active");
             a.addClass("active");
             $('.nav-klinik-top a[href="#' + a.children("a").attr("href") + '"]').tab("show");
             $("#pills-tabContent .tab-pane").removeClass("in");
             $("#pills-tabContent .tab-pane").removeClass("active");
             $(a.children("a").attr("href")).addClass("in");
             $(a.children("a").attr("href")).addClass("active");
             a = $(a.children("a")).attr("href").substring(1);
             ChangeUrl("klinik", a, url_changeurl + a);
             $("#pills-klinik-tab").fadeIn(500)
         });
         $("#menu-investor-news a").on("shown.bs.tab", function (a) {
             a = $(this).attr("href").substring(1);
             ChangeUrl("investor-relation", a, domain + "/investor-relation/" + lang + "/" + a)
         });
         $("#pills-klinik-tab a").on("shown.bs.tab", function (a) {
             a = $(this).attr("href").substring(1);
             5 < count || ChangeUrl("klinik", a, url_changeurl + a)
         });
         $(".button-upload-profil").click(function () {
             $("#upload-foto-profil").trigger("click")
         });
         $("#upload-foto-profil").change(function () {
             t(this)
         });
         $(".carousel-caption-pasien .btn-white-testimoni").click(function () {
             $("#modal-testimonial-detail .foto-testimoni-detail").attr("src",
                 $(this).data("foto"));
             $("#modal-testimonial-detail #nama-testimoni").text($(this).data("nama"));
             $("#modal-testimonial-detail #spesialis-testimoni").text($(this).data("spesialis"));
             $("#modal-testimonial-detail .text-testimoni").html($(this).data("text"))
         });
         $(".load-more").click(function (a) {
             a.preventDefault();
             var b = $(this);
             b.next("span").removeClass("hidden");
             b.attr("disabled", "disabled");
             a = b.data("action");
             var c = b.data("limit"),
                 e = b.data("page"),
                 l = b.data("target"),
                 d = b.data("type"),
                 y = $("#" + l + "-template").html();
             if ("news-event" == d) d = $("#search-news #w0").val(), d = "All" == d ? null : d, u(a, null != d ? "search[clinic]=" + d + "&limit=" + c + "&offset=" + c * (e - 1) : "limit=" + c + "&offset=" + c * (e - 1), function (a) {
                 var c = b.attr("href").split("page")[0] + "page=" + (e + 2);
                 b.attr("href", c);
                 b.data("page", e);
                 if (200 == a.status && 0 < a.data.articles.length) {
                     a = a.data.articles;
                     e++;
                     for (c = 0; c < a.length; c++) {
                         var d = domain + "/news/detail/" + a[c].slug,
                             h = new Date(a[c].date_posted),
                             l = replaceText(y, "{{ID}}", a[c].id);
                         l = replaceText(l, "SLUG", d);
                         l = replaceText(l, "{{IMAGE}}",
                             a[c].image_full);
                         l = replaceText(l, "{{TITLE}}", a[c].title);
                         l = replaceText(l, "{{DATE}}", h.toLocaleString("id-ID", {
                             year: "numeric",
                             day: "numeric",
                             month: "long"
                         }));
                         l = replaceText(l, "{{EXCERPT}}", a[c].excerpt.replace(/^(.{134}).+/, "$1&hellip;"));
                         $("#news-content").append(l);
                         if (3 < a[c].location.length) $("#detail-location-promotion-" + a[c].id).append("<span class='content-gray'>Grup</span>");
                         else
                             for (d = 0; d < a[c].location.length; d++) d == a[c].location.length - 1 ? $("#detail-location-promotion-" + a[c].id).append("<span class='content-gray'>" +
                                 a[c].location[d] + "</span>") : $("#detail-location-promotion-" + a[c].id).append("<span class='content-gray'>" + a[c].location[d] + "</span>,")
                     }
                     b.data("page", e)
                 } else b.fadeOut(), $(".all-loaded").addClass("in"), $(".all-loaded").fadeIn(), setTimeout(function () {
                     $(".all-loaded").fadeOut()
                 }, 1E3);
                 b.next("span").addClass("hidden");
                 b.removeAttr("disabled")
             });
             else if ("promotion" == d) d = $("#search-promo #w0").val(), d = "All" == d ? null : d, u(a, null != d ? "search[clinic]=" + d + "&limit=" + c + "&offset=" + c * (e - 1) : "limit=" + c + "&offset=" + c *
                 (e - 1),
                 function (a) {
                     var c = b.attr("href").split("page")[0] + "page=" + (e + 2);
                     b.attr("href", c);
                     b.data("page", e);
                     if (200 == a.status && 0 < a.data.promos.length) {
                         a = a.data.promos;
                         e++;
                         for (c = 0; c < a.length; c++) {
                             var d = domain + "/promo/detail/" + a[c].slug,
                                 h = new Date(a[c].start_date),
                                 k = replaceText(y, "{{ID}}", a[c].id);
                             k = replaceText(k, "SLUG", d);
                             k = replaceText(k, "{{IMAGE}}", a[c].image_full);
                             k = replaceText(k, "{{TITLE}}", a[c].title);
                             k = replaceText(k, "{{DATE}}", h.toLocaleString("id-ID", {
                                 year: "numeric",
                                 day: "numeric",
                                 month: "long"
                             }));
                             k = replaceText(k, "{{EXCERPT}}", a[c].excerpt.replace(/^(.{134}).+/, "$1&hellip;"));
                             $("#promotion-content").append(k);
                             if (3 < a[c].location.length) $("#detail-location-promotion-" + a[c].id).append("<span class='content-gray'>Grup</span>");
                             else
                                 for (d = 0; d < a[c].location.length; d++) d == a[c].location.length - 1 ? $("#detail-location-promotion-" + a[c].id).append("<span class='content-gray'>" + a[c].location[d] + "</span>") : $("#detail-location-promotion-" + a[c].id).append("<span class='content-gray'>" + a[c].location[d] + "</span>,")
                         }
                         b.data("page",
                             e)
                     } else b.fadeOut(), $("#" + l).next(".all-loaded").addClass("in"), $("#" + l).next(".all-loaded").fadeIn(), setTimeout(function () {
                         $("#" + l).next(".all-loaded").fadeOut()
                     }, 1E3);
                     b.next("span").addClass("hidden");
                     b.removeAttr("disabled")
                 });
             else if ("article" == d) d = $("#input-keyword").val(), "" != d ? c = "keywords=" + d + "&limit=" + c + "&offset=" + c * (e - 1) : (a = "article", c = "limit=" + c + "&offset=" + c * (e - 1)), u(a, c, function (a) {
                 var c = b.attr("href").split("page")[0] + "page=" + (e + 2);
                 b.attr("href", c);
                 b.data("page", e);
                 if (200 == a.status && 0 <
                     a.data.articles.length) {
                     a = a.data.articles;
                     e++;
                     for (c = 0; c < a.length; c++) {
                         var d = domain + "/artikel/" + a[c].category_slug + "/" + a[c].slug,
                             l = new Date(a[c].date_posted),
                             h = replaceText(y, "{{ID}}", a[c].id);
                         h = replaceText(h, "SLUG", d);
                         h = replaceText(h, "{{IMAGE}}", a[c].image_full);
                         h = replaceText(h, "{{TITLE}}", a[c].title);
                         h = replaceText(h, "{{DATE}}", l.toLocaleString("id-ID", {
                             year: "numeric",
                             day: "numeric",
                             month: "long"
                         }));
                         h = replaceText(h, "{{EXCERPT}}", a[c].excerpt.replace(/^(.{134}).+/, "$1&hellip;"));
                         $("#health-article-content").append(h)
                     }
                     b.data("page",
                         e)
                 } else b.fadeOut(), $(".all-loaded").addClass("in"), $(".all-loaded").fadeIn(), setTimeout(function () {
                     $(".all-loaded").fadeOut()
                 }, 1E3);
                 b.next("span").addClass("hidden");
                 b.removeAttr("disabled")
             });
             else if ("investor-news" == d) {
                 d = $("#select2-category-in").val();
                 var k = $("#year-ir button").text();
                 c = "search[category]=" + d + "&search[year]=" + k + "&limit=" + c + "&offset=" + c * (e - 1) + "&lang=" + lang;
                 u(a, c, function (a) {
                     var c = b.attr("href").split("page")[0] + "page=" + (e + 2);
                     b.attr("href", c);
                     b.data("page", e);
                     if (200 == a.status && 0 <
                         a.data.investor_news.length) {
                         a = a.data.investor_news;
                         e++;
                         for (c = 0; c < a.length; c++) {
                             var d = new Date(a[c].date_posted),
                                 l = replaceText(y, "{{URL}}", domain + "/investor-relation/" + lang + "/" + a[c].category_slug + "/" + a[c].slug);
                             l = replaceText(l, "{{TITLE}}", a[c].title);
                             l = replaceText(l, "{{DATE}}", d.toLocaleString("id-ID", {
                                 year: "numeric",
                                 day: "numeric",
                                 month: "long"
                             }));
                             $("#in-content").append(l)
                         }
                         b.data("page", e)
                     } else b.fadeOut(), $(".all-loaded").addClass("in"), $(".all-loaded").fadeIn(), setTimeout(function () {
                             $(".all-loaded").fadeOut()
                         },
                         1E3);
                     b.next("span").addClass("hidden");
                     b.removeAttr("disabled")
                 })
             }
         });
         $("#category-news-dd .scroll-dropdown li").click(function (a) {
             for (a = window.location.pathname.split("/");
                 "permata" != a[a.length - 1];) a.pop();
             a = a.join("/");
             a = "http://" + window.location.host + a + "/api/news";
             var b = $("#search-lokasi-klinik button").data("id");
             b = "All" == b ? null : b;
             $.ajax({
                 type: "GET",
                 url: a,
                 data: null != b ? "search[clinic]=" + b : "",
                 dataType: "json",
                 success: function (a) {
                     "" == a.data.articles ? $("#news-content").html("<div class='article-home nodata'> <p> Nantikan berita-berita seputar rumah sakit Mitra Keluarga </p> </div>") :
                         v(a.data.articles)
                 },
                 contentType: "application/json"
             })
         });
         $("#clinic-news-dd li a").click(function (a) {
             $("#search-lokasi-klinik button").data("id", $(this).data("id"));
             a = $(this).data("id");
             a = "All" == a ? null : a;
             $.ajax({
                 type: "GET",
                 url: url,
                 data: null != a ? "search[clinic]=" + a : "",
                 dataType: "json",
                 success: function (a) {
                     "" == a.data.articles ? ($("#news-content").html("<div class='article-home nodata'> <p> Nantikan berita-berita seputar rumah sakit Mitra Keluarga </p> </div>"), $(".load-more").hide()) : (v(a.data.articles),
                         $(".load-more").show())
                 },
                 contentType: "application/json"
             })
         });
         $("#spesialisasi-info-dokter").click(function (a) {
             a.preventDefault()
         });
         $("#spesialisasi-info-dokter li a").click(function (a) {
             a = $(this).data("id");
             var b = $("#w0").val();
             $.ajax({
                 type: "GET",
                 url: url_infodokter,
                 data: "All" == a ? "search[clinic]=" + b : "search[specialization]=" + a + "&search[clinic]=" + b,
                 dataType: "json",
                 success: function (a) {
                     a = a.data.doctors;
                     $("#info-dokter-content").html("");
                     if (void 0 != a)
                         for (var b = $("#info-dokter-template").html(), c = 0; c < a.length; c++) {
                             var d =
                                 replaceText(b, "{{DOCTORID}}", a[c].doctor.id);
                             d = replaceText(d, "{{DOCTORNAME}}", a[c].doctor.name);
                             d = replaceText(d, "{{IMAGE}}", a[c].doctor.photo_full);
                             d = replaceText(d, "{{DOCTORCV}}", a[c].doctor.description);
                             d = replaceText(d, "{{SLUG}}", a[c].doctor.slug);
                             d = replaceText(d, "{{SPNAME}}", a[c].doctor.specialization.name);
                             d = replaceText(d, "{{SSPNAME}}", a[c].doctor.specialization.slug);
                             d = replaceText(d, "{{SPID}}", a[c].doctor.specialization.id);
                             d = replaceText(d, "{{CLINICID}}", $("#clinic-location-name").data("id"));
                             d = replaceText(d, "{{CLINICNAME}}", $("#clinic-location-name").text());
                             0 < a[c].schedule.id ? (d = replaceText(d, "{{IS_MON}}", a[c].schedule.is_mon), d = replaceText(d, "{{IS_TUE}}", a[c].schedule.is_tue), d = replaceText(d, "{{IS_WED}}", a[c].schedule.is_wed), d = replaceText(d, "{{IS_THU}}", a[c].schedule.is_thu), d = replaceText(d, "{{IS_FRI}}", a[c].schedule.is_fri), d = replaceText(d, "{{IS_SAT}}", a[c].schedule.is_sat), d = replaceText(d, "{{IS_SUN}}", a[c].schedule.is_sun), d = replaceText(d, "{{MON}}", a[c].schedule.mon), d = replaceText(d,
                                 "{{TUE}}", a[c].schedule.tue), d = replaceText(d, "{{WED}}", a[c].schedule.wed), d = replaceText(d, "{{THU}}", a[c].schedule.thu), d = replaceText(d, "{{FRI}}", a[c].schedule.fri), d = replaceText(d, "{{SAT}}", a[c].schedule.sat), d = replaceText(d, "{{SUN}}", a[c].schedule.sun)) : d = replaceText(d, 'data-ismon="{{IS_MON}}" data-istue="{{IS_TUE}}" data-iswed="{{IS_WED}}" data-isthu="{{IS_THU}}" data-isfri="{{IS_FRI}}" data-issat="{{IS_SAT}}" data-issun="{{IS_SUN}}" data-mon="{{MON}}" data-tue="{{TUE}}" data-wed="{{WED}}" data-thu="{{THU}}" data-fri="{{FRI}}" data-sat="{{SAT}}" data-sun="{{SUN}}"',
                                 " ");
                             $("#info-dokter-content").append(d)
                         }
                 },
                 contentType: "application/json"
             })
         });
         0 < $("#info-dokter-modal").length && (m = $(".text-testimoni"), m.each(function () {
             var a = $(this).text();
             250 > a.length || $(this).html(a.slice(0, 250) + '<span>... </span><a href="#" class="more">Lihat lebih</a><span style="display:none;">' + a.slice(100, a.length) + ' <a href="#" class="less">Lihat sedikit</a></span>')
         }), $("a.more", m).click(function (a) {
             a.preventDefault();
             $(this).hide().prev().hide();
             $(this).next().show()
         }), $("a.less", m).click(function (a) {
             a.preventDefault();
             $(this).parent().hide().prev().show().prev().show()
         }));
         $("#info-dokter-modal").length && window.location.hash.length && (document.title = $('.info-dokter[data-id="' + window.location.hash.replace("#", "") + '"]').attr("data-name") + " - Klinik " + $('.info-dokter[data-id="' + window.location.hash.replace("#", "") + '"]').attr("data-spesialis") + " - Info Dokter | Mitra Keluarga - " + $('.info-dokter[data-id="' + window.location.hash.replace("#", "") + '"]').attr("data-nama-rs"), $("meta[name=title]").remove(), $("head").append('<meta name="title" content="' +
                 document.title + '">'), $("meta[name=description]").remove(), $('.info-dokter[data-id="' + window.location.hash.replace("#", "") + '"]').attr("data-cv").length ? $("head").append('<meta name="description" content="' + $('.info-dokter[data-id="' + window.location.hash.replace("#", "") + '"]').attr("data-cv") + '">') : $("head").append('<meta name="description" content="' + document.title + '">'), g = $('.info-dokter[data-id="' + window.location.hash.replace("#", "") + '"]'), $("#info-dokter-modal #doctor-name").text($(g).data("name")),
             $("#info-dokter-modal #doctor-spesialization").text($(g).data("spesialis")), $("#info-dokter-modal #doctor-clinic").text("Rumah Sakit Mitra Keluarga " + $(g).data("nama-rs")), $("#info-dokter-modal #doctor-photo").attr("src", $(g).data("src-photo")), 0 >= $(g).data("cv").length ? ($("#title-tentangdokter").addClass("hidden"), $(".scroll-cv-dokter").addClass("hidden")) : $("#info-dokter-modal #doctor-cv").html($(g).data("cv")), $('.info-dokter[data-id="' + window.location.hash.replace("#", "") + '"]').trigger("click"));
         $(document).on("click", "#info-dokter-content .info-dokter", function (a) {
             a.preventDefault();
             a = $(this).data("target");
             $(a).fadeIn();
             $("#info-dokter-modal #doctor-name").text($(this).data("name"));
             $("#info-dokter-modal #doctor-spesialization").text($(this).data("spesialis"));
             $("#info-dokter-modal #doctor-clinic").text("Rumah Sakit Mitra Keluarga " + $(this).data("nama-rs"));
             $("#info-dokter-modal #doctor-photo").attr("src", $(this).data("src-photo"));
             0 >= $(this).data("cv").length ? ($("#title-tentangdokter").addClass("hidden"),
                 $(".scroll-cv-dokter").addClass("hidden")) : $("#info-dokter-modal #doctor-cv").html($(this).data("cv"));
             a = new Date;
             a.getDay();
             a.getDay();
             a.getDay();
             a.getDay();
             a.getDay();
             a.getDay();
             a.getDay()
         });
         $(document).on("click", ".dokter-search .info-dokter", function (a) {
             a.preventDefault();
             a = $(this).data("target");
             $(a).fadeIn();
             $("#info-dokter-modal #doctor-name").text($(this).data("nametwo"));
             $("#info-dokter-modal #doctor-spesialization").text($(this).data("spesialis"));
             $("#info-dokter-modal #doctor-clinic").text("Rumah Sakit Mitra Keluarga " +
                 $(this).data("nama-rs"));
             $("#info-dokter-modal #doctor-photo").attr("src", $(this).data("src-photo"));
             0 >= $(this).data("cv").length ? ($("#title-tentangdokter").addClass("hidden"), $(".scroll-cv-dokter").addClass("hidden")) : $("#info-dokter-modal #doctor-cv").html($(this).data("cv"))
         });
         $(document).on("click", "#info-dokter-content .row .center-info-dokter .row:first-child", function (a) {
             768 > $(window).width() && (a.preventDefault(), $(this).parent().next().hasClass("hidden-xs") ? ($(this).addClass("open"), $(this).parent().next(".col-md-9").removeClass("hidden-xs"),
                 $(this).parent().next(".col-md-9").removeClass("hidden-sm"), $(this).siblings().toggleClass("hidden"), $(this).parent().siblings(".row-bottom").toggleClass("hidden")) : ($(this).removeClass("open"), $(this).siblings().toggleClass("hidden"), $(this).parent().siblings(".row-bottom").toggleClass("hidden"), $(this).parent().next(".col-md-9").addClass("hidden-xs"), $(this).parent().next(".col-md-9").addClass("hidden-sm")))
         });
         0 < $(".card-search").length && 768 > $(window).width() && $(".card-search").first().find(".col-md-9").hasClass("hidden-xs") &&
             ($(".card-search").first().find(".col-md-3").addClass("open"), $(".card-search").first().find(".col-md-9").removeClass("hidden-xs"), $(".card-search").first().find(".col-md-9").removeClass("hidden-sm"));
         0 < $("#info-dokter-content").length && (768 > $(window).width() ? $("#info-dokter-content .row").first().find(".col-md-9").hasClass("hidden-xs") && ($("#info-dokter-content .row").first().find(".col-md-3 .row:first").addClass("open"), $("#info-dokter-content .row").first().find(".col-md-3 .row:last-child").toggleClass("hidden"),
             $("#info-dokter-content .row").first().find(".row-bottom").toggleClass("hidden"), $("#info-dokter-content .row").first().find(".col-md-9").removeClass("hidden-xs"), $("#info-dokter-content .row").first().find(".col-md-9").removeClass("hidden-sm")) : $("#info-dokter-content .row").first().find(".col-md-3 .row:last-child").removeClass("hidden"));
         0 < $(".content-fh").length && $(".content-fh .hide a").each(function () {
             $(this).addClass("btn btn-border-pink button-detail-rp");
             "id" == lang ? $(this).text("Unduh") : "en" ==
                 lang && $(this).text("Download")
         });
         $("#year-ir a").click(function (a) {
             a.preventDefault();
             $("#in-content").hide();
             $(".load-more").hide();
             a = $(this).closest(".tab-pane").attr("id");
             $("#" + a + " .loading-klinik").removeClass("hidden");
             var b = $("#select2-category-in").val();
             a = domain_investor_news;
             b = "search[category]=" + b + "&search[year]=" + $(this).text() + "&lang=" + lang;
             $.ajax({
                 type: "GET",
                 url: a,
                 data: b,
                 dataType: "json",
                 success: function (a) {
                     a = a.data.investor_news;
                     $("#in-content").html("");
                     if (void 0 != a) {
                         for (var b = $("#investor-news-template").html(),
                                 c = 0; c < a.length; c++) {
                             var d = replaceText(b, "{{TITLE}}", a[c].title);
                             d = replaceText(d, "{{DATE}}", (new Date(a[c].date_posted)).toLocaleString("id-ID", {
                                 year: "numeric",
                                 day: "numeric",
                                 month: "long"
                             }));
                             d = replaceText(d, "SLUG", domain + "/investor-relation/" + lang + "/" + a[c].category_slug + "/" + a[c].slug);
                             $("#in-content").append(d)
                         }
                         $(".load-more").show()
                     }
                     0 === a.length && ($(".load-more").hide(), "id" == lang ? $("#in-content").html("<b class='text-pink'>Nantikan berita seputar investor rumah sakit Mitra Keluarga</b>") : "en" == lang &&
                         $("#in-content").html("<b class='text-pink'>Our investorn news will be released later this year</b>"))
                 }
             });
             setTimeout(function () {
                 $("#in-content").fadeIn(200);
                 $(".loading-klinik").addClass("hidden")
             }, 500)
         });
         $(".year-rp a").click(function (a) {
             a.preventDefault();
             var b = $("#rp-menu a.open").attr("data-target");
             $(b).find(".row-result").hide();
             a = $(this).closest(".tab-pane").attr("id");
             $("#" + a + " .loading-klinik").removeClass("hidden");
             var c = $("#rp-menu a.open").attr("data-id-category");
             a = domain_investor_report;
             c = "search[category]=" + c + "&search[year]=" + $(this).text() + "&lang=" + lang;
             $.ajax({
                 type: "GET",
                 url: a,
                 data: c,
                 dataType: "json",
                 success: function (a) {
                     a = a.data.investor_reports;
                     $(b).find(".row-result").html("");
                     if (void 0 != a)
                         for (var c = $("#reports-presentations-template").html(), d = 0; d < a.length; d++) {
                             var e = replaceText(c, "{{TITLE}}", a[d].title);
                             e = replaceText(e, "{{URL}}", a[d].pdf_url);
                             e = replaceText(e, "{{ID}}", a[d].id);
                             null != a[d].cover_thumb ? (e = replaceText(e, "{{IMG}}", a[d].cover_thumb), e = replaceText(e, "hidden", "")) :
                                 e = replaceText(e, "{{IMG}}", "");
                             "en" == lang && (e = replaceText(e, "Unduh", "Download"));
                             $(b).find(".row-result").append(e)
                         }
                     0 === a.length && ("id" == lang ? $(b).find(".row-result").html("<b class='text-pink'>Nantikan laporan dan presentasi rumah sakit Mitra Keluarga</b>") : "end" == lang && $(b).find(".row-result").html("<b class='text-pink'>Our reports and presentations will be released later this year</b>"))
                 }
             });
             setTimeout(function () {
                 $(b).find(".row-result").fadeIn(200);
                 $(".loading-klinik").addClass("hidden")
             }, 500)
         });
         $(document).on("click", ".dokter-search", function (a) {
             1025 > $(window).width() && (a.preventDefault(), $(this).next().hasClass("hidden-xs") ? ($(this).addClass("open"), $(this).next(".col-md-9").removeClass("hidden-xs"), $(this).next(".col-md-9").removeClass("hidden-sm")) : ($(this).removeClass("open"), $(this).next(".col-md-9").addClass("hidden-xs"), $(this).next(".col-md-9").addClass("hidden-sm")))
         });
         $(document).on("click", "#info-dokter-content .button-buat-janji-detail", function () {
             var a = $(this).data("idsp"),
                 b = $(this).data("nama");
             $("#jadwal-form #form-specialization").val(a);
             $("#jadwal-form #form-namadokter").val(b);
             $("#jadwal-form #form-submit").trigger("click")
         });
         $("#spesialisasi-jadwal-klinik li a").click(function () {
             $("#btn-search-jadwal-klinik").data("idspecialization", $(this).data("id"))
         });
         $("#btn-search-jadwal-klinik").click(function () {
             var a = $(this).data("idclinic"),
                 b = $(this).data("idspecialization");
             a = null == b || 0 == b ? "id_clinic=" + a : "id_clinic=" + a + "&id_specialization=" + b;
             for (b = window.location.pathname.split("/");
                 "permata" !=
                 b[b.length - 1];) b.pop();
             b = b.join("/");
             $.ajax({
                 type: "GET",
                 url: "http://" + window.location.host + b + "/api/clinic-schedule",
                 data: a,
                 dataType: "json",
                 success: function (a) {
                     a = a.data.clinics;
                     $("#list-jk-content").html("");
                     if (void 0 != a)
                         for (var b = 1, c = 0; c < a.length; c++) {
                             var d = $("#jadwal-klinik-template").html();
                             d = replaceText(d, "{{SPNAME}}", a[c].name_specialization);
                             d = replaceText(d, "{{IDSCH}}", a[c].id);
                             var g = new Date,
                                 k;
                             0 == g.getDay() && (k = "sun");
                             1 == g.getDay() && (k = "mon");
                             2 == g.getDay() && (k = "tue");
                             3 == g.getDay() && (k = "wed");
                             4 == g.getDay() && (k = "thu");
                             5 == g.getDay() && (k = "fri");
                             6 == g.getDay() && (k = "sat");
                             g = g.getDate() + "-" + g.getMonth() + 1 + "-" + g.getFullYear();
                             g = window.location.origin + "/" + window.location.pathname.split("/")[1] + "/buat-janji/search-result?clinic=" + a[c].id_clinic + "&specialization=" + a[c].id_specialization + "&day=" + k + "&date=" + g;
                             d = replaceText(d, "{{URL}}", g);
                             $("#list-jk-content").append(d);
                             d = a[c].id;
                             g = $("#jadwal-dokter-template").html();
                             var h = $("#jam-jadwal-dokter-template").html(),
                                 f = replaceText(g, "{{HARI}}", "Senin");
                             f = replaceText(f, "{{ID}}", b);
                             var p = a[c].mon.split(", "),
                                 m = a[c].tue.split(","),
                                 t = a[c].wed.split(","),
                                 u = a[c].thu.split(","),
                                 v = a[c].fri.split(","),
                                 w = a[c].sat.split(","),
                                 x = a[c].sun.split(",");
                             $("#jadwal-klinik-template-content-" + d).append(f);
                             for (f = 0; f < p.length; f++) {
                                 var q = replaceText(h, "{{JAM}}", p[f]);
                                 0 < f && $("#jam-container-" + b).append("<p id='row1'></p>");
                                 $("#jam-container-" + b).append(q)
                             }
                             b++;
                             f = replaceText(g, "{{HARI}}", "Selasa");
                             f = replaceText(f, "{{ID}}", b);
                             $("#jadwal-klinik-template-content-" + d).append(f);
                             for (f = 0; f < m.length; f++) q = replaceText(h, "{{JAM}}", m[f]), 0 < f && $("#jam-container-" + b).append("<p id='row1'></p>"), $("#jam-container-" + b).append(q);
                             b++;
                             p = replaceText(g, "{{HARI}}", "Rabu");
                             p = replaceText(p, "{{ID}}", b);
                             $("#jadwal-klinik-template-content-" + d).append(p);
                             for (f = 0; f < t.length; f++) q = replaceText(h, "{{JAM}}", t[f]), 0 < f && $("#jam-container-" + b).append("<p id='row1'></p>"), $("#jam-container-" + b).append(q);
                             b++;
                             f = replaceText(g, "{{HARI}}", "Kamis");
                             f = replaceText(f, "{{ID}}", b);
                             $("#jadwal-klinik-template-content-" +
                                 d).append(f);
                             for (f = 0; f < u.length; f++) q = replaceText(h, "{{JAM}}", u[f]), 0 < f && $("#jam-container-" + b).append("<p id='row1'></p>"), $("#jam-container-" + b).append(q);
                             b++;
                             f = replaceText(g, "{{HARI}}", "Jumat");
                             f = replaceText(f, "{{ID}}", b);
                             $("#jadwal-klinik-template-content-" + d).append(f);
                             for (f = 0; f < v.length; f++) q = replaceText(h, "{{JAM}}", v[f]), 0 < f && $("#jam-container-" + b).append("<p id='row1'></p>"), $("#jam-container-" + b).append(q);
                             b++;
                             f = replaceText(g, "{{HARI}}", "Sabtu");
                             f = replaceText(f, "{{ID}}", b);
                             $("#jadwal-klinik-template-content-" +
                                 d).append(f);
                             for (f = 0; f < w.length; f++) q = replaceText(h, "{{JAM}}", w[f]), 0 < f && $("#jam-container-" + b).append("<p id='row1'></p>"), $("#jam-container-" + b).append(q);
                             b++;
                             f = replaceText(g, "{{HARI}}", "Minggu");
                             f = replaceText(f, "{{ID}}", b);
                             $("#jadwal-klinik-template-content-" + d).append(f);
                             for (f = 0; f < x.length; f++) q = replaceText(h, "{{JAM}}", x[f]), 0 < f && $("#jam-container-" + b).append("<p id='row1'></p>"), $("#jam-container-" + b).append(q);
                             b++
                         }
                 },
                 contentType: "application/json"
             })
         });
         0 < $("#list-jk-content").length && $("#btn-search-jadwal-klinik").trigger("click");
         $("#search-keyword").submit(function (a) {
             a.preventDefault();
             a = $(this).find("#input-keyword").val();
             if ("" != a) {
                 var b = "keywords=" + a + "&limit=6";
                 var c = domain_search_article
             } else b = "limit=6", c = domain + "/api/article";
             var e = domain + "/artikel?search=" + a;
             $(".loading-klinik").toggleClass("hidden");
             $(".news-part").toggleClass("hidden");
             $(".load-more").hide();
             $.ajax({
                 type: "GET",
                 url: c,
                 data: b,
                 contentType: "application/json",
                 success: function (a) {
                     $("#health-article-content").html("");
                     var b = !1,
                         c = $("#health-article-content-template").html();
                     if (200 == a.status && 0 < a.data.articles.length) {
                         b = !0;
                         a = a.data.articles;
                         for (var k = 0; k < a.length; k++) {
                             var h = domain + "/artikel/" + a[k].category_slug + "/" + a[k].slug,
                                 f = new Date(a[k].date_posted),
                                 g = replaceText(c, "{{ID}}", a[k].id);
                             g = replaceText(g, "SLUG", h);
                             g = replaceText(g, "{{IMAGE}}", a[k].image_full);
                             g = replaceText(g, "{{TITLE}}", a[k].title);
                             g = replaceText(g, "{{PUREDATE}}", a[k].date);
                             g = replaceText(g, "{{DATE}}", f.toLocaleString("id-ID", {
                                 year: "numeric",
                                 day: "numeric",
                                 month: "long"
                             }));
                             g = replaceText(g, "{{EXCERPT}}", a[k].excerpt.replace(/^(.{134}).+/,
                                 "$1&hellip;"));
                             $("#health-article-content").append(g)
                         }
                     } else $(".load-more").hide(), $("#health-article-content").append("<div class='article-home nodata'><p>Nantikan penawaran menarik dari kami di masa yang akan datang</p></div>");
                     setTimeout(function () {
                         b && $(".load-more").show();
                         $(".loading-klinik").addClass("hidden");
                         $(".news-part").toggleClass("hidden");
                         ChangeUrl("artikel", "title", e)
                     }, 500)
                 }
             })
         })
     });